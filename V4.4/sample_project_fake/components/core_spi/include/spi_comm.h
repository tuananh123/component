/**
 * @file spi_comm.h
 * @author backhug team
 * @brief 
 * @version 0.1
 * @date 2022-07-06
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef __SPI_COMM_H__
#define __SPI_COMM_H__

/*******************************************************************************
**                               INCLUDES
*******************************************************************************/

/*******************************************************************************
**                                DEFINES
*******************************************************************************/
#define PIN_NUM_MISO          12
#define PIN_NUM_MOSI          13
#define PIN_NUM_CLK           14
#define PIN_NUM_CS            15
#define GPIO_HANDSHAKE        26

#define MAX_PAYLOAD           280

#define PKG_INDICATOR_1       0x0D
#define PKG_INDICATOR_2       0x0A
#define PKG_END               0x55

#define RESPOND_ACK           0x30
#define RESPOND_NACK          0x31

#define CRC_INITIAL           0xFFFF
#define CRC_POLYNOMINAL       0x1021

typedef struct packet
{
  uint8_t cmd;
  uint8_t payload[MAX_PAYLOAD];
  uint16_t data_len;
  uint16_t crc;

} packet;

/*******************************************************************************
**                     EXTERNAL VARIABLE DECLARATIONS
*******************************************************************************/

/*******************************************************************************
**                     EXTERNAL FUNCTION DECLARATIONS
*******************************************************************************/
uint16_t crc16_calculate(struct packet buffer_packet);

void spi_comm_master_init(void);
uint8_t master_comm_recv(struct packet *recv_packet);
void master_comm_transmit(struct packet send_packet);
void master_send_respond(uint8_t cmd);
bool master_send_data(char* data, uint8_t length, uint8_t cmd);
bool master_receive_data(uint8_t* data, uint8_t length, uint8_t cmd);

void spi_comm_slave_init(void);
void slave_comm_transmit(struct packet send_packet);
uint8_t slave_comm_recv(struct packet *recv_packet);
void slave_send_respond(uint8_t cmd);
void slave_send_data(char *data, uint8_t length, uint8_t cmd);

#endif 

/******************************** End of file *********************************/
