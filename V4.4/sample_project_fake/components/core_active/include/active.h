/**
 * @file active.hpp
 * @author Nhat Bui (buivietnhatctqb@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2020-06-23
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#ifndef __active_H__
#define __active_H__

#ifdef __cplusplus
#endif

/*******************************************************************************
**                               INCLUDES
*******************************************************************************/
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/queue.h>
#include "ep.h"
/*******************************************************************************
**                                DEFINES
*******************************************************************************/
#define TICK_RATE 10U
#define MAX_ACTIVE 29U
/*******************************************************************************
**                     EXTERNAL VARIABLE DECLARATIONS
*******************************************************************************/

class Active : public Hsm
{
  StaticQueue_t *q_buf;
  QueueHandle_t queue;
  StaticTask_t *tsk_buf;
  uint8_t priority;

protected:
  Active(StaticQueue_t *q_buf, QueueHandle_t queue, StaticTask_t *tsk_buf,
         void *const queue_sto, uint32_t item_size,
         uint8_t queue_size, StateHandler initial);

public:
  virtual void start(const char *thread_name,
                     uint8_t prio, void *const stk_sto,
                     uint16_t stk_size, void const *const par);

  virtual void start_dynamic(const char *thread_name,
                             uint8_t prio,
                             uint16_t stk_size, void const *const par);

  void get(Event *e);
  void post(Event *e);
  void subscribe(Signal const sig);
  void unsubcribe(Signal const sig);

  static void add(Active *const a) noexcept;
  static void task_function(void *pvParameter);

// private:
};

class TimeEvent : public Event
{
public:
  TimeEvent *prev; /* link to the previous time event in the list */
  TimeEvent *next; /* link to the next time event in the list */

  Active *act; /* the active object that receive the time event */

  uint32_t ctr{0};      /* down counter */
  uint32_t interval{0}; /* interval for periodic time event */
  void arm(uint32_t nticks);
  TimeEvent(Active *const act, Signal sig);

  uint8_t disarm();
  void post_in(uint32_t nticks);
  void post_every(uint32_t nticks);

  static void create_tick_task();
};

extern Active *active_[MAX_ACTIVE + 1];
/*******************************************************************************
**                     EXTERNAL FUNCTION DECLARATIONS
*******************************************************************************/
void ps_init(SubscribleList *subscr_sto, Signal max_signal);
void publish(Event *e);

#ifdef __cplusplus
#endif

#endif /* __active_H__ */

/******************************** End of file *********************************/
