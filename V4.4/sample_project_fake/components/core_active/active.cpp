/**
 * @file active.cpp
 * @author Nhat Bui (buivietnhatctqb@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2020-06-24
 * 
 * @copyright Copyright (c) 2020
 * 
 */

/*******************************************************************************
**                                INCLUDES
*******************************************************************************/
#include "active.h"
#include "as.h"

DEFINE_THIS_FILE

/*******************************************************************************
**                       INTERNAL MACRO DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      COMMON VARIABLE DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      INTERNAL VARIABLE DEFINITIONS
*******************************************************************************/
Active *active_[MAX_ACTIVE + 1];
/*******************************************************************************
**                      INTERNAL FUNCTION PROTOTYPES
*******************************************************************************/

/*******************************************************************************
**                          FUNCTION DEFINITIONS
*******************************************************************************/
Active::Active(StaticQueue_t *q_buf, QueueHandle_t queue, StaticTask_t *tsk_buf,
               void *const queue_sto, uint32_t item_size,
               uint8_t queue_size, StateHandler initial)
    : Hsm(initial), q_buf(q_buf), queue(queue), tsk_buf(tsk_buf)
{
  this->queue = xQueueCreateStatic(
      (UBaseType_t)queue_size,
      (UBaseType_t)item_size,
      (uint8_t *)queue_sto, this->q_buf);

  ENSURE(this->queue != NULL);
}

void Active::start(const char *thread_name,
                   uint8_t prio, void *const stk_sto,
                   uint16_t stk_size, void const *const par)
{
  TaskHandle_t thr;
  priority = prio;
  add(this);
  init(static_cast<const Event *>(par));
  /* statically create the FreeRTOS task for the AO */
  thr = xTaskCreateStatic(
      (TaskFunction_t)&task_function,          /* the task function */
      thread_name,                             /* the name of the task */
      stk_size,                                /* stack size */
      (void *)this,                            /* the 'pvParameters' parameter */
      (UBaseType_t)(prio + tskIDLE_PRIORITY),  /* FreeRTOS priority */
      (StackType_t *)stk_sto,                  /* stack storage */
      tsk_buf);                                /* task buffer */
  ENSURE(thr != static_cast<TaskHandle_t>(0)); /* must be created */
}

void Active::task_function(void *pvParameter)
{
  Active *active = static_cast<Active *>(pvParameter);
  while (1)
  {
    Event e;
    active->get(&e);
    active->dispatch(&e);
  }
}

void Active::add(Active *const a) noexcept
{
  ASSERT(a != (Active *)0);
  uint8_t id = a->priority;
  portMUX_TYPE mutex = portMUX_INITIALIZER_UNLOCKED;
  portENTER_CRITICAL(&mutex); /* lock the interrupt */
  active_[id] = a;
  portEXIT_CRITICAL(&mutex); /* unlock the interrupt */
}

void Active::get(Event *e)
{
  ASSERT(this != static_cast<Active *>(0));

  xQueueReceive(queue, static_cast<void *>(e), portMAX_DELAY);
  // printf("have received event from the queue\n");
}

void Active::post(Event *e)
{
  portMUX_TYPE mutex = portMUX_INITIALIZER_UNLOCKED;
  portENTER_CRITICAL(&mutex); /* lock the interrupt */
  xQueueSend(queue, static_cast<void *>(e), static_cast<TickType_t>(0));
  portEXIT_CRITICAL(&mutex);
  // printf("have sent event to the queue\n");
}

void Active::start_dynamic(const char* thread_name, uint8_t prio, uint16_t stk_size,
                           void const* const par)
{
  BaseType_t thr;
  TaskHandle_t handle;
  priority = prio;

  add(this);

  init(static_cast<const Event *>(par));

  thr = xTaskCreate((TaskFunction_t)&task_function,
                    thread_name,
                    stk_size,
                    (void *)this,
                    prio + tskIDLE_PRIORITY,
                    &handle);

  ENSURE(thr != (BaseType_t)0); /* must be created */
}
/******************************** End of file *********************************/
