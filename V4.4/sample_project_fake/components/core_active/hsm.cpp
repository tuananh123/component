/**
 * @file hsm.cpp
 * @author Nhat Bui (buivietnhatctqb@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2020-06-23
 * 
 * @copyright Copyright (c) 2020
 * 
 */

/*******************************************************************************
**                                INCLUDES
*******************************************************************************/
#include "ep.h"
#include "as.h"
#include <stdio.h>
DEFINE_THIS_FILE
/*******************************************************************************
**                       INTERNAL MACRO DEFINITIONS
*******************************************************************************/
/** helper macro to trigger reserved event in an HSM */
#define TRIG(State, sig_) \
  ((*(State))(this, &reserved_event[sig_]))

/** helper macro to trigger entry action in an HSM */
#define EXIT(State) \
  TRIG(State, EXIT_SIG)

/** helper macro to trigger exit action in an HSM */
#define ENTER(State) \
  TRIG(State, ENTRY_SIG)
/*******************************************************************************
**                      COMMON VARIABLE DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      INTERNAL VARIABLE DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      INTERNAL FUNCTION PROTOTYPES
*******************************************************************************/
enum
{
  EMPTY_SIG = 0
};

static Event const reserved_event[] = {
    {(uint16_t)EMPTY_SIG},
    {(uint16_t)ENTRY_SIG},
    {(uint16_t)EXIT_SIG},
    {(uint16_t)INIT_SIG}};
/*******************************************************************************
**                          FUNCTION DEFINITIONS
*******************************************************************************/
Hsm::Hsm(StateHandler const initial) noexcept
{
  state = initial;
}

void Hsm::init(void const *const e)
{
  StateHandler t;
  /* the top-most initial transition must be taken */
  State r = (*state)(this, EVT_CAST(Event));
  ASSERT(r == RET_TRAN);

  t = STATE_CAST(&top); /* HSM starts in the top state */
  do
  { /* drill into the target. . . */
    StateHandler path[MAX_NEST_DEPTH];
    int8_t ip = (int8_t)0; /* transition entry path index */
    path[0] = state;       /* save the target of the initial transition */

    TRIG(state, EMPTY_SIG);

    while (state != t)
    {
      path[++ip] = state;
      TRIG(state, EMPTY_SIG);
    }

    state = path[0]; /* restore the target of the initial tran. */
                     /* entry path must not overflow */
    ASSERT(ip < (int8_t)MAX_NEST_DEPTH);
    do
    {                  /* retrace the entry path in reverse (desired) order. . . */
      ENTER(path[ip]); /* enter path [ip] */
    } while ((--ip) >= (int8_t)0);

    t = path[0]; /* current state becomes the new source */

  } while (TRIG(t, INIT_SIG) == RET_TRAN);

  state = t;
}

void Hsm::dispatch(Event const *const e) noexcept
{
  StateHandler path[MAX_NEST_DEPTH];
  StateHandler s;
  StateHandler t;
  State r;
  t = state; /* save the current state */

  do
  { /* process the event hierarchically. . . */
    s = state;
    r = (*s)(this, e); /* invoke state handler s */
  } while (r == RET_SUPER);

  if (r == RET_TRAN)
  { /* transition taken? */
    int8_t ip;
    path[0] = state; /* save the target of the transition */
    path[1] = t;     /* save the current state before transition */
    path[2] = s;

    while (t != s)
    { /* exit current state to transition source s. . . */
      if (TRIG(t, EXIT_SIG) == RET_HANDLED)
      {                     /* exit handled? */
        TRIG(t, EMPTY_SIG); /* find superstate of t */
      }
      t = state; /* state holds the superstate */
    }

    ip = hsm_tran(path);

    /* retrace the entry path in reverse (desired) order... */
    for (; ip >= 0; --ip)
    {
      ENTER(path[ip]); /* enter path[ip] */
    }

    t = path[0]; /* stick the target into register */
    state = t;   /* update the next state */

    /* drill into the target hierarchy... */
    while (TRIG(t, INIT_SIG) == (State)RET_TRAN)
    {
      ip = 0;
      path[0] = state;

      TRIG(state, EMPTY_SIG); /* find superstate */

      while (state != t)
      {
        ++ip;
        path[ip] = state;
        TRIG(state, EMPTY_SIG); /* find super */
      }
      state = path[0];

      /* entry path must not overflow */
      ASSERT(ip < MAX_NEST_DEPTH);

      /* retrace the entry path in reverse (correct) order... */
      do
      {
        ENTER(path[ip]); /* enter path[ip] */
        --ip;
      } while (ip >= 0);

      t = path[0]; /* current state becomes the new source */
    }
  }

  state = t; /* change the current active state */
}

int8_t Hsm::hsm_tran(StateHandler (&path)[MAX_NEST_DEPTH])
{
  int8_t ip = -1; /* transition entry path index */
  int8_t iq;      /* helper transition entry path index */
  StateHandler t = path[0];
  StateHandler const s = path[2];
  State r;

  /* (a) check source==target (transition to self)... */
  if (s == t)
  {
    EXIT(s); /* exit the source */
    ip = 0;  /* enter the target */
  }
  else
  {
    TRIG(t, EMPTY_SIG); /* find superstate of target */

    t = state;

    /* (b) check source==target->super... 
        requires only entry to the target but no exit from the source */
    if (s == t)
    {
      ip = 0; /* enter the target */
    }
    else
    {
      TRIG(s, EMPTY_SIG); /* find superstate of src */

      /* (c) check source->super==target->super... */
      if (state == t)
      {
        EXIT(s); /* exit the source */
        ip = 0;  /* enter the target */
      }
      else
      {
        /* (d) check source->super==target... */
        if (state == path[0])
        {
          EXIT(s); /* exit the source */
        }
        else
        {
          /* (e) check rest of source==target->super->super..
                    * and store the entry path along the way
                    */
          iq = 0;      /* indicate that LCA not found */
          ip = 1;      /* enter target and its superstate */
          path[1] = t; /* save the superstate of target */
          t = state;   /* save source->super */

          /* find target->super->super... */
          r = TRIG(path[1], EMPTY_SIG);
          while (r == (State)RET_SUPER)
          {
            ++ip;
            path[ip] = state; /* store the entry path */
            if (state == s)
            {         /* is it the source? */
              iq = 1; /* indicate that LCA found */

              /* entry path must not overflow */
              ASSERT(ip < MAX_NEST_DEPTH);
              --ip;                   /* do not enter the source */
              r = (State)RET_HANDLED; /* terminate loop */
            }
            /* it is not the source, keep going up */
            else
            {
              r = TRIG(state, EMPTY_SIG);
            }
          }

          /* the LCA not found yet? */
          if (iq == 0)
          {

            /* entry path must not overflow */
            ASSERT(ip < MAX_NEST_DEPTH);

            EXIT(s); /* exit the source */

            /* (f) check the rest of source->super
                        *                  == target->super->super...
                        */
            iq = ip;
            r = (State)RET_IGNORED; /* LCA NOT found */
            do
            {
              if (t == path[iq])
              {                         /* is this the LCA? */
                r = (State)RET_HANDLED; /* LCA found */
                ip = iq - 1;            /* do not enter LCA */
                iq = -1;                /* cause termintion of the loop */
              }
              else
              {
                --iq; /* try lower superstate of target */
              }
            } while (iq >= 0);

            /* LCA not found? */
            if (r != (State)RET_HANDLED)
            {
              /* (g) check each source->super->...
                            * for each target->super...
                            */
              r = (State)RET_IGNORED; /* keep looping */
              do
              {
                /* exit t unhandled? */
                if (TRIG(t, EXIT_SIG) == (State)RET_HANDLED)
                {

                  TRIG(t, EMPTY_SIG);
                }
                t = state; /* set to super of t */
                iq = ip;
                do
                {
                  /* is this LCA? */
                  if (t == path[iq])
                  {
                    /* do not enter LCA */
                    ip = (int8_t)(iq - 1);
                    iq = -1; /* break out of inner loop */
                    /* break out of outer loop */
                    r = (State)RET_HANDLED;
                  }
                  else
                  {
                    --iq;
                  }
                } while (iq >= 0);
              } while (r != (State)RET_HANDLED);
            }
          }
        }
      }
    }
  }
  return ip;
}

State Hsm::top(void *const me, Event const *const e) noexcept
{
  return RET_IGNORED;
}

bool Hsm::is_in(StateHandler const s) noexcept
{
  bool in_state = false; /* assume that this HSM is not in 'state' */
  State r;

  /* scan the state hierarchy bottom-up */
  do
  {
    /* do the states match? */
    if (state == s)
    {
      in_state = true; /* 'true' means that match found */
      r = RET_IGNORED; /* break out of the loop */
    }
    else
    {
      r = TRIG(state, EMPTY_SIG);
    }
  } while (r != RET_IGNORED); /* top() state not reached */

  return in_state; /* return the status */
}

/******************************** End of file *********************************/
