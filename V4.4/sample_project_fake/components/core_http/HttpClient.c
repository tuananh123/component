///
/// @file HttpServer.cpp
///
/// @author Boyan M. Atanasov
/// @author MageControl Systems LTD.
///
/// @date Jun 11, 2018
/// @version MCS_v1
///
/// @brief This source file contains definitions for the http server. Declarations are contained within HttpServer.h
///
/// @copyright Copyright (c) 2018 MageControl Systems LTD.
/// All rights reserved. This file may not, in whole or in part, be reproduced,
/// used, displayed, modified, disclosed, or transferred outside of
/// MageControl Systems LTD., without express written permission to do so.
///
/// @section DESCRIPTION
///
/// This source file contains definitions for the parts,
/// distinctly associated with the custom API building part
/// of the Http Server implementation.
///

#include <string.h>
#include <stdlib.h>
#include "HttpClient.h"
#include "esp_http_client.h"
#include <esp_log.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

#define TAG "HTTP_CLIENT"
#define MAX_HTTP_RECV_BUFFER 512

esp_http_client_handle_t client;
uint8_t was_client_init;

esp_err_t http_client_init(const char *url, int *content_length)
{
    esp_http_client_config_t config =
        {
            .url = url,
            .event_handler = _http_event_handle,
            .transport_type = HTTP_TRANSPORT_OVER_SSL,
        };

    client = esp_http_client_init(&config);
    was_client_init = 1;
    esp_err_t err;
    if ((err = esp_http_client_open(client, 0)) != ESP_OK)
    {
        ESP_LOGE(TAG, "Failed to open HTTP connection: %s", esp_err_to_name(err));
        return err;
    }

    *content_length = esp_http_client_fetch_headers(client);
    ESP_LOGI(TAG, "Content Length = %d", *content_length);

    return ESP_OK;
}

esp_err_t http_client_get_with_token(const char *url, int *content_length, char *token)
{
    esp_http_client_config_t config =
        {
            .url = url,
            .event_handler = _http_event_handle,
            .transport_type = HTTP_TRANSPORT_OVER_SSL,
        };

    client = esp_http_client_init(&config);
    was_client_init = 1;
    esp_http_client_set_header(client, "token", token);
    esp_http_client_set_method(client, HTTP_METHOD_GET);
    esp_err_t err;
    if ((err = esp_http_client_open(client, 0)) != ESP_OK)
    {
        ESP_LOGE(TAG, "Failed to open HTTP connection: %s", esp_err_to_name(err));
        return err;
    }

    *content_length = esp_http_client_fetch_headers(client);

    return ESP_OK;
}

esp_err_t http_client_post_with_token(const char *url, char *token, char *post_data)
{
    esp_http_client_config_t config =
        {
            .url = url,
            .event_handler = _http_event_handle,
            .transport_type = HTTP_TRANSPORT_OVER_SSL,
        };

    client = esp_http_client_init(&config);
    was_client_init = 1;
    esp_http_client_set_header(client, "token", token);
    esp_http_client_set_method(client, HTTP_METHOD_POST);

    // esp_http_client_set_post_field(client, post_data, strlen(post_data));

    if (http_client_open(strlen(post_data)) != ESP_OK)
    {
        return -1;
    }

    if (esp_http_client_write(client, post_data, strlen(post_data)) == -1)
    {
        ESP_LOGE(TAG, "HTTP write failed");
        return -1;
    }

    esp_http_client_fetch_headers(client);
    // ESP_LOGI(TAG, "Content Length = %d", content_length);

    return ESP_OK;
}

esp_err_t http_client_put_with_token(const char *url, char *token, char *post_data)
{
    esp_http_client_config_t config =
        {
            .url = url,
            .event_handler = _http_event_handle,
            .transport_type = HTTP_TRANSPORT_OVER_SSL,
        };

    client = esp_http_client_init(&config);
    was_client_init = 1;
    esp_http_client_set_header(client, "token", token);
    esp_http_client_set_method(client, HTTP_METHOD_PUT);

    // esp_http_client_set_post_field(client, post_data, strlen(post_data));

    if (http_client_open(strlen(post_data)) != ESP_OK)
    {
        return -1;
    }

    if (esp_http_client_write(client, post_data, strlen(post_data)) == -1)
    {
        ESP_LOGE(TAG, "HTTP write failed");
        return -1;
    }

    esp_http_client_fetch_headers(client);
    // ESP_LOGI(TAG, "Content Length = %d", content_length);

    return ESP_OK;
}

esp_err_t http_client_put_file_with_token(const char *url, char *token, char *post_data)
{
    esp_http_client_set_header(client, "token", token);
    esp_http_client_set_method(client, HTTP_METHOD_PUT);

    // esp_http_client_set_post_field(client, post_data, strlen(post_data));

    if (http_client_open(strlen(post_data)) != ESP_OK)
    {
        return -1;
    }

    if (esp_http_client_write(client, post_data, strlen(post_data)) == -1)
    {
        ESP_LOGE(TAG, "HTTP write failed");
        return -1;
    }

    esp_http_client_fetch_headers(client);
    // ESP_LOGI(TAG, "Content Length = %d", content_length);

    return ESP_OK;
}

void http_client_read(char *buffer, int *read, bool *endMessage)
{
    *read = esp_http_client_read(client, buffer, MAX_HTTP_RECV_BUFFER);
    if (*read <= 0)
    {
        ESP_LOGI(TAG, "END OF MESSAGE");
        *endMessage = true;
    }
    ESP_LOGI(TAG, "read = %d", *read);
}

void http_client_read_with_contentLength(char *buffer, int *read, bool *endMessage, int contentLength)
{
    *read = esp_http_client_read(client, buffer, contentLength);
    if (*read <= 0)
    {
        ESP_LOGE(TAG, "Error read data");
        *endMessage = true;
    }
    ESP_LOGI(TAG, "read = %d", *read);
}

esp_err_t http_client_open(int length)
{
    esp_err_t err;
    if ((err = esp_http_client_open(client, length)) != ESP_OK)
    {
        ESP_LOGE(TAG, "Failed to open HTTP connection: %s", esp_err_to_name(err));
        return err;
    }
    return ESP_OK;
}

int http_client_write(char *postData, int length)
{
    return esp_http_client_write(client, postData, length);
}

void http_client_set_post()
{
    esp_http_client_set_method(client, HTTP_METHOD_POST);
}

void http_client_set_header(char *headerKey, char *headerValue)
{
    esp_http_client_set_header(client, headerKey, headerValue);
}

int http_client_fetch_headers()
{
    return esp_http_client_fetch_headers(client);
}

bool http_client_is_chunked_response()
{
    return esp_http_client_is_chunked_response(client);
}

int http_client_get_content_length()
{
    return esp_http_client_get_content_length(client);
}

int http_client_get_status_code()
{
    return esp_http_client_get_status_code(client);
}

void http_client_end()
{
    if (was_client_init)
    {
        was_client_init = 0;
        esp_http_client_close(client);
        esp_http_client_cleanup(client);
    }
}

void http_client_end1()
{
    // esp_http_client_cleanup(client);
    esp_http_client_close(client);
}

void http_client_close()
{
    esp_http_client_close(client);
}

esp_err_t _http_event_handle(esp_http_client_event_t *evt)
{
    switch (evt->event_id)
    {
    case HTTP_EVENT_ERROR:
        ESP_LOGE(TAG, "HTTP_EVENT_ERROR");
        break;
    case HTTP_EVENT_ON_CONNECTED:
        ESP_LOGI(TAG, "HTTP_EVENT_ON_CONNECTED");
        break;
    case HTTP_EVENT_HEADER_SENT:
        ESP_LOGI(TAG, "HTTP_EVENT_HEADER_SENT");
        break;
    case HTTP_EVENT_ON_HEADER:
        // ESP_LOGI(TAG, "HTTP_EVENT_ON_HEADER");
        printf("%.*s", evt->data_len, (char *)evt->data);
        break;
    case HTTP_EVENT_ON_DATA:
        ESP_LOGI(TAG, "HTTP_EVENT_ON_DATA, len=%d", evt->data_len);
        // if (!esp_http_client_is_chunked_response(evt->client))
        // {
        //     printf("%.*s", evt->data_len, (char *)evt->data);
        // }

        break;
    case HTTP_EVENT_ON_FINISH:
        ESP_LOGI(TAG, "HTTP_EVENT_ON_FINISH");
        break;
    case HTTP_EVENT_DISCONNECTED:
        ESP_LOGI(TAG, "HTTP_EVENT_DISCONNECTED");
        break;
    }
    return ESP_OK;
}
