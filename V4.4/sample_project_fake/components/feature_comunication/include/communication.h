/**
 * @file comunication.h
 * @author backhug team
 * @brief 
 * @version 0.1
 * @date 2022-07-07
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef __COMMUNICATION_H__
#define __COMMUNICATION_H__

/*******************************************************************************
**                               INCLUDES
*******************************************************************************/
#include "spi_comm.h"

/*******************************************************************************
**                                DEFINES
*******************************************************************************/
#define CMD_UPDATE_REQ        0x20
#define CMD_UPDATE_PARAMS     0x21
#define CMD_UPDATE_DATA       0x22
#define CMD_TERMINATE         0x23
#define CMD_LOG_REQ           0x24
#define CMD_LOG_PARAMS        0x25
#define CMD_LOG_DATA          0x26
#define CMD_LOG_END           0x27
#define CMD_WIFI_SSID_REQ     0x28
#define CMD_WIFI_PASSWORD_REQ 0x29
#define CMD_SEND_DATA_REQ     0xB0
#define WIFI_CONNECT_SUCCEED  0x01
#define WIFI_CONNECT_FAILED   0x00
#define CMD_WIFI_MAC_REQ      0x40
#define CMD_QUERY_REQ         0x42
#define CMD_WIFI_SCAN_REQ     0x43
#define CMD_WIFI_SCAN_DATA    0x44
#define CMD_DAT_REQ_SMAP      0x34
#define CMD_DAT_FILE          0x35
#define CMD_DAT_PARAMS        0x36
#define CMD_DAT_DATA          0x37
#define CMD_DAT_END           0x38
#define CMD_GET_DATA_REQ      0x39
#define CMD_OTA_WIFI_REQ      0x3A
#define CMD_OTA_WIFI_PARAMS   0x3B
#define CMD_OTA_WIFI_DATA     0x3C
#define CMD_OTA_WIFI_END      0x3D
#define CMD_DAT_REQ_LOG       0x3F

typedef struct
{
  uint8_t wifi_state;
  uint8_t version[3];

} wifi_data_t;

typedef struct
{
  uint8_t version[3];
  uint8_t ble_connected;

} ble_data_t;

typedef struct
{
  uint8_t new_wifi_request;
  uint8_t new_wifi_wps;
  uint8_t new_wifi_scan;
  uint8_t new_firmware_wifi;
  uint8_t new_file_massage;
  uint8_t new_testing_wifi;
  uint8_t new_file_log;

} query_request_t;

typedef struct
{
  uint8_t wifi_state;
  uint8_t version[3];

} send_data_t;

typedef struct
{
  uint8_t version[3];
  uint8_t ble_connected;

} receive_data_t;

extern query_request_t query_request;
/*******************************************************************************
**                     EXTERNAL VARIABLE DECLARATIONS
*******************************************************************************/

/*******************************************************************************
**                 EXTERNAL FUNCTION DECLARATIONS SENDER
*******************************************************************************/
bool send_wifi_mac_request(packet *buffer_packet);
bool send_wifi_ssid_request(packet *buffer_packet);
bool send_wifi_password_request(packet *buffer_packet);
void create_master_comm_task(void);
/*******************************************************************************
**                 EXTERNAL FUNCTION DECLARATIONS RECEIVER
*******************************************************************************/
void slave_send_wifi_parameter(uint8_t parameter);
void slave_send_wifi_mac_address(uint8_t cmd);
void slave_send_version(uint8_t cmd);
void slave_send_firmware_version(struct packet buffer_packet);
void suspend_spi_task();
void create_slave_comm_task(void);

#endif 

/******************************** End of file *********************************/
