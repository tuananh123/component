/**
 * @file receiver.cpp
 * @author backhug team
 * @brief 
 * @version 0.1
 * @date 2022-07-07
 * 
 * @copyright Copyright (c) 2022
 * 
 */

/*******************************************************************************
**                                INCLUDES
*******************************************************************************/
#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "esp_system.h"
#include "esp_log.h"
#include "communication.h"
#include "nvs_flash.h"

/*******************************************************************************
**                       INTERNAL MACRO DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      COMMON VARIABLE DEFINITIONS
*******************************************************************************/
static TaskHandle_t comm_task_handle = NULL;

/*******************************************************************************
**                      INTERNAL VARIABLE DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      INTERNAL FUNCTION PROTOTYPES
*******************************************************************************/

/*******************************************************************************
**                          FUNCTION DEFINITIONS
*******************************************************************************/

void slave_send_wifi_parameter(uint8_t parameter)
{
  nvs_handle my_nvs_handle;
  size_t required_size;
  char wifi_parameter[64];

  nvs_open("storage", NVS_READWRITE, &my_nvs_handle);
  if (parameter == CMD_WIFI_SSID_REQ)
  {
    /* Get default AP's SSID */
    nvs_get_str(my_nvs_handle, "SSID", NULL, &required_size);
    if (nvs_get_str(my_nvs_handle, "SSID", wifi_parameter, &required_size) !=
        ESP_OK || wifi_parameter[0] == NULL)
      slave_send_respond(RESPOND_NACK);
    else
      slave_send_data(wifi_parameter, required_size, RESPOND_ACK);
  }
  else if (parameter == CMD_WIFI_PASSWORD_REQ)
  {
    /* Get default AP's */
    nvs_get_str(my_nvs_handle, "PASS", NULL, &required_size);
    if (nvs_get_str(my_nvs_handle, "PASS", wifi_parameter, &required_size) !=
        ESP_OK)
      slave_send_respond(RESPOND_NACK);
    else
      slave_send_data(wifi_parameter, required_size, RESPOND_ACK);
  }
  nvs_close(my_nvs_handle);
}

void slave_send_wifi_mac_address(uint8_t cmd)
{
  uint8_t mac_addr[6] = {0};

  esp_read_mac(mac_addr, ESP_MAC_BT);

  slave_send_data((char*) mac_addr, 6, RESPOND_ACK);
}

void slave_send_version(uint8_t cmd)
{

  slave_send_data((char*) 1, 1, RESPOND_ACK);
}

void slave_send_firmware_version(packet buffer_packet)
{  
  slave_send_data((char*)firmware_version, 3, RESPOND_ACK);
}

void suspend_spi_task()
{
  printf("Suspend spi task\n");
  
  vTaskSuspend(comm_task_handle);
}

static void slave_comm_task(void)
{
  struct packet buffer_packet;

  while (1)
  {
    if (slave_comm_recv(&buffer_packet))
    {
      printf("cmd: %x\n", buffer_packet.cmd);
    //   switch (buffer_packet.cmd)
    //   {
    //     case CMD_UPDATE_REQ:
    //       if (!update_manager.isUpdating)
    //       {
    //         backhug.local_state = UPDATING_FROM_WIFI;
    //         ble_update_adv_data((uint8_t) backhug.local_state );
    //         slave_ota_process(buffer_packet);
    //         backhug.local_state = CONNECTED;
    //         ble_update_adv_data((uint8_t) backhug.local_state);   
    //       }
    //       else
    //       {
    //         slave_send_respond(RESPOND_NACK);    
    //       }        
    //       break;
    //     case CMD_WIFI_SSID_REQ:
    //       slave_send_wifi_parameter(CMD_WIFI_SSID_REQ);
    //       break;
    //     case CMD_WIFI_PASSWORD_REQ:
    //       slave_send_wifi_parameter(CMD_WIFI_PASSWORD_REQ);
    //       new_wifi_updated = false;
    //       break;
    //     case CMD_RECEIVE_DATA_REQ:
    //       memcpy((uint8_t*)&receive_data, buffer_packet.payload, sizeof(receive_data_t));

    //       if(new_wifi_updated == 0 && new_wps_updated == 0)
    //         ble_update_wifi_status(receive_data.wifi_state);
    //       if (receive_data.version[0] == 1 && receive_data.version[1] == 1 && receive_data.version[2] > 6)
    //       {
    //         if (wifi_version[0] != receive_data.version[0] || wifi_version[1] != receive_data.version[1] || wifi_version[2] != receive_data.version[2] )
    //         {
    //           nvs_store_version_wifi(receive_data.version,"WIFI_VERSION");
    //           nvs_get_version_wifi(wifi_version,"WIFI_VERSION");
    //           if (backhug.local_state == UPDATING_FROM_BLE)
    //             backhug.local_state = CONNECTED;
    //           ble_update_adv_data((uint8_t) backhug.local_state);  
    //         }
    //       }
    //       printf("WIFI{STATE:%d,VER:%d.%d.%d}\n",receive_data.wifi_state,wifi_version[0],wifi_version[1],wifi_version[2]);
    //       slave_send_respond(RESPOND_ACK);
    //       break;
    //     case CMD_WIFI_MAC_REQ:
    //       slave_send_wifi_mac_address(CMD_WIFI_MAC_REQ);
    //       break;
    //     case CMD_QUERY_REQ:
    //     {
    //       if (wifi_version[0] == 1 && wifi_version[1] == 1 && wifi_version[2] >= 7)
    //       {
    //         send_request.new_wifi_request = new_wifi_updated;
    //         send_request.new_wifi_wps = new_wps_updated;
    //         send_request.new_wifi_scan = new_wifi_scan;
    //         send_request.new_firmware_wifi = update_manager.new_firmware_wifi;
    //         if (receive_data.wifi_state)
    //           send_request.new_file_massage = countFileSDcard();
    //         new_wps_updated  = false;
    //         new_wifi_scan    = false;
    //         slave_send_data((char*)&send_request, sizeof(send_request_t), RESPOND_ACK);
    //       }
    //       else
    //       {
    //         if (new_wifi_updated)
    //           slave_send_respond(RESPOND_ACK);
    //         else
    //           slave_send_respond(RESPOND_NACK);
    //       }
    //       break;
    //     }
    //     case CMD_WIFI_SCAN_REQ:
    //       slave_send_respond(RESPOND_ACK);  
    //       while (slave_comm_recv(&buffer_packet))
    //       {
    //         if (buffer_packet.cmd == CMD_WIFI_SCAN_DATA)
    //         {
    //           slave_send_respond(RESPOND_ACK);  
    //           printf("Index %d: RSSI %d, SSID: %s\n",buffer_packet.payload[0],(int8_t)buffer_packet.payload[1],(char*)&buffer_packet.payload[2]);
    //           global_cmd_notify_send(GLOBAL_CMD_SCAN_WIFI,buffer_packet.payload,35);
    //         }
    //         else if (buffer_packet.cmd == CMD_TERMINATE)
    //         {
    //           break;
    //         }
    //         else 
    //         {
    //           printf("BREAK!! Command failed\n");
    //           break;
    //         }
    //       }  
    //       break;
    //     case CMD_DAT_REQ:
    //       slave_dat_process(buffer_packet);
    //       break;
    //     case CMD_GET_DATA_REQ:
    //       send_data.version[0] = firmware_version[0];
    //       send_data.version[1] = firmware_version[1];
    //       send_data.version[2] = firmware_version[2];
    //       send_data.ble_connected = ble_connected;
    //       slave_send_data((char*)&send_data, sizeof(send_data_t), RESPOND_ACK);
    //       break;
    //     case CMD_OTA_WIFI_REQ:
    //       slave_ota_wifi_process(buffer_packet);
    //       break;
    //     default:
    //       slave_send_respond(RESPOND_NACK);    
    //       break;
    //   }
    }

    vTaskDelay(100 / portTICK_PERIOD_MS);
  }
}

void create_slave_comm_task(void)
{
  if (comm_task_handle == NULL)
  {
    xTaskCreate((TaskFunction_t) & slave_comm_task, "comm_task", 5*1024, NULL, 5,
                &comm_task_handle);

    ESP_LOGW("spi slave comm task", "start");
  }
}