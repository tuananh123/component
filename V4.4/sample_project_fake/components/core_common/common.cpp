/**
 * @file common.cpp
 * @author backhug team
 * @brief 
 * @version 0.1
 * @date 2022-07-04
 * 
 * @copyright Copyright (c) 2022
 * 
 */

/*******************************************************************************
**                                INCLUDES
*******************************************************************************/
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <driver/gpio.h>
#include "as.h"
#include "common.h"

DEFINE_THIS_FILE

/*******************************************************************************
**                       INTERNAL MACRO DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      COMMON VARIABLE DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      INTERNAL VARIABLE DEFINITIONS
*******************************************************************************/
StackType_t hb_stk_sto[2*1024];
StaticTask_t hb_buf;

/*******************************************************************************
**                      INTERNAL FUNCTION PROTOTYPES
*******************************************************************************/

/*******************************************************************************
**                          FUNCTION DEFINITIONS
*******************************************************************************/

NORETURN on_assert(char_t const *const module, int_t const location)
{
  printf("assert failed on %s: line %d\n", module, location);

  // while (1)
  // {
  //     // do nothing
  // }
  // save_reset_predicted(1);
  esp_restart();
}

static void heart_beat_led(void *pvParameter)
{
  uint8_t output = 0;
  gpio_pad_select_gpio(LED_GPIO);
  gpio_set_direction(LED_GPIO, GPIO_MODE_OUTPUT);
  while (1)
  {
    gpio_set_level(LED_GPIO, (output = (output) ? 0 : 1));
    vTaskDelay(1000 / portTICK_PERIOD_MS);
  }
}

void create_heart_beat_task(void)
{
	TaskHandle_t thr;
	thr = xTaskCreateStatic(
			&heart_beat_led,
			"hb",
			2*1024,
			NULL,
			(UBaseType_t)(configMAX_PRIORITIES + 1),
			(StackType_t *)hb_stk_sto,
			&hb_buf);
	ENSURE(thr != (TaskHandle_t)0);
}
