/**
 * @file log_to_sdcard.cpp
 * @author backhug team
 * @brief 
 * @version 0.1
 * @date 2022-25-08
 * 
 * @copyright Copyright (c) 2022
 * 
 */

/*******************************************************************************
**                                INCLUDES
*******************************************************************************/
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <stdio.h>
#include <string.h>
#include <sys/unistd.h>
#include "esp_err.h"
#include "esp_log.h"
#include "esp_vfs_fat.h"
#include "driver/sdmmc_host.h"
#include "driver/sdspi_host.h"
#include "sdmmc_cmd.h"
#include <sys/stat.h>
/*******************************************************************************
**                       INTERNAL MACRO DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      COMMON VARIABLE DEFINITIONS
*******************************************************************************/
static const char *TAG ="Debug";
static FILE *file;
/*******************************************************************************
**                      INTERNAL VARIABLE DEFINITIONS
*******************************************************************************/
static TaskHandle_t task_handle = NULL;
/*******************************************************************************
**                      INTERNAL FUNCTION PROTOTYPES
*******************************************************************************/

/*******************************************************************************
**                          FUNCTION DEFINITIONS
*******************************************************************************/

int printf_to_sd_card(const char *fmt, va_list list)
{
  if(file == NULL)
  {
    return -1;
  }
  int res = vfprintf(file ,fmt, list);

  fsync(fileno(file));
  
  return res;
}

void log_handler(void)
{
  int fileLength = 0;
  struct stat st;

  // while(1)
  // {  
  file = fopen("/sdcard/log_wifi.txt", "a");

  if (file == NULL)
  {
    ESP_LOGE(TAG, "Failed to open file for logging!");
    printf("Failed to open file for logging1!");
  } 

  else
  {
    esp_log_set_vprintf(&printf_to_sd_card); 
  }
  
  while(1)
  {
  
    if (stat("/sdcard/log_wifi.txt", &st) == 0)
    {
      fileLength = st.st_size;
      if(fileLength >= 20000)
        {
            fclose(file);
            file=NULL;
            int ret = rename("/sdcard/log_wifi.txt", "/sdcard/put.txt");
            if (ret == 0)
            {
                //fclose(file);
                printf("The file is renamed successfully.\n");
                file = fopen("/sdcard/log_wifi.txt", "a");
                  if (file == NULL)
                    {
                      ESP_LOGE(TAG, "Failed to open file for logging!");
                      printf("Failed to open file for logging2!");
                    } 

                    else
                    {
                      esp_log_set_vprintf(&printf_to_sd_card); 
                    }
   
            }
            else
            {
                unlink("/sdcard/put.txt");
                printf("The file could not be renamed.\n");
            }
        }
    }
    else
    {
      file = fopen("/sdcard/log_wifi.txt", "a");

      if (file == NULL)
      {
        ESP_LOGE(TAG, "Failed to open file for logging!");
          printf("Failed to open file for logging3!\n");
      } 

      else
      {
        esp_log_set_vprintf(&printf_to_sd_card); 
      }
    }
      
  
  vTaskDelay(5000 / portTICK_PERIOD_MS);
  //vTaskDelete(task_handle); 
  }
}


void create_log_handle(void)
{
  if (task_handle == NULL)
  {
    xTaskCreate((TaskFunction_t) &log_handler,
                "task",
                10*1024,
                NULL,
                5,
                &task_handle);
  }
}

