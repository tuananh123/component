/**
 * @file uploader_spine_map.h
 * @author backhug team
 * @brief 
 * @version 0.1
 * @date 2022-07-05
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef __UPLOADER_SPINE_MAP_H_
#define __UPLOADER_SPINE_MAP_H_

/*******************************************************************************
**                               INCLUDES
*******************************************************************************/
#include "uploader.h"
/*******************************************************************************
**                                DEFINES
*******************************************************************************/
#define HTTP_OK 200
#define HTTP_ERROR 401
#define HTTP_UNKNOWN_WEB 404

#define HTTP_BUFFER_SIZE 512

#define HEADER_NORMAL_LENGTH 64
#define HEADER_FILE_LENGTH 82
#define TAIL_LENGTH 8



#define HTTP_DEBUG 1
#define HTTP_BOUNDARY "------------------------a636d386524243e8"
#define TAG "HTTP_CLIENT"
/*******************************************************************************
**                     EXTERNAL VARIABLE DECLARATIONS
*******************************************************************************/

/*******************************************************************************
**                     EXTERNAL FUNCTION DECLARATIONS
*******************************************************************************/
int countFileSDcard();
int UploadSdcardFile(int numFile);
#endif
