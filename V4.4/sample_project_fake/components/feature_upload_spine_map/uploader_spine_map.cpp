/**
 * @file uploader_spine_map.cpp
 * @author backhug team
 * @brief 
 * @version 0.1
 * @date 2022-21-05
 * 
 * @copyright Copyright (c) 2022
 * 
 */

/*******************************************************************************
**                                INCLUDES
*******************************************************************************/
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "esp_log.h"
#include "esp_system.h"
#include "cJSON.h"
#include <ctype.h>
#include "esp_http_client.h"
#include "downloader.h"
#include <dirent.h>
#include "esp_task_wdt.h"
#include "HttpClient.h"
#include "uploader_spine_map.h"
#include "dbg.h"
/*******************************************************************************
**                       INTERNAL MACRO DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      COMMON VARIABLE DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      INTERNAL VARIABLE DEFINITIONS
*******************************************************************************/

// char buffer[MASSAGE_ID_LENGTH + 1];
// static char nameSDcardFile[MAX_FILE_STORED_SDCARD][20];
// //static char nameSDcardFile[MAX_FILE_STORED_SDCARD][20];
// // static int countNumSDcardFile = 0;

// // production endpoint
// const char *base_url = "http://api.mybackhug.com";
// // const char *base_url = "https://api-beta.mybackhug.com";
// //const char *base_url = "https://api-alpha-v3.mybackhug.com";
// const char *new_upload_endpoint = "/api/v2/s3/new-beddata-url";
// // const char *check_valid_booking_endpoint = "/api/v2/s3/check-booking";
// // const char *check_eligible_endpoint = "/api/v2/checkin-eligible";
// const char *bed_endpoint = "/api/v2/bedlog";
// const char *bed_endpointGet = "/api/v2/bedstats";
// const char *bed_endpointGetV3 = "/api/v3/bedstats";
// // const char *bed_endpoint_notify = "api/v2/beddata";

// const char *massage_session_id = "?massage_session_id=";

/*******************************************************************************
**                      INTERNAL FUNCTION PROTOTYPES
*******************************************************************************/

/*******************************************************************************
**                          FUNCTION DEFINITIONS
*******************************************************************************/

int countFileSDcard()
{
  int countNumSDcardFile = 0;
  /* List all file name in SD card */
  struct dirent* de; // Pointer for directory entry
  // opendir() returns a pointer of DIR type.
  DIR* dr = opendir("/sdcard/");

  check(dr != NULL, "Could not open SD card");

  while ((de = readdir(dr)) != NULL)
  {
    /* Skip if file not contain TXT or BIN */
    char* tail = &(de->d_name[strlen(de->d_name) - 3]);
    if (strcmp(tail, "MSG"))
    {
      continue;
    }

    if (countNumSDcardFile < MAX_FILE_STORED_SDCARD)
    {
      strcpy(nameSDcardFile[countNumSDcardFile++], de->d_name);
    }
    else
    {
      /* code */
      break;
    }
  }

  //ESP_LOGI("SDcard", "number of file in SD card: %d", countNumSDcardFile);

  closedir(dr);
  return countNumSDcardFile;

  error:
  return 0;
}

int UploadSdcardFile(int numFile)
{
  FILE *file;
  int fileLength = 0;
  char postData[140];
  if (numFile == 0)
    return 0;
  printf("Number File: %d\n",numFile);
  for (int i = 0; i < numFile; i++)
  {
//    if (request_upload_permission() != 1)
//    {
      char massageID[MASSAGE_ID_LENGTH + 1];
      char *request_url = NULL;

      struct stat st;

      char *SDcardPATH = (char *)malloc(40);
      strcpy(SDcardPATH, "/sdcard/");
      strcat(SDcardPATH, nameSDcardFile[numFile - 1 - i]);

      if (stat(SDcardPATH, &st) == 0)
      {
        fileLength = st.st_size;
      }
      else
      {
        ESP_LOGE(TAG, "cannot link to SD card file");
      }

      printf("%s\n", SDcardPATH);

      char *tail = &SDcardPATH[strlen(SDcardPATH) - 3];
      char *req_file = (char *)"&type=";
      printf("tail: %s\n", tail);
      tail = "BIN"; //Fake tail
      file = fopen(SDcardPATH, "rb");
      if (file == NULL)
      {
        ESP_LOGE(TAG, "cannot open file in sd card");
        unlink(SDcardPATH);
        free(SDcardPATH);
        continue;
      }

      fread(massageID, sizeof(char), MASSAGE_ID_LENGTH, file);

      massageID[MASSAGE_ID_LENGTH] = '\0';
      
      printf("massageID: %s\n", massageID);
      fclose(file);
      
      // request_url: http://api.mybackhug.com/api/v2/s3/new-beddata-url?massage_session_id=000000000000000332158254&type=txt

      request_url = (char *)malloc(strlen(base_url) + strlen(new_upload_endpoint) + strlen(massageID) + strlen(massage_session_id) + strlen(req_file) + strlen(tail) + 1);

      check_mem(request_url);

      strcpy(request_url, base_url);
      strcat(request_url, new_upload_endpoint);
      strcat(request_url, massage_session_id);
      strcat(request_url, massageID);
      strcat(request_url, req_file);
      strcat(request_url, tail);

      ESP_LOGI("request_url", "%s", request_url);
      int read;
      int totalRead = 0;
      bool endMessage = false;
      int contentLength;

      char *mac_address = get_mac_address();
      char token[50];
      strcpy(token, "bed:");
      strcat(token, mac_address);

      //free(mac_address);

      file = fopen(SDcardPATH, "rb");

      if (file == NULL)
      {
        ESP_LOGE(TAG, "Could not open file to send through wifi");
        unlink(SDcardPATH);
        fclose(file);
        free(SDcardPATH);
        free(request_url);
        continue;
      }
      else
      {
        ESP_LOGI("fileLength", "%d", fileLength);
        if (fileLength == 0)
        {
          ESP_LOGE(TAG, "No file or empty file!");
          unlink(SDcardPATH);
          free(SDcardPATH);
          free(request_url);
          continue;
        }
        else
        {
          check(http_client_get_with_token(request_url, &contentLength, token) == ESP_OK, "HTTP client get faild");
          int req_status_code;
          req_status_code = http_client_get_status_code();
          ESP_LOGI(TAG, "UPLOAD FILE, GET status:%d", req_status_code);
          
          //FIXME: Test content length in case lost wifi connection
          if (req_status_code == 200) //  HTTP OK
          {
            char *testBuffer = (char *)malloc(contentLength + 1);
            strcpy(testBuffer, "");

            http_client_read_with_contentLength(testBuffer, &read, &endMessage, contentLength);
#if HTTP_DEBUG
            ESP_LOGI("SD", "F Write");
#endif

            totalRead += read;
#if HTTP_DEBUG
            ESP_LOGI("HTTP", "Read = %d Total = %d,", read, totalRead);
#endif
            testBuffer[contentLength] = '\0';
            // ESP_LOGI("testBuffer", "%s", testBuffer);
            http_client_end();
           
            // if (contentLength > 0)
            // {
            //TODO: Check if really finish above get the data then go to this parse JSON,
            //cause reset if this JSON string is null, case token is expired?

            cJSON *root = cJSON_Parse(testBuffer);
            // char *object = cJSON_GetObjectItem(root, "object")->valuestring;
            cJSON *info = cJSON_GetObjectItem(root, "info");
            char *url = cJSON_GetObjectItem(info, "url")->valuestring;
            cJSON *fields = cJSON_GetObjectItem(info, "fields");
            char *x_amz_algorithm = cJSON_GetObjectItem(fields, "x-amz-algorithm")->valuestring;
            char *x_amz_date = cJSON_GetObjectItem(fields, "x-amz-date")->valuestring;
            char *key = cJSON_GetObjectItem(fields, "key")->valuestring;
            // printf("Key: %s\n", key);
            // strcat(key, tail);
            // printf("key: %s\n", key);
            char *x_amz_credential = cJSON_GetObjectItem(fields, "x-amz-credential")->valuestring;
            char *policy = cJSON_GetObjectItem(fields, "policy")->valuestring;
            char *x_amz_signature = cJSON_GetObjectItem(fields, "x-amz-signature")->valuestring;

            char contentTypeStr[50] = "multipart/form-data; boundary=";
            char headerValue[10];
            int fullContentLength = fileLength + 2000;

            itoa(fullContentLength, headerValue, 10);
            strcat(contentTypeStr, HTTP_BOUNDARY);
            http_client_init(url, &contentLength);
            http_client_set_header((char *)"Content-Length", headerValue);
            http_client_set_header((char *)"Content-Type", contentTypeStr);
            http_client_set_post();
            http_client_open(fullContentLength);

            //=============================================================================================
            const char *fileName = nameSDcardFile[numFile - 1 - i];
            // strcat(fileName, tail);

            // printf("name: %s\n", fileName);
            // TODO: Optimize the length for each header request

            //key header for x-amz-algorithm
            char *keyHeader = (char *)malloc(strlen(x_amz_algorithm) + 140);
            check_mem(keyHeader);
            strcpy(keyHeader, "--");
            strcat(keyHeader, HTTP_BOUNDARY);
            strcat(keyHeader, "\r\n");
            strcat(keyHeader, "Content-Disposition: form-data; name=\"x-amz-algorithm\"\r\n\r\n");
            strcat(keyHeader, x_amz_algorithm);
            strcat(keyHeader, "\r\n");

            check(http_client_write(keyHeader, strlen(keyHeader)) != -1, "HTTP client write filed");
            free(keyHeader);

            keyHeader = (char *)malloc(strlen(x_amz_date) + 140);
            check_mem(keyHeader);
            strcpy(keyHeader, "--");
            strcat(keyHeader, HTTP_BOUNDARY);
            strcat(keyHeader, "\r\n");
            strcat(keyHeader, "Content-Disposition: form-data; name=\"x-amz-date\"\r\n\r\n");
            strcat(keyHeader, x_amz_date);
            strcat(keyHeader, "\r\n");

            check(http_client_write(keyHeader, strlen(keyHeader)) != -1, "HTTP client write filed");
            free(keyHeader);

            keyHeader = (char *)malloc(strlen(key) + 140);
            check_mem(keyHeader);
            strcpy(keyHeader, "--");
            strcat(keyHeader, HTTP_BOUNDARY);
            strcat(keyHeader, "\r\n");
            strcat(keyHeader, "Content-Disposition: form-data; name=\"key\"\r\n\r\n");
            strcat(keyHeader, key);
            strcat(keyHeader, "\r\n");

            check(http_client_write(keyHeader, strlen(keyHeader)) != -1, "HTTP client write filed");
            free(keyHeader);

            // key header for x-amz-algorithm
            keyHeader = (char *)malloc(strlen(x_amz_credential) + 140);
            check_mem(keyHeader);
            strcpy(keyHeader, "--");
            strcat(keyHeader, HTTP_BOUNDARY);
            strcat(keyHeader, "\r\n");
            strcat(keyHeader, "Content-Disposition: form-data; name=\"x-amz-credential\"\r\n\r\n");
            strcat(keyHeader, x_amz_credential);
            strcat(keyHeader, "\r\n");
            // ESP_LOGI(TAG, "keyHeader: %s", keyHeader);
            check(http_client_write(keyHeader, strlen(keyHeader)) != -1, "HTTP client write filed");
            free(keyHeader);

            //key header for x-amz-algorithm
            keyHeader = (char *)malloc(strlen(policy) + 140);
            check_mem(keyHeader);
            strcpy(keyHeader, "--");
            strcat(keyHeader, HTTP_BOUNDARY);
            strcat(keyHeader, "\r\n");
            strcat(keyHeader, "Content-Disposition: form-data; name=\"policy\"\r\n\r\n");
            strcat(keyHeader, policy);
            strcat(keyHeader, "\r\n");
            // ESP_LOGI(TAG, "keyHeader: %s", keyHeader);
            check(http_client_write(keyHeader, strlen(keyHeader)) != -1, "HTTP client write filed");
            free(keyHeader);

            //key header for x-amz-algorithm
            keyHeader = (char *)malloc(strlen(x_amz_signature) + 140);
            check_mem(keyHeader);
            strcpy(keyHeader, "--");
            strcat(keyHeader, HTTP_BOUNDARY);
            strcat(keyHeader, "\r\n");
            strcat(keyHeader, "Content-Disposition: form-data; name=\"x-amz-signature\"\r\n\r\n");
            strcat(keyHeader, x_amz_signature);
            strcat(keyHeader, "\r\n");
            // ESP_LOGI(TAG, "keyHeader: %s", keyHeader);
            check(http_client_write(keyHeader, strlen(keyHeader)) != -1, "HTTP client write filed");
            free(keyHeader);

            //request header for file upload
            char contentType[] = "application/octet-stream";
            char *requestHead = (char *)malloc(240);
            check_mem(requestHead);
            strcpy(requestHead, "--");
            strcat(requestHead, HTTP_BOUNDARY);
            strcat(requestHead, "\r\n");
            strcat(requestHead, "Content-Disposition: form-data; name=\"file\"; filename=\"");
            strcat(requestHead, fileName);
            strcat(requestHead, "\"\r\n");
            strcat(requestHead, "Content-Type: ");
            strcat(requestHead, contentType);
            strcat(requestHead, "\r\n\r\n");
            // ESP_LOGI(TAG, "requestHead: %s", requestHead);
            check(http_client_write(requestHead, strlen(requestHead)) != -1, "HTTP client write filed");
            free(requestHead);

            cJSON_Delete(root);

            free(testBuffer);

            //FIXME: Test only, send some data via http:
            char *buffer = (char *)malloc(HTTP_BUFFER_SIZE);
            check_mem(buffer);
            unsigned fileProgress = 0;

            int length = 1;
            // while (length)
            for (int i = 0; i < ((fileLength / 512) + 1); i++)
            {
              length = fread(buffer, sizeof(char), HTTP_BUFFER_SIZE, file);
              if (length)
              {
                int wret = http_client_write(buffer, length);
                check(wret > 0, "HTTP client write failed");
                fileProgress += wret;
                ESP_LOGI(TAG, "write file:\t%d/%d/%d/%d", wret, length, fileProgress, fileLength);
              }
              else
              {
                ESP_LOGI(TAG, "write file:\t%d/%d/%d", length, fileProgress, fileLength);
              }
            }
            printf("heap_spine:%d\n",esp_get_free_heap_size());
            free(buffer);

            //request tail
            char *tail = (char *)malloc(90);
            check_mem(tail);
            strcpy(tail, "\r\n--");
            strcat(tail, HTTP_BOUNDARY);
            strcat(tail, "--\r\n\r\n");
            // ESP_LOGI(TAG, "tail: %s", tail);
            check(http_client_write(tail, strlen(tail)) != -1, "HTTP client write failed");
            //http_client_end();

            free(tail);

            //update backend upload massage_session success
            strcpy(request_url, base_url);
            strcat(request_url, new_upload_endpoint);
            strcat(request_url, massage_session_id);
            strcat(request_url, massageID);
            sprintf(postData, "{\"success_uploaded\": 1}"); 
            if (http_client_put_file_with_token(request_url, token, postData) != ESP_OK)
            {
              ESP_LOGE(TAG, "PUT failed");
            }

            http_client_end();
            printf("success upload massage_session \n");

            // }
            unlink(SDcardPATH); // remove file in SD card
            free(SDcardPATH);
            fclose(file);
            free(request_url);
          }
          else // server error, return
          {
            erare_file_sd_card(SDcardPATH);
            free(SDcardPATH);
            fclose(file);
            free(request_url);
            return 1;
          }
          vTaskDelay(1000 / portTICK_PERIOD_MS);
        }
      }
//    }
//    else // somehow server did not respond, return
//    {
//      return 1;
//    }
  }

  //printf("End of sending files\n");
  return 0;

error:
  //save_reset_predicted(1);
  esp_restart();
  return -1;
}



