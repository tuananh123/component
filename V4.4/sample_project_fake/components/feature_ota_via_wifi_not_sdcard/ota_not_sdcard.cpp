/**
 * @file ota_not_sdcard.cpp
 * @author backhug team
 * @brief 
 * @version 0.1
 * @date 2022-27-07
 * 
 * @copyright Copyright (c) 2022
 * 
 */

/*******************************************************************************
**                                INCLUDES
*******************************************************************************/
#include <cstring>
#include <cstdio>
#include <cstring>
#include <sstream>
#include "esp_system.h"

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <rom/md5_hash.h>
#include "esp_ota_ops.h"
#include "ota.h"
#include "nvs.h"
#include "esp_log.h"
#include "wifi_sta.h"
#include "HttpClient.h"
#include "ota_not_sdcard.h"

/*******************************************************************************
**                       INTERNAL MACRO DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      COMMON VARIABLE DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      INTERNAL VARIABLE DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      INTERNAL FUNCTION PROTOTYPES
*******************************************************************************/

/*******************************************************************************
**                          FUNCTION DEFINITIONS
*******************************************************************************/

void get_firmware_and_ota(p_firmware_t p_firmware)
{
  MD5Context ctx;
  memset(&ctx, 0, sizeof(ctx));
  MD5Init(&ctx);

  nvs_get_md5(p_firmware->md5, p_firmware->md5_nvs_str);
  printf("downloading firmwares ...\n");
  int total_read = 0, read, content_length;
  bool end_message = false;
  uint16_t index=0;
  /* Get firmware from back-end */
  esp_err_t ret = http_client_init(p_firmware->url_version, &content_length);
  if (ret != ESP_OK) printf("failed to dowload firmware from sever");
  char *buffer = (char *)malloc(MAX_HTTP_RECV_BUFFER + 1);
  ota_init(); /* Init partion OTA */
  while (!end_message)
  {
    /* Read from sever and write to partion OTA */
    http_client_read(buffer, &read, &end_message);
    MD5Update(&ctx,(unsigned char*) buffer, read); /* Caculate MD5 for Data Input */
    ota_handle((void*)buffer, read);
    total_read += read;
  }

  /* Check if correct firmware size downloaded */
  if (total_read != content_length) 
   printf("\nIncorrect firmware size dowloaded");

  unsigned char digest[16];
  memset(digest, 0, sizeof(digest));
  MD5Final(digest, &ctx);
  std::stringstream ss;
  
  for (int x = 0; x < 16; x++)
  {
    if (digest[x] < 0x10)
      ss << std::hex << (int)0;
    ss << std::hex << (int)digest[x];
  }
  
  std::string mystr = ss.str();
  ESP_LOGI("Digest", "%s<", mystr.c_str());/* Convert string type C++ to type C */
  ESP_LOGI("MD5", "%s<", p_firmware->md5);
  
  /* store firmware version to NVS */
  if (strcmp(mystr.c_str(), p_firmware->md5) == 0)
  {
    nvs_store_version((uint8_t *) p_firmware->url_version, p_firmware->version_nvs_str);
    ota_end();  
  }
  
  http_client_end();
  free(buffer);

  return;
}

void download_firmware_notsdcard(p_firmware_t p_firmware, uint8_t* version)
{
  bool ret;
 
  /* Check for downloading required */
  if (get_connection_state() == false) return;
  ret = check_url_version(p_firmware,version);
  if (ret == true)
  {
    /* Download new firmware and MD5 */
    
    download_md5(p_firmware);
    get_firmware_and_ota(p_firmware);
  }
  else
    printf("failed to dowload firmware!");  

}


