/**
 * @file nvs.cpp
 * @author backhug team
 * @brief 
 * @version 0.1
 * @date 2022-07-05
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef __UPLOADER_H_
#define __UPLOADER_H_

/*******************************************************************************
**                               INCLUDES
*******************************************************************************/
#include "downloader.h"
/*******************************************************************************
**                                DEFINES
*******************************************************************************/

/*******************************************************************************
**                     EXTERNAL VARIABLE DECLARATIONS
*******************************************************************************/
extern char nameSDcardFile[MAX_FILE_STORED_SDCARD][20];
extern const char *base_url;
extern const char *new_upload_endpoint;
extern const char *bed_endpoint;
extern const char *bed_endpointGet;
extern const char *bed_endpointGetV3;
extern const char *massage_session_id;
extern const char *name;
extern const char *file_type;
extern const char *chip_wifi;
extern const char *base_url_log;
extern const char *chip_ble;
/*******************************************************************************
**                     EXTERNAL FUNCTION DECLARATIONS
*******************************************************************************/
char *get_mac_address();
int upload_bed_stat();
int request_upload_permission();
int request_upgrade_permission(char *typeChip);
void erare_file_sd_card(char* SDcardPATH );

#endif
