/**
 * @file PWM.h
 * @author backhug team
 * @brief 
 * @version 0.1
 * @date 2022-09-06
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#ifndef PWM_H_
#define PWM_H_
/*******************************************************************************
**                                INCLUDES
*******************************************************************************/
#include <driver/gpio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <lwip/api.h>
#include <esp32/rom/gpio.h>
#include "driver/ledc.h"
#include <esp_log.h>

/*******************************************************************************
**                                DEFINES
*******************************************************************************/

//Pulse-width modulation
#define PWM_PIN1                     GPIO_NUM_21
#define PWM_PIN2                     GPIO_NUM_22

#define DISABLE_PIN                  GPIO_NUM_25

// PWM Settings
#define PWM_FREQ                20000           // Frequency of the PWM (HZ)
#define PWM_DUTY_RES        LEDC_TIMER_10_BIT   // Duty Resolution of the PWM

/*******************************************************************************
**                     EXTERNAL VARIABLE DECLARATIONS
*******************************************************************************/

/**
 * @brief  Creates a context for PWM to store it's configuration.
 * One of these needs to be created before using any of the methods.
 */
typedef struct pwm_config_t
{
    ledc_timer_config_t tConfig;    /**< Configuration of the PWM */
    ledc_channel_config_t lChannel; /**< The channel to run PWM on */
} pwm_context_t;

/*******************************************************************************
**                     EXTERNAL FUNCTION DECLARATIONS
*******************************************************************************/

#ifdef __cplusplus
extern "C"
{
#endif
/**
 * @brief  initPWM: Creates a PWM handler from a given PWM context.
 * 
 * @param pwm_context      Used to hold PWM config and channel
 * @return esp_err_t
 */
esp_err_t initPWM(pwm_context_t *pwm_context);

/**
 * @brief  setDutyPWM: Sets the duty cycle from a given PWM context.
 * 
 * @param pwm_context      Used to hold PWM config and channel
 * @param duty             To set the duty cycle of PWM 
 * @return esp_err_t
 */
esp_err_t setDutyPWM(pwm_context_t *pwm_context, uint32_t duty);

/**
 * @brief  setDutyPercentPWM: Sets the duty cycle as a percentage of the duty resolution from a given PWM context.
 * 
 * @param pwm_context       Used to hold PWM config and channel
 * @param duty_percent      The percentage of the duty resolution to set
 * @return esp_err_t
 */
esp_err_t setDutyPercentPWM(pwm_context_t *pwm_context, uint32_t duty_percent);

/**
 * @brief  setFreqPWM: Set the frequency (HZ) from a given PWM context.
 * 
 * @param pwm_context      Used to hold PWM config and channel
 * @param freq              The percentage of the duty resolution to set
 * @return esp_err_t
 */
esp_err_t setFreqPWM(pwm_context_t *pwm_context, uint32_t freq);

/** 
 * Configure and initalise the pwm component
 */
esp_err_t pwm_init(pwm_context_t* pwm, int gpio, ledc_channel_t channel);

/**
 * @brief  task process handle pwm
 * 
 * @param  None      
 * @param  None
 * @return None
 */
void create_PWM_handle(void);
#ifdef __cplusplus
}
#endif

#endif