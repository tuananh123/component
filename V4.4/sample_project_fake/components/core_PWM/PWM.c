/**
 * @file PWM.c
 * @author backhug team
 * @brief 
 * @version 0.1
 * @date 2022-09-06
 * 
 * @copyright Copyright (c) 2022
 * 
 */

/*******************************************************************************
**                                INCLUDES
*******************************************************************************/
#include <stdio.h>
#include "PWM.h"
#include <math.h>
#include <stdlib.h>
#include "esp_system.h"

#define PWM_DEBUG 0
pwm_context_t pwm1;
pwm_context_t pwm2;
static TaskHandle_t task_handle = NULL;

esp_err_t initPWM(pwm_context_t *pwm_context)
{
    ledc_timer_config(&(pwm_context->tConfig));
    ledc_channel_config(&(pwm_context->lChannel));
    return ledc_fade_func_install(0);
}

esp_err_t setDutyPWM(pwm_context_t *pwm_context, uint32_t duty)
{
    ledc_set_duty(pwm_context->tConfig.speed_mode, pwm_context->lChannel.channel, duty);
    return ledc_update_duty(pwm_context->tConfig.speed_mode, pwm_context->lChannel.channel);
}

esp_err_t setDutyPercentPWM(pwm_context_t *pwm_context, uint32_t duty_percent)
{
    int dutyRes = (pow(2, pwm_context->tConfig.duty_resolution)) * (duty_percent / 100.0f);
    ledc_set_duty(pwm_context->tConfig.speed_mode, pwm_context->lChannel.channel, dutyRes);
    return ledc_update_duty(pwm_context->tConfig.speed_mode, pwm_context->lChannel.channel);
}

esp_err_t setFreqPWM(pwm_context_t *pwm_context, uint32_t freq)
{
    return ledc_set_freq(pwm_context->tConfig.speed_mode, pwm_context->tConfig.timer_num, freq);
}

esp_err_t pwm_init(pwm_context_t* pwm, int gpio, ledc_channel_t channel_num)
{
    esp_err_t err;

    ledc_timer_config_t timer =
        {
            .speed_mode = LEDC_LOW_SPEED_MODE,
            .duty_resolution = PWM_DUTY_RES,
            .timer_num = LEDC_TIMER_0,
            .freq_hz = PWM_FREQ,
            .clk_cfg = LEDC_AUTO_CLK
        };

    ledc_channel_config_t channel =
        {
            .gpio_num = gpio,
            .speed_mode = LEDC_LOW_SPEED_MODE,
            .channel = channel_num,
            .timer_sel = LEDC_TIMER_0,
            .duty = 0,
            .hpoint = 0
        };

    pwm->lChannel = channel;
    pwm->tConfig = timer;

    err = initPWM(pwm);
    err = setDutyPercentPWM(pwm, 0);
    return err;
}

void pwm_handle(void)
{
    // initialise the PWM pins
    pwm_init(&pwm1, PWM_PIN1, LEDC_CHANNEL_0);
    pwm_init(&pwm2, PWM_PIN2, LEDC_CHANNEL_1);

    // configure the disable line as an output
    gpio_set_direction(DISABLE_PIN, GPIO_MODE_OUTPUT);

    while (1)
    {
        // Set disable line high
        gpio_set_level(DISABLE_PIN, 1);

        // drive first pwm for 35 seconds
        setDutyPercentPWM(&pwm1, 50);
        vTaskDelay(35000 / portTICK_PERIOD_MS);
        setDutyPercentPWM(&pwm1, 0);
        vTaskDelay(10 / portTICK_PERIOD_MS);

        // drive second pin for 35 seconds
        setDutyPercentPWM(&pwm2, 50);
        vTaskDelay(35000 / portTICK_PERIOD_MS);
        setDutyPercentPWM(&pwm2, 0);
        vTaskDelay(10 / portTICK_PERIOD_MS);

        // set disable line low
        gpio_set_level(DISABLE_PIN, 0);

    }
}

void create_PWM_handle(void)
{
  if (task_handle == NULL)
  {
    xTaskCreate((TaskFunction_t) &pwm_handle,
                "task",
                4*1024,
                NULL,
                5,
                &task_handle);

  }
}
