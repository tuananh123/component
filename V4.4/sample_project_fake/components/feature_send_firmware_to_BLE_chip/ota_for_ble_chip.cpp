/**
 * @file: ota_for_ble_chip.cpp
 * @author backhug team
 * @brief 
 * @version 0.1
 * @date 2022-07-05
 * 
 * @copyright Copyright (c) 2022
 * 
 */
/************************************************************************
 *                              INCLUDES                                 *
 ************************************************************************/
#include <stdlib.h>
#include "esp_system.h"

#include "spi_comm.h"
#include "communication.h"
#include "ota_for_ble_chip.h"
#include "nvs.h"
#include "main.h"

/*******************************************************************************
**                       INTERNAL MACRO DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      COMMON VARIABLE DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      INTERNAL VARIABLE DEFINITIONS
*******************************************************************************/

// static FILE *file = NULL;
uint32_t file_size = 0;
static packet buffer_packet;
bool send_firmware_ble = false;

// static char md5[32] = {0};
// static char url_version[100] = {0};

/*******************************************************************************
**                      INTERNAL FUNCTION PROTOTYPES
*******************************************************************************/

/*******************************************************************************
**                          FUNCTION DEFINITIONS
*******************************************************************************/

static bool send_update_request(p_firmware_t p_firmware)
{
  buffer_packet.cmd = CMD_UPDATE_REQ;
  buffer_packet.data_len = strlen(p_firmware->url_version) + 1;
  memcpy(buffer_packet.payload, p_firmware->url_version, buffer_packet.data_len);
  buffer_packet.crc = crc16_calculate(buffer_packet);  
  printf("URL:%s \n",p_firmware->url_version);
  /* Send request and check for updating allow */
  master_comm_transmit(buffer_packet);
  if (!master_comm_recv(&buffer_packet) || buffer_packet.cmd != RESPOND_ACK)
    return false;

  return true;
}

static bool send_update_params(p_firmware_t p_firmware)
{
  file_size = MageControl::Storage::SecureDigital::fSize("BLE.BIN");
  nvs_get_md5(p_firmware->md5, p_firmware->md5_nvs_str);
  printf("MD5:%s \n",p_firmware->md5);
  buffer_packet.cmd = CMD_UPDATE_PARAMS;
  buffer_packet.data_len = strlen(p_firmware->md5) + sizeof(file_size);
  memcpy(&buffer_packet.payload[0], p_firmware->md5, strlen(p_firmware->md5));
  memcpy(&buffer_packet.payload[32], (uint8_t *) &file_size, sizeof(file_size));
  buffer_packet.crc = crc16_calculate(buffer_packet);
  
  /* Send update params and check respond */
  master_comm_transmit(buffer_packet);
  if (!master_comm_recv(&buffer_packet) || buffer_packet.cmd != RESPOND_ACK)
    return false;
  
  return true;
}

static bool send_firmware_payload(p_firmware_t p_firmware)
{
  FILE *file;
  uint32_t bytes_written = 0;

  file = fopen("/sdcard/BLE.BIN", "r");
  if (file == NULL) return false;

  while (bytes_written < file_size)
  {
    uint16_t bytes_to_trans = (((file_size - bytes_written) > OTA_PAYLOAD)
                              ? OTA_PAYLOAD : (file_size - bytes_written));
    
    buffer_packet.cmd = CMD_UPDATE_DATA;
    buffer_packet.data_len = bytes_to_trans;
    fread(buffer_packet.payload, sizeof(char), bytes_to_trans, file);
    buffer_packet.crc = crc16_calculate(buffer_packet);
    master_comm_transmit(buffer_packet);
    
    /* Transfer the payload and check respond */
    if (!master_comm_recv(&buffer_packet) || buffer_packet.cmd != RESPOND_ACK)
    {
      fclose(file);
      return false;
    }
    
    bytes_written += bytes_to_trans;
    if (bytes_written % 51200 == 0)
      printf("SEND_FIRMWARE: 51200 BYTES SENT\n");
  }

  fclose(file);
  return true;
}

static void send_terminate_process(void)
{
  buffer_packet.cmd = CMD_TERMINATE;
  buffer_packet.data_len = 0;
  buffer_packet.crc = crc16_calculate(buffer_packet);

  master_comm_transmit(buffer_packet);

  printf("MASTER: Finish transaction\n");
}

bool spi_transfer_firmware(p_firmware_t p_firmware)
{
  bool ret;

  /* Send update request: URL version */
  ret = send_update_request(p_firmware);
  if (ret == false) return false;
  printf("UPDATE_REQ: ALLOW!!\n");

  /* Send update params: MD5 + Size */
  ret = send_update_params(p_firmware);
  if (ret == false) return false;
  printf("UPDATE_PARAMS: SEND!!!\n");

  /* Send all firmware */
  ret = send_firmware_payload(p_firmware);
  if (ret == false) return false;
  printf("SEND_FIRMWARE: FINISHED!!!\n");

  send_terminate_process();
  return true;
}
void ota_spi(void)
{
  bool ret;
  if(send_firmware_ble)
  {
    printf("BLE upgrade start\n");
    int count = 0;
    while (count < 10)
    {
      vTaskDelay(100 / portTICK_PERIOD_MS);
      ret = spi_transfer_firmware(&ble_firmware);
      if (ret)
      {
        printf("BLE upgrade successed\n");
        break;
      }
      count++;
    }
    send_firmware_ble = false;
  }
}