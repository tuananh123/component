/*
 * stringop.h
 *
 *  Created on: 1 Jun 2018
 *      Author: boyan
 */

#ifndef MAIN_SRC_STRINGOP_H_
#define MAIN_SRC_STRINGOP_H_


#include <cstdio>
#include <cstdarg>

namespace MageControl 
{
///
/// @brief Namespace contains string operators for old c-style null terminated strings
namespace StringOp 
{


///
/// @brief returns the number of characters that sprintf would need
/// in a buffer to construct a string with the semantics of printf()
///
/// @param _fmt format string, with same rules as the printf() family
/// @param ... a variadic list of arguments, to fill in the string
///
/// @return size_t length of the string, if it was to be constructed
///
size_t c99_ctsprintf(const char * _fmt, ...);

}
}


#endif /* MAIN_SRC_STRINGOP_H_ */
