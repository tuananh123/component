/**
 * @file wifi_sta.c
 * @author backhug team
 * @brief 
 * @version 0.1
 * @date 2022-07-07
 * 
 * @copyright Copyright (c) 2022
 * 
 */

/************************************************************************
 *                              INCLUDES                                *
 ************************************************************************/
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_log.h"
#include "esp_event_loop.h"
#include "esp_wifi.h"
#include "nvs.h"
#include "string.h"

#include "wifi_sta.h"
#include "esp_wps.h"
#include "communication.h"
#include "spi_comm.h"
#include "esp_event_legacy.h"
#include "esp_log.h"
/*******************************************************************************
**                       INTERNAL MACRO DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      COMMON VARIABLE DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      INTERNAL VARIABLE DEFINITIONS
*******************************************************************************/
static esp_wps_config_t config = WPS_CONFIG_INIT_DEFAULT(WPS_TYPE_PBC);

/* Wifi connected status */
static bool disconnected = false;
static wifi_sta_type_t mode;
static wifi_sta_state_t wifi_state = WIFI_DISCONECTED;

/* FreeRTOS event group to signal when we are connected*/
static EventGroupHandle_t wifi_event_group;
static bool loop_event_init = false;
static const char* TAG = "WIFI STA";

/* BIT identity if ESP connected to AP with an IP provided */
// const int WIFI_CONNECTED_BIT = BIT0;

/* Wifi scan mode*/
static bool got_scan_done_event= false;
static uint16_t number = DEFAULT_SCAN_LIST_SIZE;
wifi_ap_record_t ap_info[DEFAULT_SCAN_LIST_SIZE];
uint16_t ap_count;
char *TAG1 = "WIFI STATE";
/*******************************************************************************
**                      INTERNAL FUNCTION PROTOTYPES
*******************************************************************************/

/************************************************************************
 *                         FUNCTIONS DEFENITION                         *
 ************************************************************************/
static void save_wifi_to_nvs(char* wifi_ssid, char* wifi_pass)
{ 
  nvs_handle my_handle;
  nvs_open("storage", NVS_READWRITE, &my_handle);

  nvs_set_str(my_handle, "SSID", wifi_ssid);
  nvs_set_str(my_handle, "PASS", wifi_pass);

  nvs_set_u8(my_handle, "WIFI_EXIST", 1);

  nvs_set_u8(my_handle, "WPS_EXIST", 0);


  nvs_commit(my_handle);
  nvs_close(my_handle);
}

static void clear_wps_exist()
{
  nvs_handle my_handle;
  nvs_open("storage", NVS_READWRITE, &my_handle);
  nvs_set_u8(my_handle, "WPS_EXIST", 0);
  nvs_commit(my_handle);
  nvs_close(my_handle);
}

static esp_err_t event_handler(void* ctx, system_event_t* event)
{
  //system_event_sta_disconnected_t *disconnected = (system_event_sta_disconnected_t*)event;
  wifi_config_t wifi_cfg;
  loop_event_init = true;

  switch (event->event_id)
  {
    case SYSTEM_EVENT_STA_START:
      ESP_LOGI(TAG, "SYSTEM_EVENT_STA_START");
      if (mode == WIFI_STA_NORMAL)
      {
        ESP_ERROR_CHECK(esp_wifi_connect());
        wifi_state = WIFI_CONNECTING;
      }
      break;

    case SYSTEM_EVENT_STA_GOT_IP:
      ESP_LOGI(TAG, "SYSTEM_EVENT_STA_GOT_IP: %s",
               ip4addr_ntoa(&event->event_info.got_ip.ip_info.ip));

      esp_wifi_get_config(ESP_IF_WIFI_STA, &wifi_cfg);
      save_wifi_to_nvs((char*) wifi_cfg.sta.ssid,
                       (char*) wifi_cfg.sta.password);
      wifi_state = WIFI_CONNECTED;
      // error_code &= ~(1 << WIFI_ERROR);
      break;

    case SYSTEM_EVENT_STA_DISCONNECTED: //todo
      ESP_LOGW(TAG, "SSID: %s & Reason code: %d\n",event->event_info.disconnected.ssid,event->event_info.disconnected.reason);
      if (!disconnected)
        ESP_ERROR_CHECK(esp_wifi_connect());

      wifi_state = WIFI_DISCONECTED;

      // error_code |= (1 << WIFI_ERROR);
      printf("WIFI DISCONNECTED \n");
      break;

    case SYSTEM_EVENT_STA_WPS_ER_SUCCESS:
      wifi_state = WIFI_CONNECTING;
      ESP_LOGI(TAG, "SYSTEM_EVENT_STA_WPS_ER_SUCCESS");
      ESP_ERROR_CHECK(esp_wifi_wps_disable());
      ESP_ERROR_CHECK(esp_wifi_connect());
      break;

    case SYSTEM_EVENT_STA_WPS_ER_FAILED:
      ESP_LOGI(TAG, "SYSTEM_EVENT_STA_WPS_ER_FAILED");
      ESP_ERROR_CHECK(esp_wifi_wps_disable());
      ESP_ERROR_CHECK(esp_wifi_wps_enable(&config));
      ESP_ERROR_CHECK(esp_wifi_wps_start(0));
      wifi_state = WIFI_WPS_FAILE;
      break;
      
    case SYSTEM_EVENT_STA_WPS_ER_TIMEOUT:
      ESP_LOGI(TAG, "SYSTEM_EVENT_STA_WPS_ER_TIMEOUT");
      ESP_ERROR_CHECK(esp_wifi_wps_disable());
      clear_wps_exist();
      wifi_state = WIFI_WPS_TIMOUT;
      break;
    case SYSTEM_EVENT_SCAN_DONE:
      got_scan_done_event = true;
      break;     
    default:
      break;
  }
  return ESP_OK;
}

bool check_data_emty(uint8_t* data, uint8_t len)
{
  uint8_t index;

  for (index = 0; index < len; index++)
    if (data[index] != 0) return false;
  return true;
}

void wifi_init_sta(void)
{

  wifi_event_group = xEventGroupCreate();
  tcpip_adapter_init();
  if (!loop_event_init)
  {
    ESP_ERROR_CHECK(esp_event_loop_init(event_handler, NULL));
  }
  /* Must be called before calling other Wifi functions */
  wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
  ESP_ERROR_CHECK(esp_wifi_init(&cfg));
}

void wifi_start_wps(void)
{
  mode = WIFI_STA_WPS;
  /* Connect to AP with the above config */
  ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
  ESP_ERROR_CHECK(esp_wifi_start());

  ESP_LOGI(TAG, "start wps...");

  vTaskDelay(1000/portTICK_PERIOD_MS);
    
  ESP_ERROR_CHECK(esp_wifi_wps_enable(&config));
  ESP_ERROR_CHECK(esp_wifi_wps_start(0));
  wifi_state = WIFI_WPS_SCAN;
  disconnected = false;
}

static void perform_scan(void)
{
  number = DEFAULT_SCAN_LIST_SIZE;
  ap_count = 0;
  memset(ap_info, 0, sizeof(ap_info));
  ESP_ERROR_CHECK(esp_wifi_scan_get_ap_records(&number, ap_info));
  ESP_ERROR_CHECK(esp_wifi_scan_get_ap_num(&ap_count));
  ESP_LOGI(TAG, "Total APs scanned = %u", ap_count);
  // for (int i = 0; (i < DEFAULT_SCAN_LIST_SIZE) && (i < ap_count); i++) {
  //     ESP_LOGI(TAG, "SSID \t\t%s", ap_info[i].ssid);
  //     ESP_LOGI(TAG, "RSSI \t\t%d", ap_info[i].rssi);
  // }
}

void wifi_scan_sta(void)
{
  mode = WIFI_STA_SCAN;
  ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
  ESP_ERROR_CHECK(esp_wifi_start());

  //vTaskDelay(1000 / portTICK_RATE_MS);

  ESP_ERROR_CHECK(esp_wifi_scan_start(NULL, true));  

  perform_scan();

  disconnected = false;
}

void wifi_start(char* ap_ssid, char* ap_pass)
{
  mode = WIFI_STA_NORMAL;
  uint8_t mac_addr[6] = {0};
  /* Initial wifi config value */
  wifi_config_t wifi_cfg = {.sta = {.ssid = ESP_STA_SSID,},};
  // wifi_config_t wifi_cfg = {.sta = {.password = ESP_STA_PASW,},};
  // strcpy((char*) wifi_cfg.sta.ssid, (char*)"VAIS");
  // strcpy((char*) wifi_cfg.sta.password, (char*)"Vais@2020");
  // /* Setup wifi config param to connect to AP */
  if (ap_ssid)
  {
    strcpy((char*) wifi_cfg.sta.ssid, (char*) ap_ssid);
  }

  if (ap_pass)
  {
    strcpy((char*) wifi_cfg.sta.password, (char*) ap_pass);
  }

  /* Connect to AP with the above config */
  esp_wifi_set_mode(WIFI_MODE_STA);
  //wifi_set_mac_addr();
  esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_cfg);
  esp_wifi_start();
  esp_wifi_get_mac(ESP_IF_WIFI_STA, mac_addr);

  disconnected = false;
}

void wifi_disconnect(void)
{
  if (get_connection_state())
  {
    disconnected = true;
    wifi_state = WIFI_DISCONECTED;
    esp_wifi_disconnect();
  }
}

void wifi_connect(void)
{
  disconnected = false;
  wifi_state = WIFI_DISCONECTED;
  esp_wifi_connect();
}

void wifi_stop(void)
{
  if (wifi_state == WIFI_WPS_SCAN)
    ESP_ERROR_CHECK(esp_wifi_wps_disable());
  disconnected = true;
  wifi_state = WIFI_DISCONECTED;
  esp_wifi_stop();
}

static bool wifi_detail_exist(void)
{
  uint8_t exist = 0;
  nvs_handle my_handle;

  nvs_open("storage", NVS_READWRITE, &my_handle);

  if (nvs_get_u8(my_handle, "WIFI_EXIST", &exist) == ESP_OK && exist)
  {
    nvs_close(my_handle);
    return true;
  }

  nvs_close(my_handle);
  return false;
}

bool wifi_check_mac_address(void)
{
  uint8_t exist = 0;
  nvs_handle my_handle;

  nvs_open("storage", NVS_READWRITE, &my_handle);

  if (nvs_get_u8(my_handle, "WIFI_MAC_EXIST", &exist) == ESP_OK && exist)
  {
    nvs_close(my_handle);
    return true;
  }

  nvs_close(my_handle);
  return false;
}


/*
 * Set Mac Address of wifi.
 * For the first time, Mac address will be get from Slave through
 * spi, then storage in nvs memory.
 */
bool wifi_set_mac_addr(void)
{
  nvs_handle my_nvs_handle;
  size_t required_size;
  char temp[7] = {0};
  uint8_t mac_addr[6] = {0};

  nvs_open("storage", NVS_READWRITE, &my_nvs_handle);
  /* Get default AP's WIFI_MAC */
  nvs_get_str(my_nvs_handle, "WIFI_MAC", NULL, &required_size);
  nvs_get_str(my_nvs_handle, "WIFI_MAC", temp, &required_size);
  nvs_close(my_nvs_handle);

  ESP_ERROR_CHECK(esp_wifi_set_mac(ESP_IF_WIFI_STA, mac_addr));

  return true;
}

/*
* Firstly, Master will try to connect to the wifi that storage in nvs before. 
* If Don't have wifi in nvs, It will be send request to the Slave to get 
* the ssid and password of wifi.
* The wifi will be disconnect and connect again with new parameter
* when receive command CMD_WIFI_PASSWORD_REQ from slave.
*/

void wifi_connect_default_ap(void)
{
  if (wifi_detail_exist())
  {
    char ap_ssid[32] = {0};
    char ap_pass[64] = {0};

    //get ssid and password from the flash memory
    get_wifi_ssid_pass(ap_ssid, ap_pass);
    if (check_data_emty((uint8_t*) ap_ssid, 32)) //||
        //check_data_emty((uint8_t*) ap_pass, 64))
    {
      printf("SSID PASS INCORRECT\n");
    }
    else
      wifi_start(ap_ssid, ap_pass);
  }
}

void get_wifi_ssid_pass(char* ssid, char* pass)
{
  nvs_handle my_nvs_handle;
  size_t required_size;

  nvs_open("storage", NVS_READWRITE, &my_nvs_handle);
  /* Get default AP's SSID */
  nvs_get_str(my_nvs_handle, "SSID", NULL, &required_size);
  nvs_get_str(my_nvs_handle, "SSID", ssid, &required_size);
  printf("SSID<%d>: ", required_size);
  for (uint8_t i = 0; i < required_size; i++)
    printf("%c", ssid[i]);
  printf("\n");
  /* Get default AP's */
  nvs_get_str(my_nvs_handle, "PASS", NULL, &required_size);
  nvs_get_str(my_nvs_handle, "PASS", pass, &required_size);
  /* Connect to default AP */
  printf("PASS<%d>: ", required_size);
  for (uint8_t i = 0; i < required_size; i++)
    printf("%c", pass[i]);
  printf("\n");
  nvs_close(my_nvs_handle);
}

bool get_connection_state(void)
{
  if (wifi_state == WIFI_CONNECTED)
    return true;
  return false;
}

uint8_t get_wifi_state(void)
{
  return wifi_state;
}

void print_wifi_state(void)
{
  switch(get_wifi_state())
  {
    case 0 : ESP_LOGI(TAG1,"WIFI_DISCONECTED\n"); break;
    case 1 : ESP_LOGI(TAG1,"WIFI_CONNECTED\n"); break;
    case 2 : ESP_LOGI(TAG1,"WIFI_CONNECTING\n"); break;
    case 3 : ESP_LOGI(TAG1,"WIFI_WPS_SCAN\n"); break;
    case 4 : ESP_LOGI(TAG1,"WIFI_WPS_FAILE\n"); break;
    case 5 : ESP_LOGI(TAG1,"WIFI_WPS_TIMOUT\n"); break;
  }
}

void wifi_deinit_sta(void)
{
  ESP_ERROR_CHECK(esp_wifi_disconnect());
  ESP_ERROR_CHECK(esp_wifi_deinit());
}
//  bool mac_requested(void)
//  {
//     return wifi_check_mac_address(); 
//  }