/**
 * @file nvs.cpp
 * @author backhug team
 * @brief 
 * @version 0.1
 * @date 2022-07-05
 * 
 * @copyright Copyright (c) 2022
 * 
 */

/*******************************************************************************
**                                INCLUDES
*******************************************************************************/
//#include "esp_system.h"
#include "nvs_flash.h"
#include "spi_comm.h"
#include <sstream>

/*******************************************************************************
**                       INTERNAL MACRO DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      COMMON VARIABLE DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      INTERNAL VARIABLE DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      INTERNAL FUNCTION PROTOTYPES
*******************************************************************************/

/*******************************************************************************
**                          FUNCTION DEFINITIONS
*******************************************************************************/

void nvs_store_version(uint8_t* version, char* nvs_str)
{
  size_t size = 3;
  nvs_handle my_handle;
  nvs_open("storage", NVS_READWRITE, &my_handle);
  nvs_set_blob(my_handle, nvs_str, version, size);
  nvs_commit(my_handle);
  nvs_close(my_handle);
}

void nvs_get_version(uint8_t* version, char* nvs_str)
{
  size_t size = 3;
  nvs_handle my_handle;
  nvs_open("storage", NVS_READWRITE, &my_handle);
  if(nvs_get_blob(my_handle, nvs_str, version, &size) != ESP_OK)
  {
      version[0] = 0;
      version[1] = 0;
      version[2] = 0;
  }
  nvs_close(my_handle);
}

void nvs_store_md5(char *md5, char *nvs_str)
{
  nvs_handle my_handle;

  nvs_open("storage", NVS_READWRITE, &my_handle);
  nvs_set_str(my_handle, nvs_str, md5);
  nvs_commit(my_handle);
  nvs_close(my_handle);
}

void nvs_get_md5(char *md5, char *nvs_str)
{
  nvs_handle my_handle;
  size_t required_size = 0;

  nvs_open("storage", NVS_READWRITE, &my_handle);
  nvs_get_str(my_handle, nvs_str, NULL, &required_size);
  nvs_get_str(my_handle, nvs_str, md5, &required_size);
  md5[required_size] = 0;
  nvs_close(my_handle);
}

void nvs_store_update(uint8_t update, char* nvs_str)
{
  nvs_handle my_handle;
  nvs_open("storage", NVS_READWRITE, &my_handle);

  nvs_set_u8(my_handle, nvs_str, update);
  
  nvs_commit(my_handle);
  nvs_close(my_handle);
}

void nvs_get_update(uint8_t* update, char* nvs_str)
{
  nvs_handle my_handle;
  nvs_open("storage", NVS_READWRITE, &my_handle);

  nvs_get_u8(my_handle, nvs_str, update);
  
  nvs_close(my_handle);
}

void nvs_save_macadd(char *mac_addr)
{
  nvs_handle my_handle;
  nvs_open("storage", NVS_READWRITE, &my_handle);
  nvs_set_str(my_handle, "WIFI_MAC", mac_addr);
  nvs_set_u8(my_handle, "WIFI_MAC_EXIST", 1);
  nvs_commit(my_handle);
  nvs_close(my_handle);
}



