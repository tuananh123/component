/**
 * @file nvs.h
 * @author backhug team
 * @brief 
 * @version 0.1
 * @date 2022-07-05
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef __NVS_H__
#define __NVS_H__

/*******************************************************************************
**                               INCLUDES
*******************************************************************************/

/*******************************************************************************
**                                DEFINES
*******************************************************************************/

/*******************************************************************************
**                     EXTERNAL VARIABLE DECLARATIONS
*******************************************************************************/

/*******************************************************************************
**                     EXTERNAL FUNCTION DECLARATIONS
*******************************************************************************/

void nvs_store_version(uint8_t* version, char* nvs_str);
void nvs_get_version(uint8_t* version, char* nvs_str);
void nvs_store_md5(char *md5, char *nvs_str);
void nvs_get_md5(char *md5, char *nvs_str);
void nvs_store_update(uint8_t update, char* nvs_str);
void nvs_get_update(uint8_t* update, char* nvs_str);
void nvs_save_macadd(char *mac_addr);

 #endif 

/******************************** End of file *********************************/

