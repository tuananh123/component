/**
 * @file wifihandler.cpp
 * @author backhug team
 * @brief 
 * @version 0.1
 * @date 2022-07-07
 * 
 * @copyright Copyright (c) 2022
 * 
 */
/************************************************************************
 *                                INCLUDES                              *
 ************************************************************************/
#include "WifiHandler.h"
#include <cstring>

/*******************************************************************************
**                       INTERNAL MACRO DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      COMMON VARIABLE DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      INTERNAL VARIABLE DEFINITIONS
*******************************************************************************/

packet WifiHandler::buffer_packet{};
char WifiHandler::ap_ssid[32];
char WifiHandler::ap_pass[64];

nvs_handle WifiHandler::my_handle{};

WifiHandler wifi_handler;
bool WifiHandler::new_wifi_request = false;
bool WifiHandler::new_wifi = false;
bool WifiHandler::new_wifi_wps = false;
bool WifiHandler::new_wifi_scan = false;
bool WifiHandler::send_wifi_scan = false;
uint8_t WifiHandler::wifi_config;

/*******************************************************************************
**                      INTERNAL FUNCTION PROTOTYPES
*******************************************************************************/

/*******************************************************************************
**                          FUNCTION DEFINITIONS
*******************************************************************************/

bool WifiHandler::send_ssid_request()
{
  memset(&buffer_packet, 0, sizeof(packet));
  buffer_packet.cmd = CMD_WIFI_SSID_REQ;
  buffer_packet.data_len = 0;
  buffer_packet.crc = crc16_calculate(buffer_packet);
  master_comm_transmit(buffer_packet);
  if (!master_comm_recv(&buffer_packet) || buffer_packet.cmd != RESPOND_ACK)
  {
    printf("RESPOND: %x\n", buffer_packet.cmd);
    return false;
  }
  return true;
}

bool WifiHandler::send_password_request()
{
  memset(&buffer_packet, 0, sizeof(packet));
  buffer_packet.cmd = CMD_WIFI_PASSWORD_REQ;
  buffer_packet.data_len = 0;
  buffer_packet.crc = crc16_calculate(buffer_packet);
  master_comm_transmit(buffer_packet);
  if (!master_comm_recv(&buffer_packet) || buffer_packet.cmd != RESPOND_ACK)
  {
    printf("RESPOND: %x\n", buffer_packet.cmd);
    return false;
  }
  return true;
}

bool WifiHandler::send_wifi_scan_data_request()
{
  memset(&buffer_packet, 0, sizeof(packet));
  buffer_packet.cmd = CMD_WIFI_SCAN_REQ;
  buffer_packet.data_len = 0;
  buffer_packet.crc = crc16_calculate(buffer_packet);
  master_comm_transmit(buffer_packet);
  if (!master_comm_recv(&buffer_packet) || buffer_packet.cmd != RESPOND_ACK)
  {
    printf("RESPOND: %x\n", buffer_packet.cmd);
    return false;
  }
  return true;
}

bool WifiHandler::send_wifi_scan_data(uint8_t index, wifi_ap_record_t ap_info)
{
  buffer_packet.cmd = CMD_WIFI_SCAN_DATA;
  buffer_packet.data_len = 2 + sizeof(ap_info.ssid);
  memcpy(&buffer_packet.payload[0], &index, 1);
  memcpy(&buffer_packet.payload[1], &ap_info.rssi, 1);
  memcpy(&buffer_packet.payload[2], (uint8_t *) &ap_info.ssid, sizeof(ap_info.ssid));
  buffer_packet.crc = crc16_calculate(buffer_packet);
  
  /* Send update params and check respond */
  master_comm_transmit(buffer_packet);
  if (!master_comm_recv(&buffer_packet) || buffer_packet.cmd != RESPOND_ACK)
    return false;
  
  return true;
}

void WifiHandler::send_terminate_process(void)
{
  buffer_packet.cmd = CMD_TERMINATE;
  buffer_packet.data_len = 0;
  buffer_packet.crc = crc16_calculate(buffer_packet);

  master_comm_transmit(buffer_packet);

  printf("MASTER: Finish transaction\n");
}

void WifiHandler::save_ssid()
{
  nvs_open("storage", NVS_READWRITE, &my_handle);
  nvs_set_str(my_handle, "SSID", (char*) buffer_packet.payload);
  nvs_commit(my_handle);
  nvs_close(my_handle);
}

void WifiHandler::save_password()
{
  nvs_open("storage", NVS_READWRITE, &my_handle);
  nvs_set_str(my_handle, "PASS", (char*) buffer_packet.payload);
  nvs_set_u8(my_handle, "WIFI_EXIST", 1);
  nvs_commit(my_handle);
  nvs_close(my_handle);
}

void WifiHandler::connect()
{
  wifi_connect();
}

void WifiHandler::stop()
{
  wifi_stop();
}

void WifiHandler::disconnect()
{
  wifi_disconnect();
}

void WifiHandler::start()
{
  get_wifi_ssid_pass(ap_ssid, ap_pass);
  wifi_start(ap_ssid, ap_pass);
}

void WifiHandler::init()
{
  wifi_init_sta();
  wifi_connect_default_ap();
}

bool WifiHandler::check_data_valid()
{
  return check_data_emty(buffer_packet.payload, buffer_packet.data_len);
}

void WifiHandler::start_query()
{
  if (wifi_update_handle == nullptr)
  {
    xTaskCreate(
      (TaskFunction_t)&update,
      "update",
      4096,
      NULL,
      5,
      &wifi_update_handle);
  }
}


void WifiHandler::suspend_query()
{
  if (wifi_update_handle)
    vTaskSuspend(wifi_update_handle);
}

void WifiHandler::resume_query()
{
  if (wifi_update_handle)
    vTaskResume(wifi_update_handle);

}

void WifiHandler::update()
{
  while (1)
  {      
    vTaskDelay(100 / portTICK_PERIOD_MS);
    
    if (new_wifi_scan)
    {
      new_wifi_scan = false;
      printf("wifi scan AP\n");
      stop();
      wifi_scan_sta();
      send_wifi_scan = true;
      connect();
    }

    if (new_wifi_wps)
    {
      new_wifi_wps = false;
      printf("wifi wps\n");
      stop();
      wifi_start_wps();
    }

    if (new_wifi)
    {
      new_wifi = false;
      printf("wifi new connect\n");
      stop();
      wifi_connect_default_ap();
    }
  }
}

void WifiHandler::query_wifi_handle(query_request_t query_request)
{
  bool ret;
  new_wifi_request = query_request.new_wifi_request; 
  new_wifi_wps = query_request.new_wifi_wps;
  new_wifi_scan = query_request.new_wifi_scan;      
  if (new_wifi_request)
  {
    new_wifi_request = false;
    vTaskDelay(100 / portTICK_PERIOD_MS);
    ret = send_ssid_request();
    if (ret == false || check_data_valid())
    {
      printf("send_wifi_ssid_request fail!!\n");
    }
    else
    {
      save_ssid();
      vTaskDelay(100 / portTICK_PERIOD_MS);
      ret = send_password_request();
      if (ret == false)//|| check_data_valid())
      {
        printf("send_wifi_password_request fail!!\n");
      }         
      else
      {
        save_password();
        new_wifi = true;
      }       
    }
  }
    
  if (new_wifi_scan)
  {
    printf("New wifi scan\n");
    while (!send_wifi_scan)
    {
      vTaskDelay(100 / portTICK_PERIOD_MS);  
    }
  } 

  if (send_wifi_scan)
  {
    send_wifi_scan = false;
    send_wifi_scan_data_request();
    vTaskDelay(10 / portTICK_PERIOD_MS);
    if(ap_count >= 20)
      ap_count = 20;
    for (uint8_t i=0; i<ap_count; i++)
    {
      ret = send_wifi_scan_data(i,ap_info[i]);
    }
    send_terminate_process();
  }
}
// bool WifiHandler::query_new_wifi()
// {
//   memset(&buffer_packet, 0, sizeof(packet));
//   buffer_packet.cmd = CMD_NEW_WIFI_REQ;
//   buffer_packet.data_len = 0;
//   buffer_packet.crc = crc16_calculate(buffer_packet);
//   master_comm_transmit(buffer_packet);
//   if (!master_comm_recv(&buffer_packet) || buffer_packet.cmd != RESPOND_ACK)
//   {
//     // printf("RESPOND: %x\n", buffer.cmd);
//     return false;
//   }
//   return true;
// }

