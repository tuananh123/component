/**
 * @file wifihander.h
 * @author backhug team
 * @brief 
 * @version 0.1
 * @date 2022-07-07
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef _WIFIHANDLER_H 
#define _WIFIHANDLER_H

/*******************************************************************************
**                               INCLUDES
*******************************************************************************/

#include "main.h"
#include "spi_comm.h"
#include "communication.h"
#include "nvs_flash.h"
#include "esp_wifi.h"
#include "wifi_sta.h" 

/*******************************************************************************
**                                DEFINES
*******************************************************************************/

/*******************************************************************************
**                     INTERNAL STRUCT DECLARATIONS
*******************************************************************************/

struct WifiHandler
{
  static char ap_ssid[32];
  static char ap_pass[64];

  TaskHandle_t wifi_update_handle = nullptr;

  static nvs_handle my_handle;
  static packet buffer_packet;

  static bool new_wifi_request;
  static bool new_wifi;
  static bool new_wifi_wps;  
  static bool new_wifi_scan;  
  static bool send_wifi_scan;  
  static uint8_t wifi_config;

  void init();
  static bool send_ssid_request();
  static bool send_password_request();
  static void save_ssid();
  static void save_password();
  static void connect();
  static void stop();
  static void disconnect();
  static void start();
  static bool check_data_valid();
  static void query_wifi_handle(query_request_t query_request);
  static bool query_new_wifi();
  static bool send_wifi_scan_data_request();
  static bool send_wifi_scan_data(uint8_t index, wifi_ap_record_t ap_info);
  static void send_terminate_process(void);
  void start_query();
  void suspend_query();
  void resume_query();
  static void update();
};

/*******************************************************************************
**                     EXTERNAL VARIABLE DECLARATIONS
*******************************************************************************/
extern WifiHandler wifi_handler;

#endif //MAIN_WIFIHANDLER_HPP
