/**
 * @file LifeTesting.cpp
 * @author backhug team
 * @brief 
 * @version 0.1
 * @date 2022-09-19
 * 
 * @copyright Copyright (c) 2022
 * 
 */

/*******************************************************************************
**                                INCLUDES
*******************************************************************************/
#include "esp_system.h"
#include "esp_log.h"
#include "Storage.h"
#include "LifeTesting.h"

/*******************************************************************************
**                       INTERNAL MACRO DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      COMMON VARIABLE DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      INTERNAL VARIABLE DEFINITIONS
*******************************************************************************/
static FILE* file;

/*******************************************************************************
**                      INTERNAL FUNCTION PROTOTYPES
*******************************************************************************/

/*******************************************************************************
**                          FUNCTION DEFINITIONS
*******************************************************************************/

uint8_t test_sd_card(void)
{
  uint8_t value[4] = {0x01, 0x02, 0x03, 0x04};
  uint8_t value_read[4] = {};
  file = fopen("/sdcard/TEST.BIN", "w");
  if (file == NULL)
  {
    printf("open file fail\n");
    return 0;
  }
  int write_len = fwrite((const void*) value, sizeof(char), 4, file); 
  if (write_len != 4)
  {
    printf("write len fail = %d\n",write_len);
    return 0;
  }
  fclose(file);
  file = fopen("/sdcard/TEST.BIN", "r");
  if (file == NULL)
  {
    printf("open file fail\n");
    return 0;
  }
  fread(value_read, sizeof(char), 4, file);
  for (uint8_t i = 0; i < 4; i++)
  {
    if (value[i] != value_read[i])
    {
      printf("read file fail\n");
      return 0;
    }
  }
  fclose(file);
  return 1;
}
