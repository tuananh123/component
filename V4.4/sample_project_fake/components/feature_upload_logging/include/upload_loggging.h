/**
 * @file upload_logging.h
 * @author backhug team
 * @brief 
 * @version 0.1
 * @date 2022-07-05
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef __UPLOAD_LOGGING_H_
#define __UPLOAD_LOGGING_H_

/*******************************************************************************
**                               INCLUDES
*******************************************************************************/
#include "uploader.h"
/*******************************************************************************
**                                DEFINES
*******************************************************************************/

/*******************************************************************************
**                     EXTERNAL VARIABLE DECLARATIONS
*******************************************************************************/

/*******************************************************************************
**                     EXTERNAL FUNCTION DECLARATIONS
*******************************************************************************/
int UploadLogFile();
#endif
