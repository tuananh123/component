/**
 * @file main.cpp
 * @author backhug team
 * @brief 
 * @version 0.1
 * @date 2022-07-04
 * 
 * @copyright Copyright (c) 2022
 * 
 */

/*******************************************************************************
**                                INCLUDES
*******************************************************************************/

#include "main.h"
#include "common.h"
#include "esp_task_wdt.h"
#include "esp_heap_caps.h"
#include "esp_system.h"
#include "esp_event.h"
#include "spi_comm.h"
#include "esp_wifi.h"
#include "WifiHandler.h"
#include "uploader.h"
#include "StringOperators.h"
#include "esp_log.h"
#include "communication.h"
#include "process.h"
#include "log_to_sdcard.h"
#include "esp_log.h"
#include "PWM.h" 
DEFINE_THIS_FILE

/*******************************************************************************
**                       INTERNAL MACRO DEFINITIONS
*******************************************************************************/
#define WATCHDOG_TIMEOUT 3000 // 50 minutes
/*******************************************************************************
**                      COMMON VARIABLE DEFINITIONS
*******************************************************************************/
//StackType_t hb_stk_sto[512];
//StaticTask_t hb_buf;
/*******************************************************************************
**                      INTERNAL VARIABLE DEFINITIONS
*******************************************************************************/
bool sdcard_mount;
/*******************************************************************************
**                      INTERNAL FUNCTION PROTOTYPES
*******************************************************************************/

/*******************************************************************************
**                          FUNCTION DEFINITIONS
*******************************************************************************/

void app_main()
{     
  printf("firmware version %d.%d.%d\n", firmware_version[0], firmware_version[1],
         firmware_version[2]);
  bool ret;

  create_heart_beat_task();
  esp_err_t err = nvs_flash_init();
  if(err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND){
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
  }
  ESP_ERROR_CHECK(err);    
  esp_task_wdt_init(WATCHDOG_TIMEOUT, true);
  //MageControl::Auxiliary::NvsFCAL_Init();

  sdcard_mount = MageControl::Storage::SecureDigital::mountPartition();
  spi_comm_master_init();
  
  //create_log_handle();  
  nvs_get_version(ble_version,"BLE_VERSION");

  firmware_init_params(&wifi_firmware, WIFI_FIRMWARE);
  firmware_init_params(&ble_firmware, BLE_FIRMWARE);

  wifi_handler.init();
  wifi_handler.start_query();

  create_PWM_handle();
  create_process_handle();    
  create_master_comm_task();
 }


/******************************** End of file *********************************/
