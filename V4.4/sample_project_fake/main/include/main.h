/**
 * @file main.h
 * @author backhug team
 * @brief 
 * @version 0.1
 * @date 2022-08-04
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef MAIN_H_
#define MAIN_H_

/*******************************************************************************
**                               INCLUDES
*******************************************************************************/

#include <stdio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include "as.h"

#include "downloader.h"
#include "Storage.h"
#include "wifi_sta.h"
#include "HttpClient.h"


#ifdef __cplusplus
extern "C"
{
#endif
    void app_main();
#ifdef __cplusplus
}
#endif

/*******************************************************************************
**                                DEFINES
*******************************************************************************/

/*******************************************************************************
**                     EXTERNAL VARIABLE DECLARATIONS
*******************************************************************************/
extern firmware_t ble_firmware, wifi_firmware;
extern uint8_t firmware_version[3];
extern uint8_t ble_version[3];
extern bool sdcard_mount;
extern int8_t rssi_ap;
extern uint32_t heap;

/*******************************************************************************
**                     EXTERNAL FUNCTION DECLARATIONS
*******************************************************************************/
//extern SemaphoreHandle_t xSemaphore;
extern void nvs_get_version(uint8_t* version, char* nvs_str);

#endif