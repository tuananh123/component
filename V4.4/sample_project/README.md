# Development Environment Info #

  - ESP-IDF version - v4.4

# Setup #
  - Place all binaries in to flash tool
  - Follow offsets found in partition_layout.csv
  - Combine binary using tool
  - - Production ready binary is produced
  - Whenever you want to do remote update simply do ``` make all ```
  - - The esp32_firmware.bin produced is the one that you place on the server latest_version.txt
  - - Take the MD5 hash, this can be found as the ETAG on the file you have just uploaded and paste into MD5.txt on S3
  - - Separate file as this makes it easier for the firmware, and adds a layer to update process to keep safe
  - The OTA updater checks this to see if the version matches
  - - This allows upgrade or downgrade of firmware

# Partitions # 

0x1000 /bin/bootloader.bin 

0x24000 /build/partition_layout.bin  - this is built

0x2e000 /bin/ota_data_initial.bin 

0x30000 /bin/esp32_boot.bin

0xB0000 /build/esp32_firmware.bin   - this is built

