

/**
 * @file sender.cpp
 * @author backhug team
 * @brief 
 * @version 0.1
 * @date 2022-07-07
 * 
 * @copyright Copyright (c) 2022
 * 
 */

/*******************************************************************************
**                                INCLUDES
*******************************************************************************/
#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "esp_system.h"
#include "esp_log.h"
#include "spi_comm.h"
#include "communication.h"
#include "WifiHandler.h"
#include "nvs.h"
#include "ota_from_ble_chip.h"
#include "ota_for_ble_chip.h"
#include "data_spi.h"
#include "esp_log.h"
/*******************************************************************************
**                       INTERNAL MACRO DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      COMMON VARIABLE DEFINITIONS
*******************************************************************************/
static TaskHandle_t comm_task_handle = NULL;
query_request_t query_request;
static send_data_t send_data;
static receive_data_t receive_data;
char *TAG_BLE = "BLE VERSION";
/*******************************************************************************
**                      INTERNAL VARIABLE DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      INTERNAL FUNCTION PROTOTYPES
*******************************************************************************/

/*******************************************************************************
**                          FUNCTION DEFINITIONS
*******************************************************************************/
bool send_wifi_mac_request(packet *buffer_packet)
{
    buffer_packet->cmd = CMD_WIFI_MAC_REQ;
    buffer_packet->data_len = 0;
    buffer_packet->crc = crc16_calculate(*buffer_packet);
    master_comm_transmit(*buffer_packet);
    if (!master_comm_recv(buffer_packet) || buffer_packet->cmd != RESPOND_ACK)
    {
      printf("RESPOND: %x\n", buffer_packet->cmd);
      return false;
    }
    return true;
}

bool send_wifi_ssid_request(packet *buffer_packet)
{
    buffer_packet->cmd = CMD_WIFI_SSID_REQ;
    buffer_packet->data_len = 0;
    buffer_packet->crc = crc16_calculate(*buffer_packet);
    master_comm_transmit(*buffer_packet);
    if (!master_comm_recv(buffer_packet) || buffer_packet->cmd != RESPOND_ACK)
    {
      printf("RESPOND: %x\n", buffer_packet->cmd);
      return false;
    }
    return true;
}

bool send_wifi_password_request(packet *buffer_packet)
{
    buffer_packet->cmd = CMD_WIFI_PASSWORD_REQ;
    buffer_packet->data_len = 0;
    buffer_packet->crc = crc16_calculate(*buffer_packet);
    master_comm_transmit(*buffer_packet);
    if (!master_comm_recv(buffer_packet) || buffer_packet->cmd != RESPOND_ACK)
    {
      printf("RESPOND: %x\n", buffer_packet->cmd);
      return false;
    }
    return true;
}
  // master_send_data((char*)&send_data,sizeof(send_data),CMD_SEND_DATA_REQ);
  // ret = master_receive_data((uint8_t*)&(query_request),sizeof(query_request_t), CMD_QUERY_REQ); 
  // if (ret)
  //   {
  //       query_request.new_wifi_request = 1; 
  //       query_request.new_wifi_wps = 0;
  //       query_request.new_wifi_scan = 0;   
  //     //Handle request wifi
  //     wifi_handler.query_wifi_handle(query_request);  

  //   }
static void master_comm_task(void)
{
  bool ret;
  while (1)
  {
    vTaskDelay(1000/ portTICK_PERIOD_MS);
    print_wifi_state();
    //Send data : connection status + version
    send_data.wifi_state = get_wifi_state();
    send_data.version[0] = firmware_version[0];
    send_data.version[1] = firmware_version[1];
    send_data.version[2] = firmware_version[2];
    master_send_data((char*)&send_data,sizeof(send_data),CMD_SEND_DATA_REQ);
    //Get data: ble version
    vTaskDelay(100 / portTICK_PERIOD_MS);
    ret = master_receive_data((uint8_t*)&receive_data, sizeof(receive_data_t),CMD_GET_DATA_REQ);
    if (ret)
    {
      if( ble_version[0] != receive_data.version[0] || ble_version[1] != receive_data.version[1] || ble_version[2] != receive_data.version[2] )
      {
        ble_version[0] = receive_data.version[0];
        ble_version[1] = receive_data.version[1];
        ble_version[2] = receive_data.version[2];
        nvs_store_version(ble_version,"BLE_VERSION");
        printf("BLE VERSION: Saved\n");
      }
      ESP_LOGI(TAG_BLE,"%d.%d.%d\n",ble_version[0],ble_version[1],ble_version[2]);
    }
    //Send request: new wifi + wps wifi + scan wifi
    vTaskDelay(100 / portTICK_PERIOD_MS);
    ret = master_receive_data((uint8_t*)&(query_request),sizeof(query_request_t), CMD_QUERY_REQ);
    if (ret)
    {
      if (ble_version[0] >= 6 && ble_version[1] >= 0 && ble_version[2] >= 0)
      {
        //OTA WIFI
        if (query_request.new_firmware_wifi)
        {
          vTaskDelay(100 / portTICK_PERIOD_MS);
          master_ota_wifi_process();
        }
        //BED DATA
        if ((query_request.new_file_massage != 0) && (receive_data.ble_connected == 0))
        {
          vTaskDelay(100 / portTICK_PERIOD_MS);
          master_dat_process(); 
        }
      }
      else
      {
        query_request.new_wifi_request = 1; 
        query_request.new_wifi_wps = 0;
        query_request.new_wifi_scan = 0;   
      }
      //Handle request wifi
      wifi_handler.query_wifi_handle(query_request);  

    }
    //OTA SPI
    ota_spi();
  }
}


void create_master_comm_task(void)
{
  if (comm_task_handle == NULL)
  {
    xTaskCreate((TaskFunction_t) &master_comm_task,
                "comm_task",
                5*1024,
                NULL,
                5,
                &comm_task_handle);

  }
}
