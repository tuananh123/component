/**
 * @file data_spi.cpp
 * @author backhug team
 * @brief 
 * @version 0.1
 * @date 2022-07-05
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

#include "esp_system.h"
#include "spi_comm.h"
#include "Storage.h"
#include "communication.h"
#include "esp_log.h"

/************************************************************************
 *                      GLOBAL VARIABLES DECLARATION                    *
 ************************************************************************/
static uint32_t datfile_size;
static char sdcard_filepath[40] = "";
static FILE *file;
static uint32_t numfile;

/************************************************************************
 *                          FUNCTION DEFINITIONS                        *
 ************************************************************************/

static uint8_t send_dat_request(packet buffer_packet)
{
  /* Check for allowing sending dat from backend */
  /* IMPLEMENT LATER */
  
  buffer_packet.cmd = CMD_DAT_REQ;
  buffer_packet.data_len = 0;
  buffer_packet.crc = crc16_calculate(buffer_packet);
  /* Send log request and wait for respond */
  master_comm_transmit(buffer_packet);
  if (!master_comm_recv(&buffer_packet) || buffer_packet.cmd != RESPOND_ACK)
    return 0;
  
  return 1;
}

static void receive_file_params(packet buffer_packet)
{
  memcpy(&numfile, &buffer_packet.payload[0], sizeof(numfile));
  printf("NUM FILE: %d\n",numfile);

  master_send_respond(RESPOND_ACK);
}

static void receive_dat_params(packet buffer_packet)
{
  uint8_t filepath_size = buffer_packet.data_len - sizeof(datfile_size);

  memcpy(&datfile_size, &buffer_packet.payload[0], sizeof(datfile_size));
  strcpy(sdcard_filepath, "/sdcard/");
  //strcat(sdcard_filepath,(char*) &buffer_packet.payload[4]);
  memcpy((uint8_t*)&sdcard_filepath[strlen(sdcard_filepath)], &buffer_packet.payload[4],filepath_size);
  printf("FILE NAME: %s\n",sdcard_filepath);
  printf("FILE SIZE: %d\n",datfile_size);

  master_send_respond(RESPOND_ACK);
}

static uint8_t receive_dat_data(packet buffer_packet)
{
  int write_len = fwrite((const void *) buffer_packet.payload,
                        sizeof(uint8_t), buffer_packet.data_len, file);
  if (write_len != buffer_packet.data_len)
  {
    ESP_LOGE("MASTER", "Write to SD card failed");
    master_send_respond(RESPOND_NACK);
    return 0;
  }
  
  master_send_respond(RESPOND_ACK);
  return 1;
}

uint8_t master_dat_process(void)
{
  packet buffer_packet;
  uint8_t ret;
  uint32_t numfileReceive=0;
  printf("DAT_REQ: SEND REQ!!!\n");
  ret = send_dat_request(buffer_packet);
  if (!ret) return 0;
  
  while (master_comm_recv(&buffer_packet))
  {
    if (buffer_packet.cmd == CMD_DAT_FILE)
    {
      receive_file_params(buffer_packet);
      printf("DAT_FILE: RECEIVED!!!\n");
    }

    else if (buffer_packet.cmd == CMD_DAT_PARAMS)
    {
      receive_dat_params(buffer_packet);
      file = fopen(sdcard_filepath, "wb");
      if (file == NULL) 
      {
        printf("DAT_FILE: FILE CREATE FAILS!!!\n");
        return 0;
      }
      printf("DAT_PARAMS: RECEIVED!!!\n");
    }

    else if (buffer_packet.cmd == CMD_DAT_DATA)
    {
      if (!receive_dat_data(buffer_packet)) 
      {
        fclose(file); 
        return 0;
      }    
    }

    else if (buffer_packet.cmd == CMD_DAT_END)
    {
      fclose(file);
      printf("RECEIVE_DATA: FINISHED!!!\n");
      vTaskDelay(100 / portTICK_PERIOD_MS);
      numfileReceive++;
    }

    else if (buffer_packet.cmd == CMD_TERMINATE)
    {
      printf("RECEIVE TERMINATE!!!\n");
      break;
    }
  }
  printf("END OF PROCESS\n");
  if(numfileReceive != numfile || numfile == 0)
    return 0;
  return 1;
}