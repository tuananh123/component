/*
 * stringop.c
 *
 *  Created on: 1 Jun 2018
 *      Author: boyan
 */

#include "StringOperators.h"

#include <cstdio>
#include <cstdarg>

size_t MageControl::StringOp::c99_ctsprintf(const char * _fmt, ...) 
{
    va_list _args;
    size_t _count;
    void * _no_op = NULL;
    size_t _no_op_s = 0;

    va_start(_args, _fmt);
    _count = vsnprintf((char*)_no_op, _no_op_s, _fmt, _args);

    va_end(_args);

    return _count;
}


