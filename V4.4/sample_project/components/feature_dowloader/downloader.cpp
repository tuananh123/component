/**
 * @file nvs.cpp
 * @author backhug team
 * @brief 
 * @version 0.1
 * @date 2022-07-05
 * 
 * @copyright Copyright (c) 2022
 * 
 */

/*******************************************************************************
**                                INCLUDES
*******************************************************************************/
//#include <cstdio>
#include <cstring>
#include <sstream>
#include "esp_system.h"
#include "esp_ota_ops.h"
#include <rom/md5_hash.h>
//#include "HttpClient.h"
//#include "Storage.h"
//#include "error_code.h"
#include "downloader.h"
#include "ota.h"
#include "main.h"
//#include "nvs_flash.h"
#include "cJSON.h"
//#include <cstring>
#include "nvs.h"
//#include "wifi_sta.h"
#include "esp_log.h"

/*******************************************************************************
**                                INCLUDES
*******************************************************************************/

/*******************************************************************************
**                       INTERNAL MACRO DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      COMMON VARIABLE DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      INTERNAL VARIABLE DEFINITIONS
*******************************************************************************/
//char buffer[MASSAGE_ID_LENGTH + 1];
//static char nameSDcardFile[MAX_FILE_STORED_SDCARD][20];

// alpha server endpoint
// const char *base_url = "http://api-alpha.mybackhug.com";

// production endpoint


/*******************************************************************************
**                      INTERNAL FUNCTION PROTOTYPES
*******************************************************************************/

/*******************************************************************************
**                          FUNCTION DEFINITIONS
*******************************************************************************/

// static void update_error_handle(uint8_t code)
// {
//   error_code |= (1 << code);
//   store_motor_failure();
//   http_client_end();
// }

void firmware_init_params(p_firmware_t p_firmware, uint8_t firmware_type)
{
  switch (firmware_type)
  {
    case WIFI_FIRMWARE:
      p_firmware->version_nvs_str = (char *) "wifi_ver";
      p_firmware->md5_nvs_str = (char *) "wifi_md5";
      p_firmware->base_url = (char *) "https://s3.eu-west-2.amazonaws.com/robophysio-esp32-firmware/firmwarev2.5/wifi/latest_version_wifi.txt";
      p_firmware->md5_url = (char *) "https://s3.eu-west-2.amazonaws.com/robophysio-esp32-firmware/firmwarev2.5/wifi/MD5_wifi.txt";
      // p_firmware->base_url = (char *) "https://s3.eu-west-2.amazonaws.com/robophysio-esp32-firmware/firmwarev2.5_beta/wifi/latest_version_wifi.txt";
      // p_firmware->md5_url = (char *) "https://s3.eu-west-2.amazonaws.com/robophysio-esp32-firmware/firmwarev2.5_beta/wifi/MD5_wifi.txt";      
      // p_firmware->base_url = (char *) "https://s3.eu-west-2.amazonaws.com/robophysio-esp32-firmware/firmwarev2.5_alpha/wifi/latest_version_wifi.txt";
      // p_firmware->md5_url = (char *) "https://s3.eu-west-2.amazonaws.com/robophysio-esp32-firmware/firmwarev2.5_alpha/wifi/MD5_wifi.txt";
      p_firmware->path = (char *) "/sdcard/WIFI.BIN";
      break;
    case BLE_FIRMWARE:
      p_firmware->version_nvs_str = (char *) "ble_ver";
      p_firmware->md5_nvs_str = (char *) "ble_md5";
      p_firmware->base_url = (char *) "https://s3.eu-west-2.amazonaws.com/robophysio-esp32-firmware/firmwarev2.5/ble/latest_version_ble.txt";
      p_firmware->md5_url = (char *) "https://s3.eu-west-2.amazonaws.com/robophysio-esp32-firmware/firmwarev2.5/ble/MD5_ble.txt";
      // p_firmware->base_url = (char *) "https://s3.eu-west-2.amazonaws.com/robophysio-esp32-firmware/firmwarev2.5_beta/ble/latest_version_ble.txt";
      // p_firmware->md5_url = (char *) "https://s3.eu-west-2.amazonaws.com/robophysio-esp32-firmware/firmwarev2.5_beta/ble/MD5_ble.txt";     
      // p_firmware->base_url = (char *) "https://s3.eu-west-2.amazonaws.com/robophysio-esp32-firmware/firmwarev2.5_alpha/ble/latest_version_ble.txt";
      // p_firmware->md5_url = (char *) "https://s3.eu-west-2.amazonaws.com/robophysio-esp32-firmware/firmwarev2.5_alpha/ble/MD5_ble.txt";
      p_firmware->path = (char *) "/sdcard/BLE.BIN";
      break;
    default:
      break;
  }
}
void get_url_version(char* url_version,uint8_t* version)
{
  int size = strlen(url_version);
  if (size > 9)
  {
    if(url_version[size - 9] >= 48) version[0] = url_version[size - 9] - 48;
    if(url_version[size - 7] >= 48) version[1] = url_version[size - 7] - 48;
    if(url_version[size - 5] >= 48) version[2] = url_version[size - 5] - 48;
  }
}
bool check_url_version(p_firmware_t p_firmware, uint8_t* version)
{
  printf("comparing firmware version of %s...\n", p_firmware->version_nvs_str);
  int content_length, read;
  bool end_message = false;  
  char url_version[100];
  
  /* Get current URL version store in backend */
  esp_err_t ret = http_client_init(p_firmware->base_url, &content_length);
  if (ret != ESP_OK) return false;
  http_client_read(url_version, &read, &end_message);
  http_client_end();
  
  /* Checking if update is neccessary */
  url_version[read - 1] = 0;
  printf("newest version from AWS: %s\n", url_version);

  
  uint8_t versionAWS[3]={0,0,0};

  get_url_version(url_version,versionAWS);

  if(versionAWS[0] != version[0] || versionAWS[1] != version[1] || versionAWS[2] != version[2])
  {
    memcpy(p_firmware->url_version, url_version, read);
    return true;
  }
  return false;
}

static bool store_firmware_to_sdcard(p_firmware_t p_firmware)
{
  printf("downloading firmwares ...\n");
  FILE *file;
  int total_read = 0, read, content_length;
  bool end_message = false;
  
  file = fopen(p_firmware->path, "wb");
  if (file == NULL) return false;

  /* Get firmware from back-end */
  esp_err_t ret = http_client_init(p_firmware->url_version, &content_length);
  if (ret != ESP_OK) return false;
  char *buffer = (char *)malloc(MAX_HTTP_RECV_BUFFER + 1);
  while (!end_message)
  {
    /* Read and write to sdcard, check for collision */
    http_client_read(buffer, &read, &end_message);
    int write_len = fwrite((const void *)buffer, sizeof(char), read, file);
    if (write_len != read)
    {
      http_client_end();
      free(buffer);
      fclose(file);
      return false;
    }  
    total_read += read;
  }  
  http_client_end();
  free(buffer);
  fclose(file);
  /* Check if correct firmware size downloaded */
  if (total_read != content_length) return false;

  return true;
}

void download_md5(p_firmware_t p_firmware)
{
  int read, content_length;
  bool end_message = false;

  /* Get MD5 from backend */
  esp_err_t ret = http_client_init(p_firmware->md5_url, &content_length);
  if (ret != ESP_OK) return;
  http_client_read(p_firmware->md5, &read, &end_message);
  p_firmware->md5[read] = 0;
  http_client_end();

  nvs_store_md5(p_firmware->md5, p_firmware->md5_nvs_str);
}

bool download_firmware(p_firmware_t p_firmware, uint8_t* version)
{
  bool ret;
 
  /* Check for downloading required */
  if (get_connection_state() == false) return false;
  ret = check_url_version(p_firmware,version);
  if (ret == true)
  {
    /* Download new firmware and MD5 */
    store_firmware_to_sdcard(p_firmware);
    download_md5(p_firmware);
    
    return true;
  }

  return false;
}





