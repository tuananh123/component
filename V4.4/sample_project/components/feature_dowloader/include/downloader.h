#ifndef __DOWNLOADER_H
#define __DOWNLOADER_H

/*******************************************************************************
**                               INCLUDES
*******************************************************************************/
#include "stdint.h"
/*******************************************************************************
**                                DEFINES
*******************************************************************************/

#define MASSAGE_ID_LENGTH   24

#define MAX_FILE_STORED_SDCARD 512
// uint8_t get_url_version(const char *base_url, char *url_version);

/*******************************************************************************
**                     EXTERNAL VARIABLE DECLARATIONS
*******************************************************************************/

typedef enum 
{
  WIFI_FIRMWARE,
  BLE_FIRMWARE
} firmware_type_t;

typedef struct
{
  /* Storing version in NVS */
  char *base_url;
  char url_version[100];
  char *version_nvs_str;
  
  char *md5_url;
  char *md5_nvs_str;
  
  char md5[33];
  char *path;
} firmware_t;

typedef firmware_t *p_firmware_t;


/*******************************************************************************
**                     EXTERNAL FUNCTION DECLARATIONS
*******************************************************************************/

bool download_firmware(p_firmware_t p_firmware, uint8_t* version);
void firmware_init_params(p_firmware_t p_firmware, uint8_t firmware_type);
void get_url_version(char* url,uint8_t* version);
void download_md5(p_firmware_t p_firmware);
bool check_url_version(p_firmware_t p_firmware, uint8_t* version);

#define MAX_HTTP_RECV_BUFFER 512

#endif /* __DOWNLOADER_H */
