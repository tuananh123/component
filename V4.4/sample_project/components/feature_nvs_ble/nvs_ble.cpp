/**
 * @file nvs.cpp
 * @author backhug team
 * @brief 
 * @version 0.1
 * @date 2022-07-05
 * 
 * @copyright Copyright (c) 2022
 * 
 */

/*******************************************************************************
**                                INCLUDES
*******************************************************************************/
#include "esp_system.h"
#include "nvs_flash.h"
#include "nvs.h"

/*******************************************************************************
**                       INTERNAL MACRO DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      COMMON VARIABLE DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      INTERNAL VARIABLE DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      INTERNAL FUNCTION PROTOTYPES
*******************************************************************************/

/*******************************************************************************
**                          FUNCTION DEFINITIONS
*******************************************************************************/
void nvs_store_version_ble(uint8_t* version, char* nvs_str)
{
  size_t size = 3;
  nvs_handle my_handle;
  nvs_open("storage", NVS_READWRITE, &my_handle);
  nvs_set_blob(my_handle, nvs_str, version, size);
  nvs_commit(my_handle);
  nvs_close(my_handle);
}

void nvs_get_version_ble(uint8_t* version, char* nvs_str)
{
  size_t size = 3;
  nvs_handle my_handle;
  nvs_open("storage", NVS_READWRITE, &my_handle);
  if(nvs_get_blob(my_handle, nvs_str, version, &size) != ESP_OK)
  {
      version[0] = 0;
      version[1] = 0;
      version[2] = 0;
  }
  nvs_close(my_handle);
}