/**
 * @file nvs.cpp
 * @author backhug team
 * @brief 
 * @version 0.1
 * @date 2022-07-05
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef __NVS_BLE_H
#define __NVS_BLE_H

void nvs_store_version_ble(uint8_t* version, char* nvs_str);
void nvs_get_version_ble(uint8_t* version, char* nvs_str);

#endif /* __NVS_BLE_H */