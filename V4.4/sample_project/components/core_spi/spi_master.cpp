/**
 * @file spi_master.cpp
 * @author backhug team
 * @brief 
 * @version 0.1
 * @date 2022-07-06
 * 
 * @copyright Copyright (c) 2022
 * 
 */

/*******************************************************************************
**                                INCLUDES
*******************************************************************************/
#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include "esp_system.h"
#include "esp_event.h"
#include "driver/spi_master.h"
#include "esp_log.h"
#include "driver/gpio.h"
#include "spi_comm.h"


/*******************************************************************************
**                       INTERNAL MACRO DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      COMMON VARIABLE DEFINITIONS
*******************************************************************************/
static spi_transaction_t trans;
static spi_device_handle_t handle;
static uint8_t send_buf[MAX_PAYLOAD];
static uint8_t recv_buf[MAX_PAYLOAD]; 

/* Semaphore indicating if slave is ready to receive stuff */
static xQueueHandle rdy_sem;

/*******************************************************************************
**                      INTERNAL VARIABLE DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      INTERNAL FUNCTION PROTOTYPES
*******************************************************************************/

/*******************************************************************************
**                          FUNCTION DEFINITIONS
*******************************************************************************/

/* ISR called when GPIO handshake line goes high */
static void IRAM_ATTR gpio_handshake_isr_handler(void* arg)
{
    /* Sometimes due to interference or ringing or something, 
       we get two irqs after eachother
       Ignore everything < 1ms after an earlier irq */

    static uint32_t last_handshake_time;
    uint32_t cur_time = xthal_get_ccount();
    uint32_t diff = cur_time - last_handshake_time;
    if (diff < 240000) return; 
    last_handshake_time = cur_time;

    /* Give the semaphore */
    BaseType_t must_yield = false;
    xSemaphoreGiveFromISR(rdy_sem, &must_yield);
    if (must_yield) portYIELD_FROM_ISR();
}

uint16_t crc16_calculate(struct packet buffer_packet)
{
  uint16_t crc_val = CRC_INITIAL;
  uint8_t buffer[MAX_PAYLOAD];
  uint16_t idx = 0;

  /* Parse packet to buffer */
  buffer[idx++] = buffer_packet.cmd;
  buffer[idx++] = buffer_packet.data_len & 0xFF;
  buffer[idx++] = (buffer_packet.data_len >> 8) & 0xFF;

  for (int payload_idx = 0; payload_idx < buffer_packet.data_len; payload_idx++)
    buffer[idx++] = buffer_packet.payload[payload_idx];

  uint16_t buffer_len = idx;
  idx = 0;
  while (buffer_len--)
  {
    crc_val ^= (buffer[idx++] << 8);
    
    for(uint8_t count = 0; count < 8; count++) 
    {
      if(crc_val & 0x8000)
        crc_val = (crc_val << 1) ^ CRC_POLYNOMINAL;
      else
        crc_val = crc_val << 1;
    }
  }

  return crc_val;
}

void spi_comm_master_init()
{
  esp_err_t ret;
  spi_bus_config_t bus_cfg;
  spi_device_interface_config_t dev_cfg;
  gpio_config_t io_cfg;

  /* Configuration for the SPI bus */
  memset(&bus_cfg, 0, sizeof(bus_cfg));
  bus_cfg.mosi_io_num = PIN_NUM_MOSI;
  bus_cfg.miso_io_num = PIN_NUM_MISO;
  bus_cfg.sclk_io_num = PIN_NUM_CLK;
  bus_cfg.quadwp_io_num = -1;
  bus_cfg.quadhd_io_num = -1;

  /* Configuration for the SPI device on the other side of the bus */
  memset(&dev_cfg, 0, sizeof(dev_cfg));
  dev_cfg.clock_speed_hz = 1000000;
  dev_cfg.duty_cycle_pos = 128; //50% duty cycle
  dev_cfg.mode = 0;
  dev_cfg.spics_io_num = PIN_NUM_CS;
  dev_cfg.queue_size = 1;
  
  /* Create the semaphore */
  rdy_sem = xSemaphoreCreateBinary();

  memset(&io_cfg, 0, sizeof(io_cfg));
  io_cfg.intr_type = (gpio_int_type_t) GPIO_PIN_INTR_POSEDGE;
  io_cfg.mode = GPIO_MODE_INPUT;
  io_cfg.pull_up_en = (gpio_pullup_t) 1;
  io_cfg.pin_bit_mask = (1 << GPIO_HANDSHAKE);

  gpio_config(&io_cfg);
  gpio_install_isr_service(0);
  gpio_set_intr_type((gpio_num_t) GPIO_HANDSHAKE, (gpio_int_type_t) GPIO_PIN_INTR_POSEDGE);
  gpio_isr_handler_add((gpio_num_t) GPIO_HANDSHAKE, gpio_handshake_isr_handler, NULL);

  /* Initialize the SPI bus and add the device we want to send stuff to */
  ret = spi_bus_initialize(VSPI_HOST, &bus_cfg, 2);
  ESP_ERROR_CHECK(ret);

  ret = spi_bus_add_device(VSPI_HOST, &dev_cfg, &handle);
  ESP_ERROR_CHECK(ret);

  memset(&trans, 0, sizeof(trans));
  xSemaphoreGive(rdy_sem);
}

uint8_t master_comm_recv(struct packet *recv_packet)
{
  /* Skip storing 2 START bytes */
  int idx = 2;    
  memset(send_buf, 0xFF, sizeof(send_buf));
  
  trans.length = MAX_PAYLOAD * 8;
  trans.tx_buffer = send_buf;
  trans.rx_buffer = recv_buf;

  /* Wait for slave to be ready */
  xSemaphoreTake(rdy_sem, 100);
  spi_device_transmit(handle, &trans);

  while (recv_buf[idx - 2] != PKG_INDICATOR_1 
        || recv_buf[idx - 1] != PKG_INDICATOR_2)
  {
    if (idx == MAX_PAYLOAD - 2) 
    {
      printf("recv_buf_ERROR\n");
      return 0;
    }
    idx++;
  }

  /* Parse buffer data to struct buffer */
  recv_packet->cmd = recv_buf[idx++];  
  
  uint8_t l_length = recv_buf[idx++];
  uint8_t h_length = recv_buf[idx++];
  recv_packet->data_len = (h_length << 8) | l_length;

  for (int payload_idx = 0; payload_idx < recv_packet->data_len; payload_idx++)
    recv_packet->payload[payload_idx] = recv_buf[idx++]; 

  /* Checking with CRC */
  uint8_t l_crc = recv_buf[idx++];
  uint8_t h_crc = recv_buf[idx++];
  recv_packet->crc = (h_crc << 8) | l_crc;
  uint16_t crc_calc = crc16_calculate(*recv_packet);
  if (crc_calc != recv_packet->crc) return 0;

  /* Checking terminate signal */
  if (recv_buf[idx] != PKG_END) return 0;

  return 1;
}

void master_comm_transmit(struct packet send_packet)
{
  uint16_t index = 0;
  memset(send_buf, 0xFF, sizeof(send_buf));

  /* Packaging the send buffer */
  send_buf[index++] = PKG_INDICATOR_1;
  send_buf[index++] = PKG_INDICATOR_2;
  send_buf[index++] = send_packet.cmd;
  send_buf[index++] = send_packet.data_len & 0xFF;
  send_buf[index++] = (send_packet.data_len >> 8) & 0xFF;

  for (int payload_idx = 0; payload_idx < send_packet.data_len; payload_idx++)
    send_buf[index++] = send_packet.payload[payload_idx];
  
  send_buf[index++] = send_packet.crc & 0xFF;
  send_buf[index++] = (send_packet.crc >> 8) & 0xFF;

  send_buf[index] = PKG_END;

  trans.length = MAX_PAYLOAD * 8;
  trans.tx_buffer = send_buf;
  trans.rx_buffer = recv_buf;
  
  /* Wait for slave to be ready */
  xSemaphoreTake(rdy_sem, 100);
  spi_device_transmit(handle, &trans);
}

void master_send_respond(uint8_t cmd)
{
  struct packet buffer_packet;

  buffer_packet.cmd = cmd;
  buffer_packet.data_len = 0;
  buffer_packet.crc = crc16_calculate(buffer_packet);

  master_comm_transmit(buffer_packet);
}

bool master_send_data(char* data, uint8_t length, uint8_t cmd)
{
  packet buffer_packet;
  
  buffer_packet.cmd = cmd;
  buffer_packet.data_len = length;
  for (uint8_t payload_idx = 0;
      payload_idx < buffer_packet.data_len; payload_idx++)
  {
    buffer_packet.payload[payload_idx] = data[payload_idx];
  }
  buffer_packet.crc = crc16_calculate(buffer_packet);
  master_comm_transmit(buffer_packet);
  if (!master_comm_recv(&buffer_packet) || buffer_packet.cmd != RESPOND_ACK)
      return false;
  return true;
}

bool master_receive_data(uint8_t* data, uint8_t length, uint8_t cmd)
{
  packet buffer_packet;
  memset(buffer_packet.payload, 0, MAX_PAYLOAD);
  buffer_packet.cmd = cmd;
  buffer_packet.data_len = 0;
  buffer_packet.crc = crc16_calculate(buffer_packet);
  master_comm_transmit(buffer_packet);
  if (!master_comm_recv(&buffer_packet) || buffer_packet.cmd != RESPOND_ACK)
  {
    printf("RESPOND: %x\n", buffer_packet.cmd);
    return false;
  }
  memcpy((uint8_t*)data, buffer_packet.payload, length);
  return true;
}
