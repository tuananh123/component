/**
 * @file spi_slave.cpp
 * @author backhug team
 * @brief 
 * @version 0.1
 * @date 2022-07-06
 * 
 * @copyright Copyright (c) 2022
 * 
 */

/*******************************************************************************
**                                INCLUDES
*******************************************************************************/
#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include "esp_system.h"
#include "esp_event.h"
#include "driver/spi_slave.h"
#include "esp_log.h"
#include "driver/gpio.h"
#include "spi_comm.h"


/*******************************************************************************
**                       INTERNAL MACRO DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      COMMON VARIABLE DEFINITIONS
*******************************************************************************/
static packet buffer_packet;
static spi_slave_transaction_t trans;
static uint8_t send_buf[MAX_PAYLOAD];
static uint8_t recv_buf[MAX_PAYLOAD];


/*******************************************************************************
**                      INTERNAL VARIABLE DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      INTERNAL FUNCTION PROTOTYPES
*******************************************************************************/

/*******************************************************************************
**                          FUNCTION DEFINITIONS
*******************************************************************************/

/* 
 * Called after a transaction is queued and ready for pickup by master 
 * Set with HIGH value
 */
void my_post_setup_cb(spi_slave_transaction_t* trans)
{
  WRITE_PERI_REG(GPIO_OUT_W1TS_REG, (1 << GPIO_HANDSHAKE));
}

/* 
 * Called after transaction is sent/received 
 * Set with LOW value
 */
void my_post_trans_cb(spi_slave_transaction_t* trans)
{
  WRITE_PERI_REG(GPIO_OUT_W1TC_REG, (1 << GPIO_HANDSHAKE));
}

void spi_comm_slave_init()
{
  esp_err_t ret;
  gpio_config_t io_cfg;
  spi_bus_config_t bus_cfg;
  spi_slave_interface_config_t slv_cfg;

  //Configuration for the SPI bus
  memset(&bus_cfg, 0, sizeof(bus_cfg));
  bus_cfg.mosi_io_num = PIN_NUM_MOSI;
  bus_cfg.miso_io_num = PIN_NUM_MISO;
  bus_cfg.sclk_io_num = PIN_NUM_CLK;

  //Configuration for the SPI slave interface
  memset(&slv_cfg, 0, sizeof(slv_cfg));
  slv_cfg.mode = 0;
  slv_cfg.spics_io_num = PIN_NUM_CS;
  slv_cfg.queue_size = 1;
  slv_cfg.flags = 0;
  slv_cfg.post_setup_cb = my_post_setup_cb;
  slv_cfg.post_trans_cb = my_post_trans_cb;

  memset(&io_cfg, 0, sizeof(io_cfg));
  io_cfg.intr_type = GPIO_INTR_DISABLE;
  io_cfg.mode = GPIO_MODE_OUTPUT;
  io_cfg.pin_bit_mask = (1 << GPIO_HANDSHAKE);
  gpio_config(&io_cfg);

  gpio_set_pull_mode((gpio_num_t) PIN_NUM_MOSI, GPIO_PULLUP_ONLY);
  gpio_set_pull_mode((gpio_num_t) PIN_NUM_CLK, GPIO_PULLUP_ONLY);
  gpio_set_pull_mode((gpio_num_t) PIN_NUM_CS, GPIO_PULLUP_ONLY);

  //Initialize SPI slave interface
  ret = spi_slave_initialize(VSPI_HOST, &bus_cfg, &slv_cfg, 2);
  ESP_ERROR_CHECK(ret);

  memset(&trans, 0, sizeof(trans));
}

void slave_comm_transmit(struct packet send_packet)
{
  uint16_t index = 0;
  memset(send_buf, 0xFF, sizeof(send_buf));

  /* Packaging the send buffer */
  send_buf[index++] = PKG_INDICATOR_1;
  send_buf[index++] = PKG_INDICATOR_2;
  send_buf[index++] = send_packet.cmd;
  send_buf[index++] = send_packet.data_len & 0xFF;
  send_buf[index++] = (send_packet.data_len >> 8) & 0xFF;

  for (int payload_idx = 0; payload_idx < send_packet.data_len; payload_idx++)
    send_buf[index++] = send_packet.payload[payload_idx];

  send_buf[index++] = send_packet.crc & 0xFF;
  send_buf[index++] = (send_packet.crc >> 8) & 0xFF;

  send_buf[index] = PKG_END;

  trans.length = MAX_PAYLOAD * 8;
  trans.tx_buffer = send_buf;
  trans.rx_buffer = recv_buf;

  spi_slave_transmit(VSPI_HOST, &trans, portMAX_DELAY);
}

uint8_t slave_comm_recv(struct packet* recv_packet)
{
  int idx = 2;    /* Skip storing 2 START bytes */
  memset(send_buf, 0xFF, sizeof(send_buf));

  trans.length = MAX_PAYLOAD * 8;
  trans.tx_buffer = send_buf;
  trans.rx_buffer = recv_buf;

  spi_slave_transmit(VSPI_HOST, &trans, portMAX_DELAY);

  while (recv_buf[idx - 2] != PKG_INDICATOR_1 ||
         recv_buf[idx - 1] != PKG_INDICATOR_2)
  {
    if (idx == MAX_PAYLOAD - 2) return 0;
    idx++;
  }

  /* Parse buffer data to struct buffer */
  recv_packet->cmd = recv_buf[idx++];

  uint8_t l_length = recv_buf[idx++];
  uint8_t h_length = recv_buf[idx++];
  recv_packet->data_len = (h_length << 8) | l_length;

  for (int payload_idx = 0; payload_idx < recv_packet->data_len; payload_idx++)
    recv_packet->payload[payload_idx] = recv_buf[idx++];

  /* Checking with CRC */
  uint8_t l_crc = recv_buf[idx++];
  uint8_t h_crc = recv_buf[idx++];
  recv_packet->crc = (h_crc << 8) | l_crc;
  uint16_t crc_calc = crc16_calculate(*recv_packet);
  if (crc_calc != recv_packet->crc) return 0;

  if (recv_buf[idx] != PKG_END) return 0;

  return 1;
}

void slave_send_respond(uint8_t cmd)
{
  buffer_packet.cmd = cmd;
  buffer_packet.data_len = 0;
  buffer_packet.crc = crc16_calculate(buffer_packet);

  slave_comm_transmit(buffer_packet);
}

void slave_send_data(char* data, uint8_t length, uint8_t cmd)
{
  buffer_packet.cmd = cmd;
  buffer_packet.data_len = length;
  for (uint8_t payload_idx = 0;
       payload_idx < buffer_packet.data_len; payload_idx++)
  {
    // printf("%c\t", data[payload_idx]);
    buffer_packet.payload[payload_idx] = data[payload_idx];
  }
  buffer_packet.crc = crc16_calculate(buffer_packet);

  slave_comm_transmit(buffer_packet);
}
