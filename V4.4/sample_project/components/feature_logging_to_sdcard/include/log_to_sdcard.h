/**
 * @file log_to_sdcard.h
 * @author backhug team
 * @brief 
 * @version 0.1
 * @date 2022-25-08 
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef _LOG_TO_SDCARD_H
#define _LOG_TO_SDCARD_H

/*******************************************************************************
**                                INCLUDES
*******************************************************************************/

/*******************************************************************************
**                                DEFINES
*******************************************************************************/

/*******************************************************************************
**                     EXTERNAL VARIABLE DECLARATIONS
*******************************************************************************/

/*******************************************************************************
**                     EXTERNAL FUNCTION DECLARATIONS
*******************************************************************************/
void create_log_handle(void);
// int printf_to_sd_card(const char *fmt, va_list list);
#endif
