/**
 * @file log_to_sdcard.cpp
 * @author backhug team
 * @brief 
 * @version 0.1
 * @date 2022-25-08
 * 
 * @copyright Copyright (c) 2022
 * 
 */

/*******************************************************************************
**                                INCLUDES
*******************************************************************************/
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <stdio.h>
#include <string.h>
#include <sys/unistd.h>
#include "esp_err.h"
#include "esp_log.h"
#include "esp_vfs_fat.h"
#include "driver/sdmmc_host.h"
#include "driver/sdspi_host.h"
#include "sdmmc_cmd.h"
/*******************************************************************************
**                       INTERNAL MACRO DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      COMMON VARIABLE DEFINITIONS
*******************************************************************************/
static const char *TAG ="Debug";
static FILE *log_file;
/*******************************************************************************
**                      INTERNAL VARIABLE DEFINITIONS
*******************************************************************************/
static TaskHandle_t task_handle = NULL;
/*******************************************************************************
**                      INTERNAL FUNCTION PROTOTYPES
*******************************************************************************/

/*******************************************************************************
**                          FUNCTION DEFINITIONS
*******************************************************************************/
int printf_to_sd_card(const char *fmt, va_list list)
{
  if(log_file == NULL)
  {
    return -1;
  }
  int res = vfprintf(log_file ,fmt, list);

  fsync(fileno(log_file));
  
  return res;
}

void log_handler(void)
{
  log_file = fopen("/sdcard/test1.txt", "a");

  if (log_file == NULL)
  {
    ESP_LOGE(TAG, "Failed to open file for logging!");
  } 

  else
  {
    esp_log_set_vprintf(&printf_to_sd_card);
  }
  
  vTaskDelay(3000 / portTICK_PERIOD_MS);
  vTaskDelete(task_handle); 
}


void create_log_handle(void)
{
  if (task_handle == NULL)
  {
    xTaskCreate((TaskFunction_t) &log_handler,
                "task",
                8192,
                NULL,
                5,
                &task_handle);
  }
}

