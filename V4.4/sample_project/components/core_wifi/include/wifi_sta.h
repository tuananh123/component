/**
 * @file wifi_sta.h
 * @author backhug team
 * @brief 
 * @version 0.1
 * @date 2022-07-07
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef __WIFI_STA_H
#define __WIFI_STA_H

#ifdef __cplusplus
extern "C" 
{
#endif

/*******************************************************************************
**                               INCLUDES
*******************************************************************************/
#include "esp_wifi.h"

/************************************************************************
 *                                 DEFINE                              *
 ************************************************************************/
#define DEFAULT_SCAN_LIST_SIZE 20

#define ESP_STA_SSID        "null"
#define ESP_STA_PASW        "NULL"
#define TIMES_RETRY_CONNECT    2
#define FIRST_TIME             true
#define NOT_FIRST_TIME         false
// #define ESP_STA_PASS    CONFIG_ESP_WIFI_PASSWORD


/*******************************************************************************
**                     EXTERNAL VARIABLE DECLARATIONS
*******************************************************************************/

typedef enum {
    WIFI_STA_NORMAL=1,
    WIFI_STA_WPS,    
    WIFI_STA_SCAN
} wifi_sta_type_t;
typedef enum {
    WIFI_DISCONECTED,
    WIFI_CONNECTED,
    WIFI_CONNECTING,
    WIFI_WPS_SCAN,
    WIFI_WPS_FAILE,
    WIFI_WPS_TIMOUT,
} wifi_sta_state_t;

/*******************************************************************************
**                     EXTERNAL FUNCTION DECLARATIONS
*******************************************************************************/

void wifi_connect_default_ap(void);
bool get_connection_state(void);
uint8_t get_wifi_state(void);
void print_wifi_state(void);
uint8_t get_times_retry_connect(void);
void reset_times_retry_connect(void);
void wifi_start_wps(void);
void wifi_scan_sta(void);
void wifi_connect(void);
void wifi_disconnect(void);
void wifi_stop(void);
void wifi_start(char *ap_ssid, char *ap_pass);
void wifi_init_sta(void);
void wifi_deinit_sta(void);
void get_wifi_ssid_pass(char *ssid, char *pass);
bool wifi_check_mac_address(void);
bool wifi_set_mac_addr(void);
bool check_data_emty(uint8_t *data, uint8_t len);


extern wifi_ap_record_t ap_info[DEFAULT_SCAN_LIST_SIZE];
extern uint16_t ap_count;


#ifdef __cplusplus
}
#endif

#endif /* __WIFI_STA_H */
