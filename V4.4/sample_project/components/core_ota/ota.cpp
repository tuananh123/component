/**
 * @file receiver.cpp
 * @author backhug team
 * @brief 
 * @version 0.1
 * @date 2022-07-07
 * 
 * @copyright Copyright (c) 2022
 * 
 */

/*******************************************************************************
**                                INCLUDES
*******************************************************************************/
#include <cstdio>
#include <cstring>
#include <sstream>
#include <cstring>

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <rom/md5_hash.h>
#include "esp_ota_ops.h"
#include "ota.h"
#include "esp_log.h"
/*******************************************************************************
**                       INTERNAL MACRO DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      COMMON VARIABLE DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      INTERNAL VARIABLE DEFINITIONS
*******************************************************************************/
esp_ota_handle_t update_handle = 0;
const esp_partition_t *update_partition = NULL;

/*******************************************************************************
**                          FUNCTION DEFINITIONS
*******************************************************************************/
void ota_init(void)
{
  const esp_partition_t *configured = esp_ota_get_boot_partition();
  const esp_partition_t *running = esp_ota_get_running_partition();

  if (configured != running)
  {
    ESP_LOGW("OTA", "Configured OTA boot partition at offset 0x%08x, but running from offset 0x%08x",
             configured->address, running->address);
    ESP_LOGW("OTA", "(This can happen if either the OTA boot data or preferred boot image become corrupted somehow.)");
  }

  ESP_LOGI("OTA", "Running partition type %d subtype %d (offset 0x%08x)",
           running->type, running->subtype, running->address);

  update_partition = esp_ota_get_next_update_partition(NULL);
  ESP_LOGI("OTA", "Writing to partition subtype %d at offset 0x%x",
           update_partition->subtype, update_partition->address);
  esp_err_t ret = esp_ota_begin(update_partition, OTA_SIZE_UNKNOWN, &update_handle);
  if (ret != ESP_OK)
    ESP_LOGE("OTA", "esp_ota_begin failed (%s)", esp_err_to_name(ret));
  
  ESP_LOGI("OTA", "esp_ota_begin succeeded");
}

void ota_handle(void *data, uint16_t len)
{
  static int bytes_written = 0;

  esp_err_t ret = esp_ota_write(update_handle, (const void *)data, len);
  if (ret != ESP_OK)
    ESP_LOGI("OTA", "Write fail");

  bytes_written += len;

  ESP_LOGI("OTA", "Written image length %d", bytes_written);
  ESP_LOGI("OTA", "Data Read %d", len);

  // esp_task_wdt_reset();
  vTaskDelay(20 / portTICK_PERIOD_MS);
}

void ota_end(void)
{
  if (esp_ota_end(update_handle) != ESP_OK)
    ESP_LOGE("OTA", "esp_ota_end failed!");

  esp_err_t ret = esp_ota_set_boot_partition(update_partition);
  if (ret != ESP_OK)
    ESP_LOGE("OTA", "esp_ota_set_boot_partition failed (%s)!", esp_err_to_name(ret));
  ESP_LOGI("OTA", "Prepare to restart system!");

  // save_disable_mqtt(0); /* enable module mqtt */
  // save_reset_predicted(1);

  esp_restart();
}



