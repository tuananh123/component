/**
 * @file nvs.cpp
 * @author backhug team
 * @brief 
 * @version 0.1
 * @date 2022-27-07
 * 
 * @copyright Copyright (c) 2022
 * 
 */

/*******************************************************************************
**                                INCLUDES
*******************************************************************************/
#include <cstring>
#include <cstdio>
#include <cstring>
#include <sstream>
#include "esp_system.h"

#include <rom/md5_hash.h>
#include "ota.h"
#include "nvs.h"
#include "esp_log.h"
#include "ota_use_sdcard.h"
#include "esp_ota_ops.h"
#include "Storage.h"
#include <fcntl.h>

/*******************************************************************************
**                       INTERNAL MACRO DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      COMMON VARIABLE DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      INTERNAL VARIABLE DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      INTERNAL FUNCTION PROTOTYPES
*******************************************************************************/

/*******************************************************************************
**                          FUNCTION DEFINITIONS
*******************************************************************************/

bool verifying_new_firmware(p_firmware_t p_firmware)
{
  int f, fsize;
  MD5Context ctx;

  memset(&ctx, 0, sizeof(ctx));
  MD5Init(&ctx);

  nvs_get_md5(p_firmware->md5, p_firmware->md5_nvs_str);

  /* Get firmware size */
  if (strcmp(p_firmware->path, "/sdcard/WIFI.BIN") == 0)
  {
    f = MageControl::Storage::SecureDigital::fOpen("WIFI.BIN", O_RDONLY);
    fsize = MageControl::Storage::SecureDigital::fSize("WIFI.BIN");
  }
  else
  {
    f = MageControl::Storage::SecureDigital::fOpen("BLE.BIN", O_RDONLY);
    fsize = MageControl::Storage::SecureDigital::fSize("BLE.BIN");
  }
  
  /*  MD5 calculate from firmware  */
  uint8_t *buffer = (uint8_t *)malloc(4096); 
  for (int i = 0; i < fsize; i += 4096)
  {
    size_t rw_size = 4096;
    if (fsize - i < 4096)
      rw_size = fsize - i;
                                                        
    MageControl::Storage::SecureDigital::fRead(f, (void *) buffer, rw_size);
    MD5Update(&ctx, buffer, rw_size);
    /* Check if this firmware being update directly */
    if (strcmp(p_firmware->path, "/sdcard/WIFI.BIN") == 0)
      ota_handle((void *) buffer, rw_size);
  }
  close(f);
  free(buffer);

  unsigned char digest[16];
  memset(digest, 0, sizeof(digest));
  MD5Final(digest, &ctx);
  std::stringstream ss;
  for (int x = 0; x < 16; x++)
  {
    if (digest[x] < 0x10)
      ss << std::hex << (int)0;
    ss << std::hex << (int)digest[x];
  }
  std::string mystr = ss.str();
  ESP_LOGI("Digest", "%s<", mystr.c_str());
  ESP_LOGI("MD5", "%s<", p_firmware->md5);
  if (strcmp(mystr.c_str(), p_firmware->md5) == 0)
  {
    nvs_store_version((uint8_t *) p_firmware->url_version, p_firmware->version_nvs_str);
    return true;
  }
  
  /* Clear URL version to redownload firmware */
  nvs_store_version((uint8_t *) "null", p_firmware->version_nvs_str);
  return false;
}

void ota_process(p_firmware_t p_firmware)
{
  ota_init();

  /* Write new firmware and verify MD5 for firmware */
  if (verifying_new_firmware(p_firmware) == false)
    return;
  
  ota_end();
}

