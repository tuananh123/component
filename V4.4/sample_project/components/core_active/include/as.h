/**
 * @file as.hpp
 * @author Nhat Bui (buivietnhatctqb@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2020-04-14
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#ifndef __AS_H__
#define __AS_H__

#ifdef __cplusplus
// extern "C"
// {
#endif

/*******************************************************************************
**                               INCLUDES
*******************************************************************************/

/*******************************************************************************
**                                DEFINES
*******************************************************************************/

#define NORETURN    void

#define DEFINE_THIS_FILE \
    static char_t const this_module_[] = __FILE__;

#define DEFINE_THIS_MODULE(name_) \
    static char_t const this_module_[] = name_;

#define ASSERT(test_) ((test_) \
    ? (void)0 : on_assert(&this_module_[0], __LINE__))


#define REQUIRE(test_)         ASSERT(test_)

#define ENSURE(test_) ASSERT(test_)

#define DIM(array_) (sizeof(array_) / sizeof((array_)[0]))

#define ERROR() \
        on_assert(&this_module_[0], __LINE__)
/*******************************************************************************
**                     EXTERNAL VARIABLE DECLARATIONS
*******************************************************************************/
typedef char char_t;
typedef int int_t;


/*******************************************************************************
**                     EXTERNAL FUNCTION DECLARATIONS
*******************************************************************************/
NORETURN on_assert(char_t const * const module, int_t const location);



#ifdef __cplusplus
// }
#endif

#endif /* __AS_H__ */

/******************************** End of file *********************************/
