/**
 * @file ep.h
 * @author Nhat Bui (buivietnhatctqb@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2020-06-23
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#ifndef __EP_H__
#define __EP_H__

#ifdef __cplusplus
#endif

/******************************************************************************
**                               INCLUDES
*******************************************************************************/
#include <stdint.h>
/*******************************************************************************
**                                DEFINES
*******************************************************************************/
#define EVT_CAST(class_) (static_cast<class_ const *>(e))
#define STATE_CAST(handler_) (reinterpret_cast<StateHandler>(handler_))

#define STATE_DECL(state_) \
  State state_ ## _h(Event const * const e); \
  static State state_(void * const me, Event const * const e)

#define STATE_DEF(subclass_, state_) \
  State subclass_::state_(void * const me, Event const * const e) {\
      return static_cast<subclass_ *>(me)->state_ ## _h(e); } \
  State subclass_::state_ ## _h(Event const * const e)
/*******************************************************************************
**                     EXTERNAL VARIABLE DECLARATIONS
*******************************************************************************/

typedef uint8_t State;
typedef uint16_t Signal;

enum reserved_signals
{
  ENTRY_SIG = 1,
  EXIT_SIG,
  INIT_SIG,
  USER_SIG
};

struct Event
{
  Signal signal;
  Event() = default;
  Event(Signal sig) : signal(sig) {}
};

struct SubscribleList
{
  uint32_t volatile bits;
};

using StateHandler = State (*)(void * const me, Event const * const e);

class Hsm
{
  StateHandler state;

public:
  virtual ~Hsm() noexcept {}

  virtual void init(void const *const e);
  virtual void init(void) { this->init(nullptr); }

  virtual void dispatch(Event const *const e) noexcept;

  bool is_in(StateHandler const s) noexcept;

  StateHandler get_state(void) const noexcept
  {
    return state;
  }

  static State top(void *const me, Event const *const e) noexcept;

protected:
  Hsm(StateHandler const initial) noexcept;

  State tran(StateHandler const target) noexcept
  {
    state = target;
    return RET_TRAN;
  }

  State super(StateHandler const super_state) noexcept
  {
    state = super_state;
    return RET_SUPER;
  }

public:
  static const State RET_SUPER{0};
  static const State RET_IGNORED{1};
  static const State RET_TRAN{2};
  static const State RET_HANDLED{3};

  static const uint8_t MAX_NEST_DEPTH{6};

private:
  int8_t hsm_tran(StateHandler (&path)[MAX_NEST_DEPTH]);
};

/*******************************************************************************
**                     EXTERNAL FUNCTION DECLARATIONS
*******************************************************************************/

#ifdef __cplusplus
#endif

#endif /* __EP_H__ */

/******************************** End of file *********************************/
