/**
 * @file time.cpp
 * @author Nhat Bui (buivietnhatctqb@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2020-06-24
 * 
 * @copyright Copyright (c) 2020
 * 
 */

/*******************************************************************************
**                                INCLUDES
*******************************************************************************/
#include "active.h"
#include "as.h"

DEFINE_THIS_FILE
/*******************************************************************************
**                       INTERNAL MACRO DEFINITIONS
*******************************************************************************/
#define TICK_STACK_SIZE 2048
/*******************************************************************************
**                      COMMON VARIABLE DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      INTERNAL VARIABLE DEFINITIONS
*******************************************************************************/
StackType_t tick_stk_sto[TICK_STACK_SIZE];
StaticTask_t tick_task_buf;
TimeEvent *time_event_head;
/*******************************************************************************
**                      INTERNAL FUNCTION PROTOTYPES
*******************************************************************************/

/*******************************************************************************
**                          FUNCTION DEFINITIONS
*******************************************************************************/
TimeEvent::TimeEvent(Active *const act, Signal sig)
    : Event(sig), act(act)
{
  next = static_cast<TimeEvent *>(0);
}

void TimeEvent::arm(uint32_t nticks)
{
  REQUIRE((nticks > 0)                             /* cannot arm the timer with 0 tick*/
          && (prev == static_cast<TimeEvent *>(0)) /* time event must not be used */
          && (act != static_cast<Active *>(0)));

  ctr = nticks;
  prev = this;

  portMUX_TYPE mutex = portMUX_INITIALIZER_UNLOCKED;
  portENTER_CRITICAL(&mutex); /* lock the interrupt */

  next = time_event_head;
  if (time_event_head != static_cast<TimeEvent *>(0))
  {
    time_event_head->prev = this;
  }

  time_event_head = this;

  portEXIT_CRITICAL(&mutex); /* unlock the interrupt */
}

uint8_t TimeEvent::disarm()
{
  uint8_t was_armed;
  portMUX_TYPE mutex = portMUX_INITIALIZER_UNLOCKED;
  portENTER_CRITICAL(&mutex);              /* lock the interrupt */
  if (prev != static_cast<TimeEvent *>(0)) /* is the time event actually armed? */
  {
    was_armed = static_cast<uint8_t>(1);
    if (this == time_event_head)
    {
      time_event_head = next;
    }
    else
    {
      if (next != static_cast<TimeEvent *>(0)) /* not the last in the list? */
      {
        next->prev = prev;
      }
      prev->next = next;
    }
    prev = static_cast<TimeEvent *>(0); /* mark the time event as disarmed */
  }
  else /* the time event was not armed */
  {
    was_armed = static_cast<uint8_t>(0);
  }
  portEXIT_CRITICAL(&mutex); /* unlock the interrupt */
  return was_armed;
}

void TimeEvent::post_every(uint32_t nticks)
{
  nticks = nticks / TICK_RATE;
  interval = nticks;
  arm(nticks);
}

void TimeEvent::post_in(uint32_t nticks)
{
  nticks = nticks / TICK_RATE;
  interval = 0;
  arm(nticks);
}

static void tick(void)
{

  TimeEvent *t;
  // portMUX_TYPE mutex = portMUX_INITIALIZER_UNLOCKED;
  // portENTER_CRITICAL(&mutex); /* lock the interrupt */

  t = time_event_head; /* start scanning the list from the head */
  while (t != (TimeEvent *)0)
  {
    if (--t->ctr == 0) /* is time evt about to expire? */
    {
      if (t->interval != 0) /* is it periodic timeout? */
      {
        t->ctr = t->interval; /* rearm the time event */
      }
      else /* one-shot timeout, disarm by removing it from the list */
      {
        if (t == time_event_head)
        {
          time_event_head = t->next;
        }
        else
        {
          if (t->next != static_cast<TimeEvent *>(0)) /* not the last event? */
          {
            t->next->prev = t->prev;
          }
          t->prev->next = t->next;
        }
        t->prev = static_cast<TimeEvent *>(0); /* mark the event disarmed */
      }
      // portEXIT_CRITICAL(&mutex); /* unlock the interrupt */
      // active_post(t->act, (evt *)t);
      t->act->post(static_cast<Event *>(t));
    }
    else
    {
      // static uint8_t volatile dummy;
      // portMUX_TYPE mutex = portMUX_INITIALIZER_UNLOCKED;
      // portENTER_CRITICAL(&mutex); /* unlock the interrupt */
      // dummy = (uint8_t)0;         /* execute a few instructions */

      // portEXIT_CRITICAL(&mutex); /* lock scheduler again to advance the link */
      t = t->next;
    }
  }
  // portEXIT_CRITICAL(&mutex); /* unlock the interrupt */
}

static void tick_function(void *pvParameter)
{
  while (1)
  {
    tick();
    vTaskDelay(TICK_RATE / portTICK_PERIOD_MS);
  }
}

void TimeEvent::create_tick_task(void)
{
  TaskHandle_t thr;
  thr = xTaskCreateStatic(
      &tick_function,                          /* the task function */
      "tick",                                  /* the name of the task */
      TICK_STACK_SIZE,                         /* stack size */
      NULL,                                    /* the 'pvParameters' parameter */
      (UBaseType_t)(configMAX_PRIORITIES - 1), /* highest priority */
      (StackType_t *)tick_stk_sto,             /* stack storage */
      &tick_task_buf);                         /* task buffer */
  ENSURE(thr != (TaskHandle_t)0);              /* must be created */
}

/******************************** End of file *********************************/
