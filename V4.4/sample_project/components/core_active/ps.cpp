/**
 * @file ps.cpp
 * @author Nhat Bui (buivietnhatctqb@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2020-06-24
 * 
 * @copyright Copyright (c) 2020
 * 
 */

/*******************************************************************************
**                                INCLUDES
*******************************************************************************/
#include "active.h"
#include "as.h"

DEFINE_THIS_FILE
/*******************************************************************************
**                       INTERNAL MACRO DEFINITIONS
*******************************************************************************/
#define INSERT(me_, n_) \
  ((me_)->bits |= ((uint32_t)1 << ((n_)-1U)))

#define REMOVE(me_, n_) \
  ((me_)->bits &= (uint32_t)(~((uint32_t)1 << ((n_)-1U))))

// #define HAS_ELEMENT(me_, n_)
//     (((me_)->bits & ((uint32_t)1 << ((n_) - 1U))) != 0U)
#define NOT_EMPTY(me_) ((me_)->bits != 0U)

#define FIND_MAX(me_, n_) \
  ((n_) = cal_LOG2((me_)->bits))
/*******************************************************************************
**                      COMMON VARIABLE DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      INTERNAL VARIABLE DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      INTERNAL FUNCTION PROTOTYPES
*******************************************************************************/
SubscribleList *subscribe_list;
Signal max_Signal;
/*******************************************************************************
**                          FUNCTION DEFINITIONS
*******************************************************************************/
uint8_t cal_LOG2(uint32_t x)
{
  static uint8_t const log2LUT[16] = {
      0U, 1U, 2U, 2U, 3U, 3U, 3U, 3U,
      4U, 4U, 4U, 4U, 4U, 4U, 4U, 4U};
  uint_fast8_t n = 0U;
  uint32_t t;

#if (MAX_ACTIVE > 16)
  t = (uint32_t)(x >> 16);
  if (t != 0U)
  {
    n += 16U;
    x = t;
  }
#endif
#if (MAX_ACTIVE > 8)
  t = (x >> 8);
  if (t != 0U)
  {
    n += 8U;
    x = t;
  }
#endif
  t = (x >> 4);
  if (t != 0U)
  {
    n += 4U;
    x = t;
  }
  return n + log2LUT[x];
}



void publish(Event *e)
{
  SubscribleList subs; /* local, modifiable copy of the subscriber list */

  REQUIRE(e->signal < max_Signal);

  /* make a local, modifiable copy of the subscriber list */
  subs = subscribe_list[e->signal];

  if (NOT_EMPTY(&subs)) /* any subscribers? */
  {
    uint8_t id = 0;
    FIND_MAX(&subs, id); /* the highest-id subscriber */
    do
    { /* loop over all subscribers */
      ASSERT(active_[id] != static_cast<Active *>(0));
      /* active_post() asserts internally if the queue overflows */
      // active_post(active_[id], e);
      active_[id]->post(e);
      REMOVE(&subs, id); /* remove the handled subscriber */
      if (NOT_EMPTY(&subs))
      {                      /* still more subscribers? */
        FIND_MAX(&subs, id); /* highest-id subscriber */
      }
      else
      {
        id = 0U; /* no more subscribers */
      }
    } while (id != 0U);
  }
}

void Active::subscribe(Signal const sig)
{
  uint8_t id = priority;

  REQUIRE((sig < max_Signal) && (active_[id] == this));

  portMUX_TYPE mutex = portMUX_INITIALIZER_UNLOCKED;
  portENTER_CRITICAL(&mutex); /* lock the interrupt */

  /* insert the corresponding bit to the look up table */
  INSERT(&subscribe_list[sig], id);

  portEXIT_CRITICAL(&mutex);
}


void Active::unsubcribe(Signal const sig)
{
  uint8_t id = priority;
  REQUIRE((sig < max_Signal) && (active_[id] == this));

  portMUX_TYPE mutex = portMUX_INITIALIZER_UNLOCKED;
  portENTER_CRITICAL(&mutex); /* lock the interrupt */

  /* remove the corresponding bit in the look up table */
  REMOVE(&subscribe_list[sig], id);

  portEXIT_CRITICAL(&mutex);
}

void ps_init(SubscribleList *subscr_sto, Signal max_signal)
{
  REQUIRE((subscr_sto != static_cast<SubscribleList *>(0)));
  subscribe_list = subscr_sto;
  max_Signal = max_signal;
}

/******************************** End of file *********************************/
