/**
 * @file nvs.cpp
 * @author backhug team
 * @brief 
 * @version 0.1
 * @date 2022-27-07 
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef __OTA_NOT_SDCARD_H
#define __OTA_NOT_SDCARD_H

/*******************************************************************************
**                                INCLUDES
*******************************************************************************/
#include "downloader.h"

/*******************************************************************************
**                                DEFINES
*******************************************************************************/
#define MAX_HTTP_RECV_BUFFER 512

/*******************************************************************************
**                     EXTERNAL VARIABLE DECLARATIONS
*******************************************************************************/
typedef firmware_t *p_firmware_t;

/*******************************************************************************
**                     EXTERNAL FUNCTION DECLARATIONS
*******************************************************************************/

void get_firmware_and_ota(p_firmware_t p_firmware);
void download_firmware_notsdcard(p_firmware_t p_firmware, uint8_t* version);
#define MAX_HTTP_RECV_BUFFER 512

#endif /* __DOWNLOADER_H */
