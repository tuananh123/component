/**
 * @file uploader.cpp
 * @author backhug team
 * @brief 
 * @version 0.1
 * @date 2022-21-05
 * 
 * @copyright Copyright (c) 2022
 * 
 */

/*******************************************************************************
**                                INCLUDES
*******************************************************************************/
#include "uploader.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "esp_system.h"
#include "nvs_flash.h"
#include "cJSON.h"
#include <ctype.h>
#include "esp_http_client.h"
#include "main.h"
#include "ota.h"
#include "downloader.h"
#include <dirent.h>
#include "esp_task_wdt.h"
#include "HttpClient.h"
#include "nvs.h"
#include "uploader_spine_map.h"
//#include "dbg.h"
// clang-format off

/*******************************************************************************
**                       INTERNAL MACRO DEFINITIONS
*******************************************************************************/

#define HTTP_OK 200
#define HTTP_ERROR 401
#define HTTP_UNKNOWN_WEB 404

#define HTTP_BUFFER_SIZE 512

#define HEADER_NORMAL_LENGTH 64
#define HEADER_FILE_LENGTH 82
#define TAIL_LENGTH 8



#define HTTP_DEBUG 1
#define HTTP_BOUNDARY "------------------------a636d386524243e8"
#define TAG "HTTP_CLIENT"

/*******************************************************************************
**                      COMMON VARIABLE DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      INTERNAL VARIABLE DEFINITIONS
*******************************************************************************/

char buffer[MASSAGE_ID_LENGTH + 1];
char nameSDcardFile[MAX_FILE_STORED_SDCARD][20];
//static char nameSDcardFile[MAX_FILE_STORED_SDCARD][20];
// static int countNumSDcardFile = 0;

// production endpoint
const char *base_url = "http://api.mybackhug.com";
const char *base_url_log = "https://api-alpha-v3.mybackhug.com";
// const char *base_url = "https://api-beta.mybackhug.com";
// const char *base_url = "https://api-alpha-v3.mybackhug.com";
const char *new_upload_endpoint = "/api/v2/s3/new-beddata-url";
// const char *check_valid_booking_endpoint = "/api/v2/s3/check-booking";
// const char *check_eligible_endpoint = "/api/v2/checkin-eligible";
const char *bed_endpoint = "/api/v2/bedlog";
const char *bed_endpointGet = "/api/v2/bedstats";
const char *bed_endpointGetV3 = "/api/v3/bedstats";
// const char *bed_endpoint_notify = "api/v2/beddata";

const char *massage_session_id = "?massage_session_id=";
char *TAG_POST = "POST DATA";
const char *name = "/logs";
const char *file_type = "?file_type=txt";
const char *chip = "&folder_type=wifi";
/*******************************************************************************
**                      INTERNAL FUNCTION PROTOTYPES
*******************************************************************************/

/*******************************************************************************
**                          FUNCTION DEFINITIONS
*******************************************************************************/

char *get_mac_address()
{
  char *mac_addr = (char *)malloc(18);
  // char *mac_addr1 = (char *)malloc(18);
  uint8_t mac_read[6] = {0};

  nvs_handle my_nvs_handle;
  // char *request_url;
  size_t required_size;
  char temp[7] = {0};


  nvs_open("storage", NVS_READWRITE, &my_nvs_handle);
  /* Get default AP's WIFI_MAC */
  nvs_get_str(my_nvs_handle, "WIFI_MAC", NULL, &required_size);
  nvs_get_str(my_nvs_handle, "WIFI_MAC", temp, &required_size);
  for(uint8_t index = 0; index < required_size -1; index++)
  {
    mac_read[index] = temp [index];
  }
  nvs_close(my_nvs_handle);

  sprintf(mac_addr + 0, "%02x", mac_read[0]);
  mac_addr[2] = ':';
  sprintf(mac_addr + 3, "%02x", mac_read[1]);
  mac_addr[5] = ':';
  sprintf(mac_addr + 6, "%02x", mac_read[2]);
  mac_addr[8] = ':';
  sprintf(mac_addr + 9, "%02x", mac_read[3]);
  mac_addr[11] = ':';
  sprintf(mac_addr + 12, "%02x", mac_read[4]);
  mac_addr[14] = ':';
  sprintf(mac_addr + 15, "%02x", mac_read[5]);

  ESP_LOGI("mac_addr", "%s", mac_addr);
  
  return mac_addr;
}

void erare_sd_card()
{
  struct stat st;
  uint16_t numFile = countFileSDcard();
  for (int i = 0; i < numFile; i++)
  {
    char *SDcardPATH = (char *)malloc(40);
    strcpy(SDcardPATH, "/sdcard/");
    strcat(SDcardPATH, nameSDcardFile[numFile - 1 - i]);

    if (stat(SDcardPATH, &st) == 0)
    {
      // fileLength = st.st_size;

      unlink(SDcardPATH);
      ESP_LOGI("SD card erase", "%s", SDcardPATH);
    }
  }
}

void erare_file_sd_card(char* SDcardPATH )
{
  struct stat st;
  if (stat(SDcardPATH, &st) == 0)
  {
    // fileLength = st.st_size;

    unlink(SDcardPATH);
    ESP_LOGI("SD card erase file", "%s", SDcardPATH);
  }
}

int upload_bed_stat()
{
  /* upload status + errror + fw version */
  ESP_LOGI(TAG, "Upload bed status");
    char* mac_address = get_mac_address();
  const char *mac_id = "?connection_id=";


  // request_url = (char *)malloc(strlen(base_url) + strlen(bed_endpoint) + strlen(mac_id) + strlen(mac_address) + 1);
  char request_url[strlen(base_url) + strlen(bed_endpoint) + strlen(mac_id) + strlen(mac_address) + 1];
  // char *token = (char *)malloc(50);
  char token[50];
  // char *postData = (char *)malloc(140);
  char postData[140];

  strcpy(request_url, base_url);
  strcat(request_url, bed_endpoint);
  strcat(request_url, mac_id);
  strcat(request_url, mac_address);

  ESP_LOGI("update status", "%s", request_url);
  strcpy(token, "bed:");
  free(mac_address);
  sprintf(postData, "{\"state\": {\"error_code\": %d, \"error_motor\": %d, \"fw_version_ble\": \"%d.%d.%d\", \"fw_version_wifi\": \"%d.%d.%d\", \"RSSI\": \"%d\"}}",
          0, 0, ble_version[0], ble_version[1], ble_version[2],firmware_version[0], firmware_version[1], firmware_version[2],rssi_ap);

  printf("post data: %s\n", postData);
  ESP_LOGI(TAG_POST,"%s\n",postData);

//  error_motor = 0;
//  error_code = 0;

//  if (sdcard_mount)
//  {
//    store_motor_failure();
//  }

  if (http_client_put_with_token(request_url, token, postData) != ESP_OK)
  {
    ESP_LOGE(TAG, "PUT failed");
  }

  http_client_end();

  return 0;
}

int request_upload_permission()
{
  // printf("Inside request\n");
  int req_status_code;
  int isAllowUpload = 0;
  int read;
  int totalRead = 0;
  bool endMessage = false;
  int contentLength;
  // char *request_url;
  char *mac_address = get_mac_address();
  const char *mac_id = "?connection_id=";

  // request_url = (char *)malloc(strlen(base_url) + strlen(bed_endpointGet) + strlen(mac_id) + strlen(mac_address) + 1);
  char request_url[strlen(base_url) + strlen(bed_endpointGet) + strlen(mac_id) + strlen(mac_address) + 1];
  // char *token = (char *)malloc(50);
  char token[50];

  strcpy(request_url, base_url);
  strcat(request_url, bed_endpointGet);
  strcat(request_url, mac_id);
  strcat(request_url, mac_address);

  ESP_LOGI("Request permission", "request_url %s", request_url);

  strcpy(token, "bed:");
  strcat(token, mac_address);

  free(mac_address);

  if (http_client_get_with_token(request_url, &contentLength, token) != ESP_OK)
  {
    // ESP_LOGI("REQUEST UPLOAD PERMISSION", "http client get faild");
    return -1;
  }

  req_status_code = http_client_get_status_code();
  // ESP_LOGI(TAG, "REQUEST UPLOAD PERMISSION status:%d", req_status_code);

  if (req_status_code == 200) //  HTTP OK
  {
    char *buffer = (char *)malloc(contentLength + 1);

    if (buffer == NULL)
    {
      ESP_LOGE(TAG, "Malloc error");
    }

    while (1)
    {
      http_client_read(buffer, &read, &endMessage); /* Read result from HTTP server */

      totalRead += read;

      if (endMessage)
      {
        break;
      }
    }

    http_client_end();

    if (contentLength > 0)
    {
      cJSON *root = cJSON_Parse(buffer);

      isAllowUpload = cJSON_GetObjectItem(root, "allow_upload")->valueint;

      //printf("Allow to upload value: %d\n", isAllowUpload);

      cJSON_Delete(root);
    }
    free(buffer);
  }

  return isAllowUpload;
}

int request_upgrade_permission(char *typeChip)
{
  int req_status_code;
  int isAllowUpgrade = 0;
  int read;
  int totalRead = 0;
  bool endMessage = false;
  int contentLength;
  nvs_handle my_nvs_handle;
  // char *request_url;
  size_t required_size;
  char temp[7] = {0};
  char* mac_addr = get_mac_address();
  const char *mac_id = "?connection_id=";

  // request_url = (char *)malloc(strlen(base_url) + strlen(bed_endpointGet) + strlen(mac_id) + strlen(mac_addr) + 1);
  char request_url[strlen(base_url) + strlen(bed_endpointGetV3) + strlen(typeChip) + strlen(mac_id) + strlen(mac_addr) + 1];
  // char *token = (char *)malloc(50);
  char token[50];

  strcpy(request_url, base_url);
  strcat(request_url, bed_endpointGetV3);
  strcat(request_url, typeChip);
  strcat(request_url, mac_id);
  strcat(request_url, mac_addr);

  ESP_LOGI("request_url", "%s", request_url);

  strcpy(token, "bed:");
  strcat(token, mac_addr);

  free(mac_addr);

  if (http_client_get_with_token(request_url, &contentLength, token) != ESP_OK)
  {
    // ESP_LOGI("REQUEST UPGRADE PERMISSION", "http client init faild");
  }

  req_status_code = http_client_get_status_code();
  // ESP_LOGI(TAG, "REQUEST UPGRADE PERMISSION status:%d", req_status_code);

  if (req_status_code == 200) // HTTP OK
  {
    char *buffer = (char *)malloc(contentLength + 1);

    // ESP_LOGI("REQUEST PERMISSION", "token: %s", token);

    while (1)
    {
      http_client_read(buffer, &read, &endMessage); /* Read result from HTTP server */

      totalRead += read;

      if (endMessage)
      {
        break;
      }
    }


    if (contentLength > 0)
    {
      cJSON *root = cJSON_Parse(buffer);

      isAllowUpgrade = cJSON_GetObjectItem(root, "allow_upgrade")->valueint;

      //printf("Allow to upgrade value: %d\n", isAllowUpgrade);

      cJSON_Delete(root);
    }
    free(buffer);
  }

  http_client_end();

  return isAllowUpgrade;
}