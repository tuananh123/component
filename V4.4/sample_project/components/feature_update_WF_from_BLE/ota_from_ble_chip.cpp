/**
  * @file:      ota_from_ble_chip.cpp
  * @author:    backhug team
  * @version:   0.1
  * @date:      29-7-2022
  * @copyright: Copyright (c) 2022
  */
/************************************************************************
 *                               INCLUDE                                *
 ************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "esp_system.h"
#include "spi_comm.h"
#include "main.h"
#include "nvs.h"
#include "ota.h"
#include "communication.h"
#include "esp_log.h"
#include "ota_use_sdcard.h"

/*******************************************************************************
**                       INTERNAL MACRO DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      COMMON VARIABLE DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      INTERNAL VARIABLE DEFINITIONS
*******************************************************************************/
static FILE *file = NULL;
static uint32_t file_size = 0;
static uint32_t total_recv = 0;
static packet buffer_packet;
static char md5[33] = {0};

/*******************************************************************************
**                      INTERNAL FUNCTION PROTOTYPES
*******************************************************************************/

/*******************************************************************************
**                          FUNCTION DEFINITIONS
*******************************************************************************/

static uint8_t send_ota_wifi_request(void)
{
  buffer_packet.cmd = CMD_OTA_WIFI_REQ;
  buffer_packet.data_len = 0;
  buffer_packet.crc = crc16_calculate(buffer_packet);
  /* Send log request and wait for respond */
  master_comm_transmit(buffer_packet);
  if (!master_comm_recv(&buffer_packet) || buffer_packet.cmd != RESPOND_ACK)
    return 0;
  return 1;
}

static void receive_ota_wifi_params(packet buffer_packet)
{
  /* Get the MD5 and file size */
  memcpy(md5, &buffer_packet.payload[0], sizeof(md5));
  md5[32] = 0;
  memcpy((uint8_t *) &file_size, &buffer_packet.payload[32], sizeof(file_size));
  nvs_store_md5(md5, "wifi_md5");
  printf("MD5: %s - SIZE: %d\n", md5, file_size);
  master_send_respond(RESPOND_ACK);
}

static uint8_t receive_ota_wifi_data(packet buffer_packet)
{
  int write_len = fwrite((const void *) buffer_packet.payload,
                        sizeof(uint8_t), buffer_packet.data_len, file);
  if (write_len != buffer_packet.data_len)
  {
    ESP_LOGE("MASTER", "Write to SD card failed");
    master_send_respond(RESPOND_NACK);
    return 0;
  }

  total_recv += buffer_packet.data_len;
  if (total_recv % 51200 == 0)
    ESP_LOGI("SLAVE", "RECV 51200 bytes");
  
  
  master_send_respond(RESPOND_ACK);
  return 1;
}

static void receive_terminate(void)
{
  if (total_recv == file_size)
  {
    printf("total received = file size, run ota_init!!\n");
    ota_process(&wifi_firmware);
  }
}

uint8_t master_ota_wifi_process(void)
{
  struct stat st;
  packet buffer_packet;
  uint8_t ret;
  total_recv = 0;
  //printf("OTA_WIFI_REQ: SEND REQ!!!\n");
  ret = send_ota_wifi_request();
  if (!ret) return 0;

  while (master_comm_recv(&buffer_packet))
  {
    if (buffer_packet.cmd == CMD_OTA_WIFI_PARAMS)
    {
      receive_ota_wifi_params(buffer_packet);
      if (stat("/sdcard/WIFI.BIN", &st) == 0)
      {
        unlink("/sdcard/WIFI.BIN");
      }
      file = fopen("/sdcard/WIFI.BIN", "wb");
      if (file == NULL) return 0;
      printf("OTA_WIFI_PARAMS: RECEIVED!!!\n");
    }

    else if (buffer_packet.cmd == CMD_OTA_WIFI_DATA)
    {
      if (!receive_ota_wifi_data(buffer_packet)) 
      {
        fclose(file);
        return 0;
      }    
    }

    else if (buffer_packet.cmd == CMD_OTA_WIFI_END)
    {
      fclose(file);
      printf("RECEIVE_DATA: FINISHED!!!\n");
      vTaskDelay(100 / portTICK_PERIOD_MS);
    }

    else if (buffer_packet.cmd == CMD_TERMINATE)
    {
      printf("RECEIVE TERMINATE!!!\n");
      fclose(file);
      receive_terminate();      
      break;
    }
  }
  printf("END OF PROCESS\n");

  fclose(file);

  return 1;
}