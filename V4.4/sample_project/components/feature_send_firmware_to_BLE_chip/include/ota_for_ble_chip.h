/**
 * @file: ota_for_ble_chip.cpp
 * @author backhug team
 * @brief 
 * @version 0.1
 * @date 2022-07-05
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef __OTA_FOR_BLE_CHIP_H
#define __OTA_FOR_BLE_CHIP_H

/*******************************************************************************
**                               INCLUDES
*******************************************************************************/
#include "downloader.h"

/*******************************************************************************
 *                                  DEFINES                                    *
 *******************************************************************************/
#define OTA_PAYLOAD         256

/*******************************************************************************
**                     EXTERNAL VARIABLE DECLARATIONS
*******************************************************************************/
extern bool send_firmware_ble;

/*******************************************************************************
**                     EXTERNAL FUNCTION DECLARATIONS
*******************************************************************************/
bool spi_transfer_firmware(p_firmware_t p_firmware);
void ota_spi(void);

#endif