#ifndef HTTP_CLIENT_H_
#define HTTP_CLIENT_H_

#include <freertos/FreeRTOS.h>
#include "esp_http_client.h"

#ifdef __cplusplus
extern "C"
{
#endif

esp_err_t http_client_post_with_token(const char *url, char *token, char *post_data);


/**
 * @brief get content from server, including token
 * 
 * @param url : endpoint to get
 * @param content_length : length of content send from server
 * @param token : token included
 * @return esp_err_t :
 * ESP_OK: successful
 * -1: error
 */
esp_err_t http_client_get_with_token(const char * url, int * content_length, char *token);


/**
 * @brief put data to server, including token in header 
 * 
 * @param url : endpoint to put 
 * @param token : token included
 * @param post_data : data to post
 * @return esp_err_t :
 * ESP_OK: successful
 * -1: error
 */
esp_err_t http_client_put_with_token(const char * url, char *token, char *post_data);

/**
 * @brief Init HTTP client
 * 
 * @param url 
 * @param content_length : length of data responded from server
 * @return esp_err_t 
 * ESP_OK: successful
 * other: error
 */
esp_err_t http_client_init(const char * url, int * content_length);

/**
 * @brief read reponse from server
 * 
 * @param buffer : buffer to store data have read
 * @param read : length of data have read
 * @param endMessage : indicate that if is the end of data from buffer
 */
void http_client_read(char * buffer, int * read, bool * endMessage);

/**
 * @brief end http client
 * 
 */
void http_client_end();

/**
 * @brief write data to server
 * 
 * @param postData : data to write
 * @param length : length of data to write
 * @return int : length of data have written
 * -1: error
 */
int http_client_write(char * postData, int length);

/**
 * @brief  need to call after esp_http_client_open, it will read from http stream, process all receive headers.
 * 
 * @return int 
 * 0: if stream doesn’t contain content-length header
 * -1: if any errors
 * others: download data length defined by content-length header
 */
int http_client_fetch_headers();

/**
 * @brief Check response data is chunked
 * 
 * @return bool: true of false 
 */
bool http_client_is_chunked_response();

/**
 * @brief Get http response content length (from header Content-Length)
 * 
 * @return int 
 * -1: Chunked transfer
 * others: Content-Length value as bytes
 */
int http_client_get_content_length();

/**
 * @brief Get http response status code
 * 
 * @return int : status code
 */
int http_client_get_status_code();

/**
 * @brief Close http connection, still kept all http request resources.
 * 
 */
void http_client_close();

esp_err_t _http_event_handle(esp_http_client_event_t *evt);

/**
 * @brief read data from http stream
 * 
 * @param buffer : the buffer
 * @param read : length of data was read
 * @param endMessage : indicate that if there is the end of data
 * @param contentLength : total length
 */
void http_client_read_with_contentLength(char * buffer, int * read, bool * endMessage, int contentLength);

/**
 * @brief Set http request header, this function must be called after esp_http_client_init and before any perform function.
 * 
 * @param headerKey : The header key
 * @param headerValue : The header value
 */
void http_client_set_header(char * headerKey, char * headerValue);

/**
 * @brief Set http client POST method
 * 
 */
void http_client_set_post();

/**
 * @brief : open the connection, write all header strings and return
 * 
 * @param length : HTTP Content length need to write to the server
 * @return esp_err_t 
 * ESP_OK : successful
 * ESP_FAIL : error
 */
esp_err_t http_client_open(int length);

#ifdef __cplusplus
}
#endif

#endif