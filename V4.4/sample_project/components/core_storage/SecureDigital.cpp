/*
 * Storage.cpp
 *
 *  Created on: 18 Jun 2018
 *      Author: boyan
 */

#include <driver/gpio.h>
#include <driver/sdmmc_types.h>
#include <driver/sdspi_host.h>
#include <esp_err.h>
#include <esp_log.h>
#include <esp_vfs_fat.h>
#include "StringOperators.h"
#include "Storage.h"
#include <sdmmc_cmd.h>
#include <sys/_default_fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/unistd.h>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cstdlib>


#define SDMMC_C_TAG "SDMMC Driver"

#define SD_PATH "/sdcard"

#define PIN_NUM_MISO GPIO_NUM_19
#define PIN_NUM_MOSI GPIO_NUM_23
#define PIN_NUM_CLK  GPIO_NUM_18
#define PIN_NUM_CS   GPIO_NUM_5

#define SPI_DMA_CHAN    1

const char * MageControl::Storage::SecureDigital::partitionPrefix = "/sdcard";

bool MageControl::Storage::SecureDigital::partitionMounted = false;

bool MageControl::Storage::SecureDigital::mountPartition( ) 
{
    esp_err_t err;

    sdmmc_host_t sdmmc_host = SDSPI_HOST_DEFAULT();
    spi_bus_config_t bus_cfg = {
        .mosi_io_num = PIN_NUM_MOSI,
        .miso_io_num = PIN_NUM_MISO,
        .sclk_io_num = PIN_NUM_CLK,
        .quadwp_io_num = -1,
        .quadhd_io_num = -1,
        .max_transfer_sz = 4000,
    };   
    err = spi_bus_initialize(SDSPI_DEFAULT_HOST, &bus_cfg, SPI_DMA_CHAN);
    if (err != ESP_OK) {
        ESP_LOGE(SDMMC_C_TAG, "Failed to initialize bus.");
        return false;
    }   

    sdspi_device_config_t slot_config = SDSPI_DEVICE_CONFIG_DEFAULT();
    slot_config.gpio_cs = PIN_NUM_CS;
    slot_config.host_id = SDSPI_DEFAULT_HOST;

    esp_vfs_fat_sdmmc_mount_config_t mount_config = {
            .format_if_mount_failed = false,
            .max_files = 5,
            .allocation_unit_size = 16 * 1024
        };

    sdmmc_card_t * sdcard;
    err = esp_vfs_fat_sdspi_mount(
            MageControl::Storage::SecureDigital::partitionPrefix,
            &sdmmc_host,
            &slot_config,
            &mount_config,
            &sdcard);

    if (err != ESP_OK) 
    {
        if (err == ESP_FAIL) 
        {
            // ESP_LOGE(SDMMC_C_TAG, "SD Card File-system mount failure.");
            MageControl::Storage::SecureDigital::partitionMounted = false;
        } 
        else 
        {
            // ESP_LOGE(SDMMC_C_TAG, "Failed to initialize SD Card. ERROR_ID: %d", err);
            MageControl::Storage::SecureDigital::partitionMounted = false;
        }
    } 
    else 
    {
        // ESP_LOGI(SDMMC_C_TAG, "SD Card mounted");
        MageControl::Storage::SecureDigital::partitionMounted = true;
        sdmmc_card_print_info(stdout, sdcard);
    }
    return MageControl::Storage::SecureDigital::partitionMounted;
}

bool MageControl::Storage::SecureDigital::fExists( const char* fname ) 
{
    if( !MageControl::Storage::SecureDigital::partitionMounted ) 
    {
        return false;
    }

    size_t fullpathname_size = MageControl::StringOp::c99_ctsprintf("%s%s%s",
            MageControl::Storage::SecureDigital::partitionPrefix, "/", fname);
    char * fullpathname = (char*) malloc(fullpathname_size + 1);
    memset(fullpathname, 0, fullpathname_size + 1);

    if( fname[0] == '/' ) 
    {
        sprintf(fullpathname, "%s%s",
                MageControl::Storage::SecureDigital::partitionPrefix, fname);
        struct stat st;
        if ( stat(fullpathname, &st) == 0 ) 
        {
            if ( fullpathname ) 
            {
                free(fullpathname);
            }
            return true;
        }
    } 
    else 
    {
        sprintf(fullpathname, "%s%s%s",
                MageControl::Storage::SecureDigital::partitionPrefix, "/", fname);
        struct stat st;
        if( stat(fullpathname, &st) == 0 ) 
        {
            if( fullpathname ) 
            {
                free(fullpathname);
            }
            return true;
        }
    }

    if( fullpathname ) 
    {
        free(fullpathname);
    }
    return false;
}

off_t MageControl::Storage::SecureDigital::fSize( const char* fname ) 
{
    size_t fullpathname_size = MageControl::StringOp::c99_ctsprintf("%s%s%s",
            MageControl::Storage::SecureDigital::partitionPrefix, "/", fname);
    char * fullpathname = (char *) malloc(fullpathname_size + 1);
    memset(fullpathname, 0, fullpathname_size + 1);

    if ( fname[0] == '/' ) 
    {
        sprintf(fullpathname, "%s%s",
                MageControl::Storage::SecureDigital::partitionPrefix, fname);
        struct stat st;
        if ( stat(fullpathname, &st) == 0 ) {
            free(fullpathname);
            return st.st_size;
        }
    } 
    else 
    {
        sprintf(fullpathname, "%s%s%s",
                MageControl::Storage::SecureDigital::partitionPrefix, "/", fname);
        struct stat st;
        if ( stat(fullpathname, &st) == 0 ) 
        {
            free(fullpathname);
            return st.st_size;
        }
    }
    free(fullpathname);
    return 0;
}

MageControl::Storage::FileDescriptor_t MageControl::Storage::SecureDigital::fOpen(
        const char * fname, int flags ) 
{
    FileDescriptor_t fptr = 0;

    size_t fullpathname_size = MageControl::StringOp::c99_ctsprintf("%s%s%s",
            MageControl::Storage::SecureDigital::partitionPrefix, "/", fname);
    char * fullpathname = (char *) malloc(fullpathname_size + 1);
    memset(fullpathname, 0, fullpathname_size + 1);

    if( fname[0] == '/' ) 
    {
        sprintf(fullpathname, "%s%s",
                MageControl::Storage::SecureDigital::partitionPrefix, fname);
        // struct stat st;
        //if ( stat(fullpathname, &st) == 0 ) 
        {
            fptr = open(fullpathname, flags);
            if ( fullpathname ) 
            {
                free(fullpathname);
            }
        }
    } else {
        sprintf(fullpathname, "%s%s%s",
                MageControl::Storage::SecureDigital::partitionPrefix, "/", fname);
        // struct stat st;
        //if ( stat(fullpathname, &st) == 0 ) 
        {
            fptr = open(fullpathname, flags);
            if ( fullpathname ) 
            {
                free(fullpathname);
            }
        }
    }
    return fptr;
}

ssize_t
MageControl::Storage::SecureDigital::fRead(FileDescriptor_t const& fd, void* buffer, unsigned int const dlen) 
{
    ssize_t off_read;
    off_read = read(fd, buffer, dlen);
    return off_read;
}

ssize_t
MageControl::Storage::SecureDigital::fWrite(FileDescriptor_t const& fd, const void* data, unsigned int const dlen) 
{
    ssize_t offset;
    offset = write(fd, data, dlen);
    return offset;
}

void
MageControl::Storage::SecureDigital::fClose(MageControl::Storage::FileDescriptor_t const& fd) 
{
    close(fd);
}
