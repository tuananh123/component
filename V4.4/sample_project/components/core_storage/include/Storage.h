/*
 * Storage.h
 *
 *  Created on: 18 Jun 2018
 *      Author: boyan
 */

#ifndef MAIN_STORAGE_H_
#define MAIN_STORAGE_H_

#include <cstdbool>
#include <cstdlib>
#include <cstdio>
#include <string>

#include "esp_system.h"

namespace MageControl 
{
///
/// @brief Namespace contains hardware specific file
/// manipulators.
///
namespace Storage 
{

///
/// @brief Type represents a file descriptor, to be used with
/// any hardware specific file manipulators defined in MageControl::Storage
///
typedef int FileDescriptor_t;

///
/// @brief Class EEPROM contains file manipulators that leverage the
/// SPI flash hardware, if partitioned in a suitable way by the ESP
/// bootloader.
///
class EEPROM 
{
private:

    ///
    /// @brief the partition prefix can be synonymous with a
    /// "mount point" for the SPI flash filesystem.
    ///
    /// @note Defaults to "/fs". Any files under this filesystem can
    /// be addressed as /fs/file.txt
    static const char * partitionPrefix;

    ///
    /// @brief indicates whether partition has been successfully mounted
    ///
    static bool partitionMounted;
public:

public:

    ///
    /// @brief no ctor
    ///
    EEPROM() = delete;

    ///
    /// @brief no dtor
    ///
    virtual ~EEPROM() = delete;

    ///
    /// @brief no copy
    ///
    EEPROM(const EEPROM&) = delete;

    ///
    /// @brief no selfies
    ///
    EEPROM& operator=(const EEPROM&) = delete;

    ///
    /// @brief Attempts to mount the partition that belongs to the EEPROM class.
    /// In this case it's the first partition of type data and subtype spiffs
    /// that it encounters. Does not support mounting multiple partitions.
    ///
    /// @return true if success, false otherwise
    ///
    static bool mountPartition();

    ///
    /// @brief Checks if a file exists in the EEPROM filesystem.
    ///
    /// @param fname file, by name, no /fs prefix needed
    ///
    /// @return true if file exists, false otherwise
    ///
    static bool fExists(const char * fname);

    ///
    /// @brief Returns the size of a file by name. Returns 0 if the file
    /// doesn't exist.
    ///
    /// @param fname file, by name, no /fs prefix needed
    ///
    /// @return the size of a file if it exists, 0 otherwise
    ///
    static off_t fSize(const char * fname);

    ///
    /// @brief Function opens a file and returns a FileDescriptor_t of it.
    ///
    /// @param fname file name, no /fs prefix needed
    /// @param flags posix file open flags (e.g. O_RDONLY)
    ///
    /// @return FileDescriptor_t of the opened file, can be used to write the file
    /// and close it after
    ///
    static FileDescriptor_t fOpen(const char * fname, int flags);

    ///
    /// @brief Function reads bytes from a file and writes them to a buffer, up to n bytes
    /// or until EOF.
    ///
    /// @param fd FileDescriptor_t of opened file
    /// @param buffer The buffer to write to, must be pre-allocated, and of sufficient size
    /// @param dlen number of bytes to read
    ///
    /// @return ssize_t the number of bytes read, less than dlen on EOF
    ///
    static ssize_t fRead(FileDescriptor_t const& fd, void* buffer,
            unsigned int const dlen);

    ///
    /// @brief Function reads from a buffer and writes bytes to a file, up to n bytes
    ///
    /// @note FileDescriptor_t must be for a file, opened for writing or truncating
    ///
    /// @param fd FileDescriptor_t of opened file
    /// @oaram data The buffer to read from, must be pre-allocated, and of sufficient size
    /// @param dlen number of bytes to write to file
    ///
    /// @return ssize_t number of bytes actually written to file
    ///
    static ssize_t fWrite(FileDescriptor_t const& fd, const void* data,
            unsigned int const dlen);

    ///
    /// @brief Function closes an opened file
    ///
    /// @note FileDescriptor_t must be non-null, and point to an opened file
    ///
    /// @param fd FileDescriptor_t of opened file
    ///
    static void fClose(FileDescriptor_t const& fd);
};

class SecureDigital 
{
private:

    ///
    /// @brief the partition prefix can be synonymous with a
    /// "mount point" for the SPI flash filesystem.
    ///
    /// @note Defaults to "/sdcard". Any files under this filesystem can
    /// be addressed as /sdcard/file.txt
    static const char * partitionPrefix;
    static bool partitionMounted;
public:
    ///
    /// @brief no ctor
    ///
    SecureDigital() = delete;

    ///
    /// @brief no dtor
    ///
    virtual ~SecureDigital() = delete;

    ///
    /// @brief no copy
    ///
    SecureDigital(const SecureDigital&) = delete;

    ///
    /// @brief no selfies
    ///
    SecureDigital& operator=(const SecureDigital&) = delete;

    ///
    /// @brief Attempts to mount the partition that belongs to the SecureDigital class.
    /// In this case it's the SPISD card interface, if connected.
    /// Does not support mounting multiple SD card partitions. Only supports a single FAT32 formatted drive.
    ///
    /// @return true if success, false otherwise
    ///
    static bool mountPartition();

    ///
    /// @brief Checks if a file exists in the SDCard filesystem.
    ///
    /// @param fname file, by name, no /sdcard prefix needed
    ///
    /// @return true if file exists, false otherwise
    ///
    static bool fExists(const char * fname);

    ///
    /// @brief Returns the size of a file by name. Returns 0 if the file
    /// doesn't exist.
    ///
    /// @param fname file, by name, no /sdcard prefix needed
    ///
    /// @return the size of a file if it exists, 0 otherwise
    ///
    static off_t fSize(const char * fname);

    ///
    /// @brief Function opens a file and returns a FileDescriptor_t of it.
    ///
    /// @param fname file name, no /sdcard prefix needed
    /// @param flags posix file open flags (e.g. O_RDONLY)
    ///
    /// @return FileDescriptor_t of the opened file, can be used to write the file
    /// and close it after
    ///
    static FileDescriptor_t fOpen(const char * fname, int flags);

    ///
    /// @brief Function reads bytes from a file and writes them to a buffer, up to n bytes
    /// or until EOF.
    ///
    /// @param fd FileDescriptor_t of opened file
    /// @param buffer The buffer to write to, must be pre-allocated, and of sufficient size
    /// @param dlen number of bytes to read
    ///
    /// @return ssize_t the number of bytes read, less than dlen on EOF
    ///
    static ssize_t fRead(FileDescriptor_t const& fd, void* buffer,
            unsigned int const dlen);

    ///
    /// @brief Function reads from a buffer and writes bytes to a file, up to n bytes
    ///
    /// @note FileDescriptor_t must be for a file, opened for writing or truncating
    ///
    /// @param fd FileDescriptor_t of opened file
    /// @oaram data The buffer to read from, must be pre-allocated, and of sufficient size
    /// @param dlen number of bytes to write to file
    ///
    /// @return ssize_t number of bytes actually written to file
    ///
    static ssize_t fWrite(FileDescriptor_t const& fd, const void* data,
            unsigned int const dlen);

    ///
    /// @brief Function closes an opened file
    ///
    /// @note FileDescriptor_t must be non-null, and point to an opened file
    ///
    /// @param fd FileDescriptor_t of opened file
    ///
    static void fClose(FileDescriptor_t const& fd);
};
}
}

#endif /* MAIN_STORAGE_H_ */
