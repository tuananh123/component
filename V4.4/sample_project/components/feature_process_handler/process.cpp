/**
 * @file nvs.cpp
 * @author backhug team
 * @brief 
 * @version 0.1
 * @date 2022-27-07
 * 
 * @copyright Copyright (c) 2022
 * 
 */

/*******************************************************************************
**                                INCLUDES
*******************************************************************************/
#include <cstring>
#include "ota.h"
#include "esp_heap_caps.h"
#include "wifi_sta.h"
#include "esp_system.h"
#include "esp_event.h"
#include "nvs_flash.h"
#include "esp_wifi.h"
#include "uploader.h"
#include "esp_log.h"
#include "ota_not_sdcard.h"
#include "ota_for_ble_chip.h"
#include "ota_use_sdcard.h"
#include "communication.h"
#include "nvs.h"
#include "uploader_spine_map.h"
#include "esp_log.h"
#include "upload_loggging.h"
/*******************************************************************************
**                       INTERNAL MACRO DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      COMMON VARIABLE DEFINITIONS
*******************************************************************************/

/*******************************************************************************
**                      INTERNAL VARIABLE DEFINITIONS
*******************************************************************************/
static TaskHandle_t task_handle = NULL;
// char *TAG = "OTA";
/*******************************************************************************
**                      INTERNAL FUNCTION PROTOTYPES
*******************************************************************************/

/*******************************************************************************
**                          FUNCTION DEFINITIONS
*******************************************************************************/
uint8_t firmware_version[3] = {1, 1, 8};
uint8_t ble_version[3] = {0, 0, 0};
uint8_t dem=0;

// bool sdcard_mount;
firmware_t ble_firmware, wifi_firmware;
// uint8_t error_code;
int8_t rssi_ap = 0;
uint32_t heap = 0;
void process_handler(void)
{
  bool ret;
  while (!wifi_check_mac_address())
  {
    vTaskDelay(1000 / portTICK_PERIOD_MS);
    packet buffer_packet;
    memset(&buffer_packet, 0, sizeof(packet));
    ret = send_wifi_mac_request(&buffer_packet);
    if (ret == false)
    {
      printf("send_wifi_mac_request fail!!\n");
      continue;
    }
    nvs_save_macadd((char*)buffer_packet.payload);
  }

    while (1)
  {
    while (!get_connection_state())
    {
      // Delay 0.5s
      vTaskDelay(500 / portTICK_PERIOD_MS);
    }

    heap = xPortGetFreeHeapSize();
    ESP_LOGW("MAIN","xPortGetFreeHeapSize %d = DRAM",heap);

    if (get_connection_state())
    {
      wifi_ap_record_t wifidata;
      if (esp_wifi_sta_get_ap_info(&wifidata)==0){
        rssi_ap = wifidata.rssi;
      }
    }

    if (get_connection_state())
    {
      upload_bed_stat();
      vTaskDelay(1000 / portTICK_PERIOD_MS);
      dem += 1;
    }
    
    if (get_connection_state()) 
    {
      if (request_upgrade_permission("/ble"))
      {
        bool ret;
        printf("BLE allow upgrade!!!\n");
        ret = download_firmware(&ble_firmware, ble_version);
        if (ret) 
        {
          send_firmware_ble = true;   
          while (send_firmware_ble)
          {
            vTaskDelay(1000 / portTICK_PERIOD_MS); 
          }
          printf("BLE upgrade end\n");   
        }
      }
      if (request_upgrade_permission("/wifi"))
      {
        printf("WIFI allow upgrade!!!\n");
        download_firmware_notsdcard(&wifi_firmware, firmware_version);
      }
      // if (request_upgrade_permission("/wifi"))
      // {
      //   printf("WIFI allow upgrade!!!\n");
      //   ret = download_firmware(&wifi_firmware, firmware_version);
      //   if (ret)  
      //     ota_process(&wifi_firmware);
      // }
    }
  //   // //TODO:build next versiond
    if (get_connection_state()) 
    {
      if (!query_request.new_file_massage)
      {
      UploadSdcardFile(countFileSDcard()); 
          if(dem==2){
          dem = 0;
          //UploadLogFile();
        }
      }
     }
    vTaskDelay(30000 / portTICK_PERIOD_MS);
  }
}


void create_process_handle(void)
{
  if (task_handle == NULL)
  {
    xTaskCreate((TaskFunction_t) &process_handler,
                "task",
                10*1024,
                NULL,
                5,
                &task_handle);

  }
}
